$(function() {
	$('#toast-btn').click(function() {
		xin.showToast({
			content: 'hello world'
		});
	});
	$('#toast-success-btn').click(function() {
		xin.hintSuccess('hello world');
	});
	$('#toast-error-btn').click(function() {
		xin.hintError('hello world');
	});

	$('#loading-btn').click(function() {
		xin.showLoading('hello world');
		setTimeout(function() {
			xin.hideLoading();
		}, 1500);
	});
});
