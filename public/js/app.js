/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"/js/app": 0
/******/ 	};
/******/
/******/
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "js/" + ({"vendors~wangeditor":"vendors~wangeditor"}[chunkId]||chunkId) + ".js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/charenc/charenc.js":
/*!*****************************************!*\
  !*** ./node_modules/charenc/charenc.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var charenc = {
  // UTF-8 encoding
  utf8: {
    // Convert a string to a byte array
    stringToBytes: function(str) {
      return charenc.bin.stringToBytes(unescape(encodeURIComponent(str)));
    },

    // Convert a byte array to a string
    bytesToString: function(bytes) {
      return decodeURIComponent(escape(charenc.bin.bytesToString(bytes)));
    }
  },

  // Binary encoding
  bin: {
    // Convert a string to a byte array
    stringToBytes: function(str) {
      for (var bytes = [], i = 0; i < str.length; i++)
        bytes.push(str.charCodeAt(i) & 0xFF);
      return bytes;
    },

    // Convert a byte array to a string
    bytesToString: function(bytes) {
      for (var str = [], i = 0; i < bytes.length; i++)
        str.push(String.fromCharCode(bytes[i]));
      return str.join('');
    }
  }
};

module.exports = charenc;


/***/ }),

/***/ "./node_modules/crypt/crypt.js":
/*!*************************************!*\
  !*** ./node_modules/crypt/crypt.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() {
  var base64map
      = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',

  crypt = {
    // Bit-wise rotation left
    rotl: function(n, b) {
      return (n << b) | (n >>> (32 - b));
    },

    // Bit-wise rotation right
    rotr: function(n, b) {
      return (n << (32 - b)) | (n >>> b);
    },

    // Swap big-endian to little-endian and vice versa
    endian: function(n) {
      // If number given, swap endian
      if (n.constructor == Number) {
        return crypt.rotl(n, 8) & 0x00FF00FF | crypt.rotl(n, 24) & 0xFF00FF00;
      }

      // Else, assume array and swap all items
      for (var i = 0; i < n.length; i++)
        n[i] = crypt.endian(n[i]);
      return n;
    },

    // Generate an array of any length of random bytes
    randomBytes: function(n) {
      for (var bytes = []; n > 0; n--)
        bytes.push(Math.floor(Math.random() * 256));
      return bytes;
    },

    // Convert a byte array to big-endian 32-bit words
    bytesToWords: function(bytes) {
      for (var words = [], i = 0, b = 0; i < bytes.length; i++, b += 8)
        words[b >>> 5] |= bytes[i] << (24 - b % 32);
      return words;
    },

    // Convert big-endian 32-bit words to a byte array
    wordsToBytes: function(words) {
      for (var bytes = [], b = 0; b < words.length * 32; b += 8)
        bytes.push((words[b >>> 5] >>> (24 - b % 32)) & 0xFF);
      return bytes;
    },

    // Convert a byte array to a hex string
    bytesToHex: function(bytes) {
      for (var hex = [], i = 0; i < bytes.length; i++) {
        hex.push((bytes[i] >>> 4).toString(16));
        hex.push((bytes[i] & 0xF).toString(16));
      }
      return hex.join('');
    },

    // Convert a hex string to a byte array
    hexToBytes: function(hex) {
      for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
      return bytes;
    },

    // Convert a byte array to a base-64 string
    bytesToBase64: function(bytes) {
      for (var base64 = [], i = 0; i < bytes.length; i += 3) {
        var triplet = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
        for (var j = 0; j < 4; j++)
          if (i * 8 + j * 6 <= bytes.length * 8)
            base64.push(base64map.charAt((triplet >>> 6 * (3 - j)) & 0x3F));
          else
            base64.push('=');
      }
      return base64.join('');
    },

    // Convert a base-64 string to a byte array
    base64ToBytes: function(base64) {
      // Remove non-base-64 characters
      base64 = base64.replace(/[^A-Z0-9+\/]/ig, '');

      for (var bytes = [], i = 0, imod4 = 0; i < base64.length;
          imod4 = ++i % 4) {
        if (imod4 == 0) continue;
        bytes.push(((base64map.indexOf(base64.charAt(i - 1))
            & (Math.pow(2, -2 * imod4 + 8) - 1)) << (imod4 * 2))
            | (base64map.indexOf(base64.charAt(i)) >>> (6 - imod4 * 2)));
      }
      return bytes;
    }
  };

  module.exports = crypt;
})();


/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/*!*****************************************!*\
  !*** ./node_modules/is-buffer/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}


/***/ }),

/***/ "./node_modules/md5/md5.js":
/*!*********************************!*\
  !*** ./node_modules/md5/md5.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function(){
  var crypt = __webpack_require__(/*! crypt */ "./node_modules/crypt/crypt.js"),
      utf8 = __webpack_require__(/*! charenc */ "./node_modules/charenc/charenc.js").utf8,
      isBuffer = __webpack_require__(/*! is-buffer */ "./node_modules/is-buffer/index.js"),
      bin = __webpack_require__(/*! charenc */ "./node_modules/charenc/charenc.js").bin,

  // The core
  md5 = function (message, options) {
    // Convert to byte array
    if (message.constructor == String)
      if (options && options.encoding === 'binary')
        message = bin.stringToBytes(message);
      else
        message = utf8.stringToBytes(message);
    else if (isBuffer(message))
      message = Array.prototype.slice.call(message, 0);
    else if (!Array.isArray(message))
      message = message.toString();
    // else, assume byte array already

    var m = crypt.bytesToWords(message),
        l = message.length * 8,
        a =  1732584193,
        b = -271733879,
        c = -1732584194,
        d =  271733878;

    // Swap endian
    for (var i = 0; i < m.length; i++) {
      m[i] = ((m[i] <<  8) | (m[i] >>> 24)) & 0x00FF00FF |
             ((m[i] << 24) | (m[i] >>>  8)) & 0xFF00FF00;
    }

    // Padding
    m[l >>> 5] |= 0x80 << (l % 32);
    m[(((l + 64) >>> 9) << 4) + 14] = l;

    // Method shortcuts
    var FF = md5._ff,
        GG = md5._gg,
        HH = md5._hh,
        II = md5._ii;

    for (var i = 0; i < m.length; i += 16) {

      var aa = a,
          bb = b,
          cc = c,
          dd = d;

      a = FF(a, b, c, d, m[i+ 0],  7, -680876936);
      d = FF(d, a, b, c, m[i+ 1], 12, -389564586);
      c = FF(c, d, a, b, m[i+ 2], 17,  606105819);
      b = FF(b, c, d, a, m[i+ 3], 22, -1044525330);
      a = FF(a, b, c, d, m[i+ 4],  7, -176418897);
      d = FF(d, a, b, c, m[i+ 5], 12,  1200080426);
      c = FF(c, d, a, b, m[i+ 6], 17, -1473231341);
      b = FF(b, c, d, a, m[i+ 7], 22, -45705983);
      a = FF(a, b, c, d, m[i+ 8],  7,  1770035416);
      d = FF(d, a, b, c, m[i+ 9], 12, -1958414417);
      c = FF(c, d, a, b, m[i+10], 17, -42063);
      b = FF(b, c, d, a, m[i+11], 22, -1990404162);
      a = FF(a, b, c, d, m[i+12],  7,  1804603682);
      d = FF(d, a, b, c, m[i+13], 12, -40341101);
      c = FF(c, d, a, b, m[i+14], 17, -1502002290);
      b = FF(b, c, d, a, m[i+15], 22,  1236535329);

      a = GG(a, b, c, d, m[i+ 1],  5, -165796510);
      d = GG(d, a, b, c, m[i+ 6],  9, -1069501632);
      c = GG(c, d, a, b, m[i+11], 14,  643717713);
      b = GG(b, c, d, a, m[i+ 0], 20, -373897302);
      a = GG(a, b, c, d, m[i+ 5],  5, -701558691);
      d = GG(d, a, b, c, m[i+10],  9,  38016083);
      c = GG(c, d, a, b, m[i+15], 14, -660478335);
      b = GG(b, c, d, a, m[i+ 4], 20, -405537848);
      a = GG(a, b, c, d, m[i+ 9],  5,  568446438);
      d = GG(d, a, b, c, m[i+14],  9, -1019803690);
      c = GG(c, d, a, b, m[i+ 3], 14, -187363961);
      b = GG(b, c, d, a, m[i+ 8], 20,  1163531501);
      a = GG(a, b, c, d, m[i+13],  5, -1444681467);
      d = GG(d, a, b, c, m[i+ 2],  9, -51403784);
      c = GG(c, d, a, b, m[i+ 7], 14,  1735328473);
      b = GG(b, c, d, a, m[i+12], 20, -1926607734);

      a = HH(a, b, c, d, m[i+ 5],  4, -378558);
      d = HH(d, a, b, c, m[i+ 8], 11, -2022574463);
      c = HH(c, d, a, b, m[i+11], 16,  1839030562);
      b = HH(b, c, d, a, m[i+14], 23, -35309556);
      a = HH(a, b, c, d, m[i+ 1],  4, -1530992060);
      d = HH(d, a, b, c, m[i+ 4], 11,  1272893353);
      c = HH(c, d, a, b, m[i+ 7], 16, -155497632);
      b = HH(b, c, d, a, m[i+10], 23, -1094730640);
      a = HH(a, b, c, d, m[i+13],  4,  681279174);
      d = HH(d, a, b, c, m[i+ 0], 11, -358537222);
      c = HH(c, d, a, b, m[i+ 3], 16, -722521979);
      b = HH(b, c, d, a, m[i+ 6], 23,  76029189);
      a = HH(a, b, c, d, m[i+ 9],  4, -640364487);
      d = HH(d, a, b, c, m[i+12], 11, -421815835);
      c = HH(c, d, a, b, m[i+15], 16,  530742520);
      b = HH(b, c, d, a, m[i+ 2], 23, -995338651);

      a = II(a, b, c, d, m[i+ 0],  6, -198630844);
      d = II(d, a, b, c, m[i+ 7], 10,  1126891415);
      c = II(c, d, a, b, m[i+14], 15, -1416354905);
      b = II(b, c, d, a, m[i+ 5], 21, -57434055);
      a = II(a, b, c, d, m[i+12],  6,  1700485571);
      d = II(d, a, b, c, m[i+ 3], 10, -1894986606);
      c = II(c, d, a, b, m[i+10], 15, -1051523);
      b = II(b, c, d, a, m[i+ 1], 21, -2054922799);
      a = II(a, b, c, d, m[i+ 8],  6,  1873313359);
      d = II(d, a, b, c, m[i+15], 10, -30611744);
      c = II(c, d, a, b, m[i+ 6], 15, -1560198380);
      b = II(b, c, d, a, m[i+13], 21,  1309151649);
      a = II(a, b, c, d, m[i+ 4],  6, -145523070);
      d = II(d, a, b, c, m[i+11], 10, -1120210379);
      c = II(c, d, a, b, m[i+ 2], 15,  718787259);
      b = II(b, c, d, a, m[i+ 9], 21, -343485551);

      a = (a + aa) >>> 0;
      b = (b + bb) >>> 0;
      c = (c + cc) >>> 0;
      d = (d + dd) >>> 0;
    }

    return crypt.endian([a, b, c, d]);
  };

  // Auxiliary functions
  md5._ff  = function (a, b, c, d, x, s, t) {
    var n = a + (b & c | ~b & d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._gg  = function (a, b, c, d, x, s, t) {
    var n = a + (b & d | c & ~d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._hh  = function (a, b, c, d, x, s, t) {
    var n = a + (b ^ c ^ d) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };
  md5._ii  = function (a, b, c, d, x, s, t) {
    var n = a + (c ^ (b | ~d)) + (x >>> 0) + t;
    return ((n << s) | (n >>> (32 - s))) + b;
  };

  // Package private blocksize
  md5._blocksize = 16;
  md5._digestsize = 16;

  module.exports = function (message, options) {
    if (message === undefined || message === null)
      throw new Error('Illegal argument ' + message);

    var digestbytes = crypt.wordsToBytes(md5(message, options));
    return options && options.asBytes ? digestbytes :
        options && options.asString ? bin.bytesToString(digestbytes) :
        crypt.bytesToHex(digestbytes);
  };

})();


/***/ }),

/***/ "./node_modules/node-libs-browser/node_modules/punycode/punycode.js":
/*!**************************************************************************!*\
  !*** ./node_modules/node-libs-browser/node_modules/punycode/punycode.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;/*! https://mths.be/punycode v1.4.1 by @mathias */
;(function(root) {

	/** Detect free variables */
	var freeExports =  true && exports &&
		!exports.nodeType && exports;
	var freeModule =  true && module &&
		!module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	if (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/**
	 * The `punycode` object.
	 * @name punycode
	 * @type Object
	 */
	var punycode,

	/** Highest positive signed 32-bit float value */
	maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

	/** Bootstring parameters */
	base = 36,
	tMin = 1,
	tMax = 26,
	skew = 38,
	damp = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimiter = '-', // '\x2D'

	/** Regular expressions */
	regexPunycode = /^xn--/,
	regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
	regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

	/** Error messages */
	errors = {
		'overflow': 'Overflow: input needs wider integers to process',
		'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
		'invalid-input': 'Invalid input'
	},

	/** Convenience shortcuts */
	baseMinusTMin = base - tMin,
	floor = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/** Temporary variable */
	key;

	/*--------------------------------------------------------------------------*/

	/**
	 * A generic error utility function.
	 * @private
	 * @param {String} type The error type.
	 * @returns {Error} Throws a `RangeError` with the applicable error message.
	 */
	function error(type) {
		throw new RangeError(errors[type]);
	}

	/**
	 * A generic `Array#map` utility function.
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} callback The function that gets called for every array
	 * item.
	 * @returns {Array} A new array of values returned by the callback function.
	 */
	function map(array, fn) {
		var length = array.length;
		var result = [];
		while (length--) {
			result[length] = fn(array[length]);
		}
		return result;
	}

	/**
	 * A simple `Array#map`-like wrapper to work with domain name strings or email
	 * addresses.
	 * @private
	 * @param {String} domain The domain name or email address.
	 * @param {Function} callback The function that gets called for every
	 * character.
	 * @returns {Array} A new string of characters returned by the callback
	 * function.
	 */
	function mapDomain(string, fn) {
		var parts = string.split('@');
		var result = '';
		if (parts.length > 1) {
			// In email addresses, only the domain name should be punycoded. Leave
			// the local part (i.e. everything up to `@`) intact.
			result = parts[0] + '@';
			string = parts[1];
		}
		// Avoid `split(regex)` for IE8 compatibility. See #17.
		string = string.replace(regexSeparators, '\x2E');
		var labels = string.split('.');
		var encoded = map(labels, fn).join('.');
		return result + encoded;
	}

	/**
	 * Creates an array containing the numeric code points of each Unicode
	 * character in the string. While JavaScript uses UCS-2 internally,
	 * this function will convert a pair of surrogate halves (each of which
	 * UCS-2 exposes as separate characters) into a single code point,
	 * matching UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @name decode
	 * @param {String} string The Unicode input string (UCS-2).
	 * @returns {Array} The new array of code points.
	 */
	function ucs2decode(string) {
		var output = [],
		    counter = 0,
		    length = string.length,
		    value,
		    extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	/**
	 * Creates a string based on an array of numeric code points.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @name encode
	 * @param {Array} codePoints The array of numeric code points.
	 * @returns {String} The new Unicode string (UCS-2).
	 */
	function ucs2encode(array) {
		return map(array, function(value) {
			var output = '';
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
			return output;
		}).join('');
	}

	/**
	 * Converts a basic code point into a digit/integer.
	 * @see `digitToBasic()`
	 * @private
	 * @param {Number} codePoint The basic numeric code point value.
	 * @returns {Number} The numeric value of a basic code point (for use in
	 * representing integers) in the range `0` to `base - 1`, or `base` if
	 * the code point does not represent a value.
	 */
	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
			return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
			return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
			return codePoint - 97;
		}
		return base;
	}

	/**
	 * Converts a digit/integer into a basic code point.
	 * @see `basicToDigit()`
	 * @private
	 * @param {Number} digit The numeric value of a basic code point.
	 * @returns {Number} The basic code point whose value (when used for
	 * representing integers) is `digit`, which needs to be in the range
	 * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	 * used; else, the lowercase form is used. The behavior is undefined
	 * if `flag` is non-zero and `digit` has no uppercase form.
	 */
	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	/**
	 * Bias adaptation function as per section 3.4 of RFC 3492.
	 * https://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 */
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
			delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/**
	 * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	 * symbols.
	 * @memberOf punycode
	 * @param {String} input The Punycode string of ASCII-only symbols.
	 * @returns {String} The resulting string of Unicode symbols.
	 */
	function decode(input) {
		// Don't use UCS-2
		var output = [],
		    inputLength = input.length,
		    out,
		    i = 0,
		    n = initialN,
		    bias = initialBias,
		    basic,
		    j,
		    index,
		    oldi,
		    w,
		    k,
		    digit,
		    t,
		    /** Cached calculation results */
		    baseMinusT;

		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.

		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
			basic = 0;
		}

		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				error('not-basic');
			}
			output.push(input.charCodeAt(j));
		}

		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.

		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
					error('invalid-input');
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					error('overflow');
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
					break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					error('overflow');
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				error('overflow');
			}

			n += floor(i / out);
			i %= out;

			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);

		}

		return ucs2encode(output);
	}

	/**
	 * Converts a string of Unicode symbols (e.g. a domain name label) to a
	 * Punycode string of ASCII-only symbols.
	 * @memberOf punycode
	 * @param {String} input The string of Unicode symbols.
	 * @returns {String} The resulting Punycode string of ASCII-only symbols.
	 */
	function encode(input) {
		var n,
		    delta,
		    handledCPCount,
		    basicLength,
		    bias,
		    j,
		    m,
		    q,
		    k,
		    t,
		    currentValue,
		    output = [],
		    /** `inputLength` will hold the number of code points in `input`. */
		    inputLength,
		    /** Cached calculation results */
		    handledCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);

		// Cache the length
		inputLength = input.length;

		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;

		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
			currentValue = input[j];
			if (currentValue < 0x80) {
				output.push(stringFromCharCode(currentValue));
			}
		}

		handledCPCount = basicLength = output.length;

		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.

		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
			output.push(delimiter);
		}

		// Main encoding loop:
		while (handledCPCount < inputLength) {

			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue >= n && currentValue < m) {
					m = currentValue;
				}
			}

			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
				error('overflow');
			}

			delta += (m - n) * handledCPCountPlusOne;
			n = m;

			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];

				if (currentValue < n && ++delta > maxInt) {
					error('overflow');
				}

				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
							break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
							stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}

					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}

			++delta;
			++n;

		}
		return output.join('');
	}

	/**
	 * Converts a Punycode string representing a domain name or an email address
	 * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	 * it doesn't matter if you call it on a string that has already been
	 * converted to Unicode.
	 * @memberOf punycode
	 * @param {String} input The Punycoded domain name or email address to
	 * convert to Unicode.
	 * @returns {String} The Unicode representation of the given Punycode
	 * string.
	 */
	function toUnicode(input) {
		return mapDomain(input, function(string) {
			return regexPunycode.test(string)
				? decode(string.slice(4).toLowerCase())
				: string;
		});
	}

	/**
	 * Converts a Unicode string representing a domain name or an email address to
	 * Punycode. Only the non-ASCII parts of the domain name will be converted,
	 * i.e. it doesn't matter if you call it with a domain that's already in
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input The domain name or email address to convert, as a
	 * Unicode string.
	 * @returns {String} The Punycode representation of the given domain name or
	 * email address.
	 */
	function toASCII(input) {
		return mapDomain(input, function(string) {
			return regexNonASCII.test(string)
				? 'xn--' + encode(string)
				: string;
		});
	}

	/*--------------------------------------------------------------------------*/

	/** Define the public API */
	punycode = {
		/**
		 * A string representing the current Punycode.js version number.
		 * @memberOf punycode
		 * @type String
		 */
		'version': '1.4.1',
		/**
		 * An object of methods to convert from JavaScript's internal character
		 * representation (UCS-2) to Unicode code points, and back.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 */
		'ucs2': {
			'decode': ucs2decode,
			'encode': ucs2encode
		},
		'decode': decode,
		'encode': encode,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/** Expose `punycode` */
	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		true
	) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
			return punycode;
		}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}

}(this));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module), __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/querystring-es3/decode.js":
/*!************************************************!*\
  !*** ./node_modules/querystring-es3/decode.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};


/***/ }),

/***/ "./node_modules/querystring-es3/encode.js":
/*!************************************************!*\
  !*** ./node_modules/querystring-es3/encode.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};


/***/ }),

/***/ "./node_modules/querystring-es3/index.js":
/*!***********************************************!*\
  !*** ./node_modules/querystring-es3/index.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.decode = exports.parse = __webpack_require__(/*! ./decode */ "./node_modules/querystring-es3/decode.js");
exports.encode = exports.stringify = __webpack_require__(/*! ./encode */ "./node_modules/querystring-es3/encode.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/url/url.js":
/*!*********************************!*\
  !*** ./node_modules/url/url.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var punycode = __webpack_require__(/*! punycode */ "./node_modules/node-libs-browser/node_modules/punycode/punycode.js");
var util = __webpack_require__(/*! ./util */ "./node_modules/url/util.js");

exports.parse = urlParse;
exports.resolve = urlResolve;
exports.resolveObject = urlResolveObject;
exports.format = urlFormat;

exports.Url = Url;

function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.host = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.query = null;
  this.pathname = null;
  this.path = null;
  this.href = null;
}

// Reference: RFC 3986, RFC 1808, RFC 2396

// define these here so at least they only have to be
// compiled once on the first module load.
var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,

    // Special case for a simple path URL
    simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,

    // RFC 2396: characters reserved for delimiting URLs.
    // We actually just auto-escape these.
    delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],

    // RFC 2396: characters not allowed for various reasons.
    unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),

    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
    autoEscape = ['\''].concat(unwise),
    // Characters that are never ever allowed in a hostname.
    // Note that any invalid chars are also handled, but these
    // are the ones that are *expected* to be seen, so we fast-path
    // them.
    nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
    hostEndingChars = ['/', '?', '#'],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.
    unsafeProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that never have a hostname.
    hostlessProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that always contain a // bit.
    slashedProtocol = {
      'http': true,
      'https': true,
      'ftp': true,
      'gopher': true,
      'file': true,
      'http:': true,
      'https:': true,
      'ftp:': true,
      'gopher:': true,
      'file:': true
    },
    querystring = __webpack_require__(/*! querystring */ "./node_modules/querystring-es3/index.js");

function urlParse(url, parseQueryString, slashesDenoteHost) {
  if (url && util.isObject(url) && url instanceof Url) return url;

  var u = new Url;
  u.parse(url, parseQueryString, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function(url, parseQueryString, slashesDenoteHost) {
  if (!util.isString(url)) {
    throw new TypeError("Parameter 'url' must be a string, not " + typeof url);
  }

  // Copy chrome, IE, opera backslash-handling behavior.
  // Back slashes before the query string get converted to forward slashes
  // See: https://code.google.com/p/chromium/issues/detail?id=25916
  var queryIndex = url.indexOf('?'),
      splitter =
          (queryIndex !== -1 && queryIndex < url.indexOf('#')) ? '?' : '#',
      uSplit = url.split(splitter),
      slashRegex = /\\/g;
  uSplit[0] = uSplit[0].replace(slashRegex, '/');
  url = uSplit.join(splitter);

  var rest = url;

  // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"
  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.path = rest;
      this.href = rest;
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
        if (parseQueryString) {
          this.query = querystring.parse(this.search.substr(1));
        } else {
          this.query = this.search.substr(1);
        }
      } else if (parseQueryString) {
        this.search = '';
        this.query = {};
      }
      return this;
    }
  }

  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    var lowerProto = proto.toLowerCase();
    this.protocol = lowerProto;
    rest = rest.substr(proto.length);
  }

  // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    var slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] &&
      (slashes || (proto && !slashedProtocol[proto]))) {

    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c

    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.

    // find the first instance of any hostEndingChars
    var hostEnd = -1;
    for (var i = 0; i < hostEndingChars.length; i++) {
      var hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }

    // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.
    var auth, atSign;
    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    }

    // Now we have a portion which is definitely the auth.
    // Pull that off.
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = decodeURIComponent(auth);
    }

    // the host is the remaining to the left of the first non-host char
    hostEnd = -1;
    for (var i = 0; i < nonHostChars.length; i++) {
      var hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }
    // if we still have not hit it, then the entire thing is a host.
    if (hostEnd === -1)
      hostEnd = rest.length;

    this.host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);

    // pull out port.
    this.parseHost();

    // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.
    this.hostname = this.hostname || '';

    // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.
    var ipv6Hostname = this.hostname[0] === '[' &&
        this.hostname[this.hostname.length - 1] === ']';

    // validate a little.
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for (var i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];
        if (!part) continue;
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          // we test again with ASCII char only
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = '/' + notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    } else {
      // hostnames are always lower case.
      this.hostname = this.hostname.toLowerCase();
    }

    if (!ipv6Hostname) {
      // IDNA Support: Returns a punycoded representation of "domain".
      // It only converts parts of the domain name that
      // have non-ASCII characters, i.e. it doesn't matter if
      // you call it with a domain that already is ASCII-only.
      this.hostname = punycode.toASCII(this.hostname);
    }

    var p = this.port ? ':' + this.port : '';
    var h = this.hostname || '';
    this.host = h + p;
    this.href += this.host;

    // strip [ and ] from the hostname
    // the host field still retains them, though
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
      if (rest[0] !== '/') {
        rest = '/' + rest;
      }
    }
  }

  // now rest is set to the post-host stuff.
  // chop off any delim chars.
  if (!unsafeProtocol[lowerProto]) {

    // First, make 100% sure that any "autoEscape" chars get
    // escaped, even if encodeURIComponent doesn't think they
    // need to be.
    for (var i = 0, l = autoEscape.length; i < l; i++) {
      var ae = autoEscape[i];
      if (rest.indexOf(ae) === -1)
        continue;
      var esc = encodeURIComponent(ae);
      if (esc === ae) {
        esc = escape(ae);
      }
      rest = rest.split(ae).join(esc);
    }
  }


  // chop off from the tail first.
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    this.query = rest.substr(qm + 1);
    if (parseQueryString) {
      this.query = querystring.parse(this.query);
    }
    rest = rest.slice(0, qm);
  } else if (parseQueryString) {
    // no query string, but parseQueryString still requested
    this.search = '';
    this.query = {};
  }
  if (rest) this.pathname = rest;
  if (slashedProtocol[lowerProto] &&
      this.hostname && !this.pathname) {
    this.pathname = '/';
  }

  //to support http.request
  if (this.pathname || this.search) {
    var p = this.pathname || '';
    var s = this.search || '';
    this.path = p + s;
  }

  // finally, reconstruct the href based on what has been validated.
  this.href = this.format();
  return this;
};

// format a parsed object into a url string
function urlFormat(obj) {
  // ensure it's an object, and not a string url.
  // If it's an obj, this is a no-op.
  // this way, you can call url_format() on strings
  // to clean up potentially wonky urls.
  if (util.isString(obj)) obj = urlParse(obj);
  if (!(obj instanceof Url)) return Url.prototype.format.call(obj);
  return obj.format();
}

Url.prototype.format = function() {
  var auth = this.auth || '';
  if (auth) {
    auth = encodeURIComponent(auth);
    auth = auth.replace(/%3A/i, ':');
    auth += '@';
  }

  var protocol = this.protocol || '',
      pathname = this.pathname || '',
      hash = this.hash || '',
      host = false,
      query = '';

  if (this.host) {
    host = auth + this.host;
  } else if (this.hostname) {
    host = auth + (this.hostname.indexOf(':') === -1 ?
        this.hostname :
        '[' + this.hostname + ']');
    if (this.port) {
      host += ':' + this.port;
    }
  }

  if (this.query &&
      util.isObject(this.query) &&
      Object.keys(this.query).length) {
    query = querystring.stringify(this.query);
  }

  var search = this.search || (query && ('?' + query)) || '';

  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  // only the slashedProtocols get the //.  Not mailto:, xmpp:, etc.
  // unless they had them to begin with.
  if (this.slashes ||
      (!protocol || slashedProtocol[protocol]) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname.charAt(0) !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash.charAt(0) !== '#') hash = '#' + hash;
  if (search && search.charAt(0) !== '?') search = '?' + search;

  pathname = pathname.replace(/[?#]/g, function(match) {
    return encodeURIComponent(match);
  });
  search = search.replace('#', '%23');

  return protocol + host + pathname + search + hash;
};

function urlResolve(source, relative) {
  return urlParse(source, false, true).resolve(relative);
}

Url.prototype.resolve = function(relative) {
  return this.resolveObject(urlParse(relative, false, true)).format();
};

function urlResolveObject(source, relative) {
  if (!source) return relative;
  return urlParse(source, false, true).resolveObject(relative);
}

Url.prototype.resolveObject = function(relative) {
  if (util.isString(relative)) {
    var rel = new Url();
    rel.parse(relative, false, true);
    relative = rel;
  }

  var result = new Url();
  var tkeys = Object.keys(this);
  for (var tk = 0; tk < tkeys.length; tk++) {
    var tkey = tkeys[tk];
    result[tkey] = this[tkey];
  }

  // hash is always overridden, no matter what.
  // even href="" will remove it.
  result.hash = relative.hash;

  // if the relative url is empty, then there's nothing left to do here.
  if (relative.href === '') {
    result.href = result.format();
    return result;
  }

  // hrefs like //foo/bar always cut to the protocol.
  if (relative.slashes && !relative.protocol) {
    // take everything except the protocol from relative
    var rkeys = Object.keys(relative);
    for (var rk = 0; rk < rkeys.length; rk++) {
      var rkey = rkeys[rk];
      if (rkey !== 'protocol')
        result[rkey] = relative[rkey];
    }

    //urlParse appends trailing / to urls like http://www.example.com
    if (slashedProtocol[result.protocol] &&
        result.hostname && !result.pathname) {
      result.path = result.pathname = '/';
    }

    result.href = result.format();
    return result;
  }

  if (relative.protocol && relative.protocol !== result.protocol) {
    // if it's a known url protocol, then changing
    // the protocol does weird things
    // first, if it's not file:, then we MUST have a host,
    // and if there was a path
    // to begin with, then we MUST have a path.
    // if it is file:, then the host is dropped,
    // because that's known to be hostless.
    // anything else is assumed to be absolute.
    if (!slashedProtocol[relative.protocol]) {
      var keys = Object.keys(relative);
      for (var v = 0; v < keys.length; v++) {
        var k = keys[v];
        result[k] = relative[k];
      }
      result.href = result.format();
      return result;
    }

    result.protocol = relative.protocol;
    if (!relative.host && !hostlessProtocol[relative.protocol]) {
      var relPath = (relative.pathname || '').split('/');
      while (relPath.length && !(relative.host = relPath.shift()));
      if (!relative.host) relative.host = '';
      if (!relative.hostname) relative.hostname = '';
      if (relPath[0] !== '') relPath.unshift('');
      if (relPath.length < 2) relPath.unshift('');
      result.pathname = relPath.join('/');
    } else {
      result.pathname = relative.pathname;
    }
    result.search = relative.search;
    result.query = relative.query;
    result.host = relative.host || '';
    result.auth = relative.auth;
    result.hostname = relative.hostname || relative.host;
    result.port = relative.port;
    // to support http.request
    if (result.pathname || result.search) {
      var p = result.pathname || '';
      var s = result.search || '';
      result.path = p + s;
    }
    result.slashes = result.slashes || relative.slashes;
    result.href = result.format();
    return result;
  }

  var isSourceAbs = (result.pathname && result.pathname.charAt(0) === '/'),
      isRelAbs = (
          relative.host ||
          relative.pathname && relative.pathname.charAt(0) === '/'
      ),
      mustEndAbs = (isRelAbs || isSourceAbs ||
                    (result.host && relative.pathname)),
      removeAllDots = mustEndAbs,
      srcPath = result.pathname && result.pathname.split('/') || [],
      relPath = relative.pathname && relative.pathname.split('/') || [],
      psychotic = result.protocol && !slashedProtocol[result.protocol];

  // if the url is a non-slashed url, then relative
  // links like ../.. should be able
  // to crawl up to the hostname, as well.  This is strange.
  // result.protocol has already been set by now.
  // Later on, put the first path part into the host field.
  if (psychotic) {
    result.hostname = '';
    result.port = null;
    if (result.host) {
      if (srcPath[0] === '') srcPath[0] = result.host;
      else srcPath.unshift(result.host);
    }
    result.host = '';
    if (relative.protocol) {
      relative.hostname = null;
      relative.port = null;
      if (relative.host) {
        if (relPath[0] === '') relPath[0] = relative.host;
        else relPath.unshift(relative.host);
      }
      relative.host = null;
    }
    mustEndAbs = mustEndAbs && (relPath[0] === '' || srcPath[0] === '');
  }

  if (isRelAbs) {
    // it's absolute.
    result.host = (relative.host || relative.host === '') ?
                  relative.host : result.host;
    result.hostname = (relative.hostname || relative.hostname === '') ?
                      relative.hostname : result.hostname;
    result.search = relative.search;
    result.query = relative.query;
    srcPath = relPath;
    // fall through to the dot-handling below.
  } else if (relPath.length) {
    // it's relative
    // throw away the existing file, and take the new path instead.
    if (!srcPath) srcPath = [];
    srcPath.pop();
    srcPath = srcPath.concat(relPath);
    result.search = relative.search;
    result.query = relative.query;
  } else if (!util.isNullOrUndefined(relative.search)) {
    // just pull out the search.
    // like href='?foo'.
    // Put this after the other two cases because it simplifies the booleans
    if (psychotic) {
      result.hostname = result.host = srcPath.shift();
      //occationaly the auth can get stuck only in host
      //this especially happens in cases like
      //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
      var authInHost = result.host && result.host.indexOf('@') > 0 ?
                       result.host.split('@') : false;
      if (authInHost) {
        result.auth = authInHost.shift();
        result.host = result.hostname = authInHost.shift();
      }
    }
    result.search = relative.search;
    result.query = relative.query;
    //to support http.request
    if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
      result.path = (result.pathname ? result.pathname : '') +
                    (result.search ? result.search : '');
    }
    result.href = result.format();
    return result;
  }

  if (!srcPath.length) {
    // no path at all.  easy.
    // we've already handled the other stuff above.
    result.pathname = null;
    //to support http.request
    if (result.search) {
      result.path = '/' + result.search;
    } else {
      result.path = null;
    }
    result.href = result.format();
    return result;
  }

  // if a url ENDs in . or .., then it must get a trailing slash.
  // however, if it ends in anything else non-slashy,
  // then it must NOT get a trailing slash.
  var last = srcPath.slice(-1)[0];
  var hasTrailingSlash = (
      (result.host || relative.host || srcPath.length > 1) &&
      (last === '.' || last === '..') || last === '');

  // strip single dots, resolve double dots to parent dir
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = srcPath.length; i >= 0; i--) {
    last = srcPath[i];
    if (last === '.') {
      srcPath.splice(i, 1);
    } else if (last === '..') {
      srcPath.splice(i, 1);
      up++;
    } else if (up) {
      srcPath.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (!mustEndAbs && !removeAllDots) {
    for (; up--; up) {
      srcPath.unshift('..');
    }
  }

  if (mustEndAbs && srcPath[0] !== '' &&
      (!srcPath[0] || srcPath[0].charAt(0) !== '/')) {
    srcPath.unshift('');
  }

  if (hasTrailingSlash && (srcPath.join('/').substr(-1) !== '/')) {
    srcPath.push('');
  }

  var isAbsolute = srcPath[0] === '' ||
      (srcPath[0] && srcPath[0].charAt(0) === '/');

  // put the host back
  if (psychotic) {
    result.hostname = result.host = isAbsolute ? '' :
                                    srcPath.length ? srcPath.shift() : '';
    //occationaly the auth can get stuck only in host
    //this especially happens in cases like
    //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
    var authInHost = result.host && result.host.indexOf('@') > 0 ?
                     result.host.split('@') : false;
    if (authInHost) {
      result.auth = authInHost.shift();
      result.host = result.hostname = authInHost.shift();
    }
  }

  mustEndAbs = mustEndAbs || (result.host && srcPath.length);

  if (mustEndAbs && !isAbsolute) {
    srcPath.unshift('');
  }

  if (!srcPath.length) {
    result.pathname = null;
    result.path = null;
  } else {
    result.pathname = srcPath.join('/');
  }

  //to support request.http
  if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
    result.path = (result.pathname ? result.pathname : '') +
                  (result.search ? result.search : '');
  }
  result.auth = relative.auth || result.auth;
  result.slashes = result.slashes || relative.slashes;
  result.href = result.format();
  return result;
};

Url.prototype.parseHost = function() {
  var host = this.host;
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) this.hostname = host;
};


/***/ }),

/***/ "./node_modules/url/util.js":
/*!**********************************!*\
  !*** ./node_modules/url/util.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  isString: function(arg) {
    return typeof(arg) === 'string';
  },
  isObject: function(arg) {
    return typeof(arg) === 'object' && arg !== null;
  },
  isNull: function(arg) {
    return arg === null;
  },
  isNullOrUndefined: function(arg) {
    return arg == null;
  }
};


/***/ }),

/***/ "./node_modules/webpack-require-http/ext/ext-css.js":
/*!*********************************************!*\
  !*** (webpack)-require-http/ext/ext-css.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(request, id) {
    return "\
        var fun=function(resolve, reject){\
            var link = document.createElement('link');\
            link.type = 'text/css';\
            link.rel = 'stylesheet';\
            link.href = '"+request+"';\
            link.id = '"+id+"';\
            (document.head || document.body).appendChild(link);\
            link.onload = function() {\
                typeof resolve=='function' && resolve();\
            };\
            link.onerror = function() {\
                typeof reject=='function' && reject();\
            };\
        };\
        if(JQ){\
            fun(resolve, reject);\
        }else{\
            promise = new Promise(function(resolve, reject) {\
                fun(resolve, reject);\
            });\
        }\
    ";
};

/***/ }),

/***/ "./node_modules/webpack-require-http/ext/ext-js.js":
/*!********************************************!*\
  !*** (webpack)-require-http/ext/ext-js.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(request, id) {
    return "\
            var fun=function(resolve, reject){\
                var script = document.createElement('script');\
                script.src = '"+request+"';\
                script.id = '"+id+"';\
                script.async = false;\
                (document.head || document.body).appendChild(script);\
                script.onload = function() {\
                    typeof resolve=='function' && resolve();\
                };\
                script.onerror = function() {\
                    typeof reject=='function' && reject();\
                };\
            };\
            if(JQ){\
                fun(resolve, reject);\
            }else{\
                promise = new Promise(function(resolve, reject) {\
                    fun(resolve, reject);\
                });\
            }\
    ";
};

/***/ }),

/***/ "./node_modules/webpack-require-http/index.js":
/*!***************************************!*\
  !*** (webpack)-require-http/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var url = __webpack_require__(/*! url */ "./node_modules/url/url.js");

var protocols = [
    'http:',
    'https:',
    'ftp:',
    'ftps:',
    'ws:'
];
var extMap = {
    'js':__webpack_require__(/*! ./ext/ext-js */ "./node_modules/webpack-require-http/ext/ext-js.js"),
    'css':__webpack_require__(/*! ./ext/ext-css */ "./node_modules/webpack-require-http/ext/ext-css.js")
};

var evalFunction = function(content) {
    return "(function() { "+content+"})()";
};

var existFunction = function(content, id) {
    return "\
        var promise = null;\
        var JQ='undefined'!=typeof jQuery;\
        var dfd = JQ && jQuery.Deferred();\
        var resolve = JQ && dfd.resolve;\
        var reject = JQ && dfd.reject;\
        if(document.getElementById('"+id+"')) {\
            if(JQ){\
                resolve();\
            }else{\
                promise = new Promise(function(resolve, reject) {\
                    resolve();\
                });\
            }\
        }\
        "+content+"\
\
        return JQ ? dfd.promise() : Promise.all([promise]);\
    ";
};

var getExtContent = function(pathname, request, options) {
    var ext = pathname.split('.').pop();
    if(options && options.ext) {
        ext = options.ext(pathname, request);
    }
    if(!extMap[ext]) {
        ext = 'js';
    }
    var id = __webpack_require__(/*! md5 */ "./node_modules/md5/md5.js")(request);
    return evalFunction(existFunction(extMap[ext].call(null, request, id), id));
};

var defaultExports = function(context, request, callback, options) {
    var rules = options ? options.rules : null;
    if(request && rules) {
        if(typeof rules == 'function') {
            request = rules.call(null, context, request);
        }
        if(typeof rules == 'object') {
            Object.keys(rules).forEach(function(rule, index) {
                request = request.replace(new RegExp(rule, 'ig'), rules[rule]);
            });
        }
    }

    if(request && request.indexOf('>') == 0) {
        request = request.substring(1);
        return callback(null, getExtContent(request, request, options));
    }else {
        var result = url.parse(request);
        if(protocols.indexOf(result.protocol) > -1) {
            var pathname = result.pathname || "";
            var content = getExtContent(pathname, request, options);
            return callback(null, content);
        }
    }

    return callback();
};

defaultExports.custom = function(option) {
    return function() {
        return defaultExports.apply(null, Array.prototype.slice.call(arguments).concat(option));
    };
};

module.exports = defaultExports;


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./resources/js/xin/app.js":
/*!*********************************!*\
  !*** ./resources/js/xin/app.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index */ "./resources/js/xin/index.js");
/* harmony import */ var _interceptor_request_default__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./interceptor/request.default */ "./resources/js/xin/interceptor/request.default.js");
/* harmony import */ var _vendor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./vendor */ "./resources/js/xin/vendor/index.js");
/* harmony import */ var _vue_layui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vue/layui */ "./resources/js/xin/vue/layui/index.js");
/* harmony import */ var _autorender__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./autorender */ "./resources/js/xin/autorender/index.js");




 //解析地址栏参数

Object.defineProperty(_index__WEBPACK_IMPORTED_MODULE_0__, 'query', {
  get: function get() {
    return _index__WEBPACK_IMPORTED_MODULE_0__["parseQuery"](window ? window.location.search.substr(1) : "");
  }
}); // 基本配置

_index__WEBPACK_IMPORTED_MODULE_0__["setConfig"]({
  map: {
    key: '4dcc5c8b956a746a3f6ab1c47a8f15d5'
  }
});
window.xin = _index__WEBPACK_IMPORTED_MODULE_0__;

/***/ }),

/***/ "./resources/js/xin/autorender/ajax.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/autorender/ajax.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");

$(function () {
  // get url address
  function resolveUri(othis, target) {
    return othis.attr('href') || othis.attr('url') || othis.attr('action') || othis.data('url') || target.attr('action') || target.attr('href') || target.attr('url') || target.data('url');
  } // build target options


  function buildOptions(othis, controlName) {
    return $.extend({
      confirm: true,
      confirmContent: '你确定要执行此操作吗？',
      mustTargetQuery: false,
      notTargetQueryMsg: '没有任何数据！',
      replaceParam: false,
      refresh: true,
      onData: null
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](othis, controlName));
  } // 执行操作


  function handler() {
    var othis = $(this),
        controlName = _index__WEBPACK_IMPORTED_MODULE_0__["getControlName"](this, "ajaxGet,ajaxPost"); // build target options

    var options = buildOptions(othis, controlName); // get a target jquery object.

    var target = $(options.target); // get url address

    var url = resolveUri(othis, target);

    if (!url) {
      console.error(othis.text() + "中使用ajax，请确保url 不能为空");
      return false;
    } // if support validator the verify whether it is correct


    if (_index__WEBPACK_IMPORTED_MODULE_0__["isSupportFormValidator"]() && target.get(0).tagName.toLowerCase() === "form") {
      var formValidOptions = othis.data('validator');
      if (!_index__WEBPACK_IMPORTED_MODULE_0__["formValid"](formValidOptions)) return false;
    } // execute ajax


    var ajaxActuator = makeAjaxActuator(url, othis, target, controlName, options); // if show confirm modal the click ok button after exec request

    if (options.confirm) {
      // fix layui select
      var oldValue = othis[0].oldValue;
      _index__WEBPACK_IMPORTED_MODULE_0__["showModal"]({
        content: options.confirmContent,
        showCancel: true
      }).then(function (res) {
        if (res.cancel) {
          updateLayuiRender(othis, oldValue);
          return;
        }

        ajaxActuator();
      });
    } else {
      // right away exec request
      ajaxActuator();
    }
  } // 更新渲染layui.form


  function updateLayuiRender(othis, oldValue) {
    // if (!othis.attr('lay-skin')) {
    // 	return;
    // }
    var type = "select";

    if ('INPUT' === othis.prop("tagName")) {
      othis.prop('checked', !othis.prop('checked'));
      type = "checkbox";
    } else if ('SELECT' === othis.prop("tagName")) {
      if (oldValue !== undefined) {
        othis.val(oldValue);
      }
    }

    var filter = othis.closest('.layui-form').attr('lay-filter');
    layui.form.render(type, filter);
  } // 获取执行器


  function makeAjaxActuator(url, othis, target, controlName, options) {
    // is get request
    var isGet = "ajaxGet" === controlName; // elem state

    var elemStateActuator = makeElemStateActuator(othis);

    var isFormElem = function isFormElem() {
      return ['INPUT', 'SELECT'].indexOf(othis.prop("tagName")) !== -1;
    };

    return function () {
      var method = isGet ? "GET" : "POST"; // 检查是否有请求参数回调器

      var params = options.onRequest ? options.onRequest.call(othis) : '';

      if (typeof params !== 'string') {
        params = $.param(params);
      } // 如果是form元素 则检查name属性


      if (isFormElem()) {
        var fieldName = othis.attr('name');

        if (fieldName) {
          params += (!params || params.charAt(params.length - 1) === "&" ? "" : "&") + fieldName + "=" + encodeURIComponent(othis.val());
        }
      } // 03 13 18 23 31    04 07
      // 是否是替换现有参数，如果不是则是追加参数


      if (options.replaceParam !== true) {
        var targetParam = target.serialize();

        if (targetParam !== "") {
          params = targetParam + "&" + params;
        }
      } // 如果参数为空并且要求必须有参数时，则提示错误信息


      if (!params && options.mustTargetQuery) {
        _index__WEBPACK_IMPORTED_MODULE_0__["showModal"]({
          icon: 'error',
          content: options.notTargetQueryMsg
        });
        return;
      } // disable elem state.


      elemStateActuator.disabled(); // async request server

      _index__WEBPACK_IMPORTED_MODULE_0__["request"]({
        url: url,
        type: method,
        data: params
      }).then(function (res) {
        othis.trigger('success', [res]); // 是否存在模板编译字符串

        if (options.template) {
          var template = $(options.template);
          var templateDist = $(options.templateDist || othis);
          var content = _index__WEBPACK_IMPORTED_MODULE_0__["tpl"]({
            template: template.html(),
            data: res.data
          });
          templateDist.html(content);
        } else if (options.refresh !== false) {
          // 是否需要刷新浏览器
          setTimeout(function () {
            if (res.url) {
              location.href = res.url;
            } else {
              location.reload();
            }
          }, 1000); //res.wait ? res.wait * 1000 : 1000
        }
      }, function (err) {
        othis.trigger('error', [err]);
      }).always(function () {
        elemStateActuator.enable();
      });
    };
  } // 获取元素状态


  function makeElemStateActuator(target) {
    return {
      // statement propDisabled function Used to disable button
      disabled: function disabled() {
        target.addClass('disabled btn-disabled layui-btn-disabled').prop('disabled', true);
      },
      // statement propEnable function User to enable button
      enable: function enable() {
        target.removeClass('disabled btn-disabled layui-btn-disabled').prop('disabled', false);
      }
    };
  } //ajax config


  $(document).on('click change blur', '[data-ajax-get],[data-ajax-post]', function (e) {
    if (e.type === 'focusout' && !this.dataset.enableFocus) {
      return false;
    }

    _index__WEBPACK_IMPORTED_MODULE_0__["safeCallback"](this, handler);
    return false;
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/autoclear.js":
/*!**************************************************!*\
  !*** ./resources/js/xin/autorender/autoclear.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// handler
$(function () {
  var handler = function handler() {
    var self = $(this);

    if (self.val() === '') {
      self.next('i').hide();
    } else {
      self.next('i').show();
    }
  };

  $(document).on('click', 'input[data-autoclear]+i', function () {
    var self = $(this);
    self.prev('input').val('');
    self.hide();
  });
  $(document).on('keyup', 'input[data-autoclear]', handler);
  $('input[autoclear]').each(handler);
});

/***/ }),

/***/ "./resources/js/xin/autorender/child-window.js":
/*!*****************************************************!*\
  !*** ./resources/js/xin/autorender/child-window.js ***!
  \*****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 // layer window

$(function () {
  $(document).on('click', '[data-open]', function () {
    var self = $(this);
    var url = self.attr('href') || self.attr('url') || self.attr('action') || self.data('url');
    var options = _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'open');
    options.content = options.content || url;
    _index__WEBPACK_IMPORTED_MODULE_0__["open"](options);
    return false;
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/colorpicker.js":
/*!****************************************************!*\
  !*** ./resources/js/xin/autorender/colorpicker.js ***!
  \****************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //color picker

$(function () {
  $('[data-colorpicker]').each(function () {
    var options = $.extend({
      elem: this,
      done: function done(color) {
        if (this.target) {
          $(this.target).val(color);
        }
      }
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](this, 'colorpicker'));
    _index__WEBPACK_IMPORTED_MODULE_0__["colorpicker"](options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/datetime.js":
/*!*************************************************!*\
  !*** ./resources/js/xin/autorender/datetime.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //init date or time

$(function () {
  $('[data-datetime]').each(function () {
    //type:year,month,date,time,datetime
    //range:范围选择,true //或 range: '~' 来自定义分割字符
    var options = $.extend({
      elem: this,
      trigger: 'click',
      calendar: true
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](this, 'datetime'));
    _index__WEBPACK_IMPORTED_MODULE_0__["datepicker"](options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/editor.js":
/*!***********************************************!*\
  !*** ./resources/js/xin/autorender/editor.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //editor

$(function () {
  $('[data-editor]').each(function () {
    var self = $(this);
    var options = $.extend({
      name: self.attr('name')
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'editor'), {
      el: this
    });
    _index__WEBPACK_IMPORTED_MODULE_0__["editor"](options).then(function (editor) {
      self[0].editor = editor;
    });
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/img-error.js":
/*!**************************************************!*\
  !*** ./resources/js/xin/autorender/img-error.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //img error auto

$(function () {
  $('img[data-error]').each(function () {
    var self = $(this);
    _index__WEBPACK_IMPORTED_MODULE_0__["imgError"]({
      el: this,
      url: self.data('error')
    });
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/index.js":
/*!**********************************************!*\
  !*** ./resources/js/xin/autorender/index.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ajax__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ajax */ "./resources/js/xin/autorender/ajax.js");
/* harmony import */ var _autoclear__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./autoclear */ "./resources/js/xin/autorender/autoclear.js");
/* harmony import */ var _autoclear__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_autoclear__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _child_window__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./child-window */ "./resources/js/xin/autorender/child-window.js");
/* harmony import */ var _colorpicker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./colorpicker */ "./resources/js/xin/autorender/colorpicker.js");
/* harmony import */ var _datetime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./datetime */ "./resources/js/xin/autorender/datetime.js");
/* harmony import */ var _editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editor */ "./resources/js/xin/autorender/editor.js");
/* harmony import */ var _img_error__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./img-error */ "./resources/js/xin/autorender/img-error.js");
/* harmony import */ var _input_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./input-mask */ "./resources/js/xin/autorender/input-mask.js");
/* harmony import */ var _input_mask__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_input_mask__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _layui_controls__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./layui-controls */ "./resources/js/xin/autorender/layui-controls.js");
/* harmony import */ var _layui_controls__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_layui_controls__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _map__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./map */ "./resources/js/xin/autorender/map.js");
/* harmony import */ var _masonry__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./masonry */ "./resources/js/xin/autorender/masonry.js");
/* harmony import */ var _preview__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./preview */ "./resources/js/xin/autorender/preview.js");
/* harmony import */ var _sortable__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./sortable */ "./resources/js/xin/autorender/sortable.js");
/* harmony import */ var _toggle_checkbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./toggle-checkbox */ "./resources/js/xin/autorender/toggle-checkbox.js");
/* harmony import */ var _tooltips__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./tooltips */ "./resources/js/xin/autorender/tooltips.js");
/* harmony import */ var _treetable__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./treetable */ "./resources/js/xin/autorender/treetable.js");
/* harmony import */ var _uploader__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./uploader */ "./resources/js/xin/autorender/uploader.js");


















/***/ }),

/***/ "./resources/js/xin/autorender/input-mask.js":
/*!***************************************************!*\
  !*** ./resources/js/xin/autorender/input-mask.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//input-mask
$(function () {
  if ($.fn.inputmask) {
    $('[data-inputmask]').inputmask();
  }
});

/***/ }),

/***/ "./resources/js/xin/autorender/layui-controls.js":
/*!*******************************************************!*\
  !*** ./resources/js/xin/autorender/layui-controls.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// 渲染 layui 相关组件
$(function () {
  if (!window.layui) {
    return;
  } // switch


  layui.form.on('switch', function (data) {
    // const self = $(data.elem);
    // let value = self.attr('lay-value');
    // if (value) {
    // 	value = value.split('|', 2);
    // 	self.val(value[data.elem.checked ? 0 : 1]);
    // }
    $(data.elem).trigger('change');
  }); // select

  layui.form.on('select', function (data) {
    $(data.elem).trigger('change');
    data.elem.oldValue = data.elem.value;
  });
  lay('.layui-form select').each(function (index, it) {
    it.oldValue = it.value;
  });
  layui.form.render(); //轮播图

  $('[data-carousel]').each(function () {
    var options = $.extend({
      elem: this,
      width: '100%' //设置容器宽度
      // arrow: 'always' //始终显示箭头
      //,anim: 'updown' //切换动画方式

    }, xin.getDataOptions(this, 'carousel'));
    layui.carousel.render(options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/map.js":
/*!********************************************!*\
  !*** ./resources/js/xin/autorender/map.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 // 地图

$(function () {
  $('[data-map]').each(function () {
    var _this = this;

    var self = $(this),
        markers = self.data('markers') || [],
        circles = self.data('circles') || [],
        center = self.data("center") || "",
        name = self.data('name') || "location";
    var options = $.extend(_index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'map'), {
      el: this,
      markers: markers,
      circles: circles,
      center: center,
      name: name
    });
    _index__WEBPACK_IMPORTED_MODULE_0__["map"](options).then(function (map) {
      _this.map = map;
    });
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/masonry.js":
/*!************************************************!*\
  !*** ./resources/js/xin/autorender/masonry.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //masonry

$(function () {
  $('[data-masonry]').each(function () {
    var self = $(this);
    var options = $.extend({
      gutter: 30,
      isAnimated: true
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'masonry'));
    self.masonry(options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/preview.js":
/*!************************************************!*\
  !*** ./resources/js/xin/autorender/preview.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //preview

$(function () {
  $(document).on('click', '[data-preview]', function (e) {
    var options = _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](this, 'preview');

    if (!options.urls) {
      options.urls = [];
      var warpper = $(this);
      warpper.find('img').each(function (index, item) {
        options.urls.push(item.src);

        if (e.target === item) {
          options.current = index;
        }
      });
    }

    _index__WEBPACK_IMPORTED_MODULE_0__["preview"](options);
    return false;
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/sortable.js":
/*!*************************************************!*\
  !*** ./resources/js/xin/autorender/sortable.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //sortable

$(function () {
  var sortable = $('[data-sortable]');

  if (!sortable.length) {
    return;
  }

  _index__WEBPACK_IMPORTED_MODULE_0__["dynamicImport"](location.origin + '/vendor/sortable/Sortable.min.js').then(function () {
    sortable.each(function () {
      var self = $(this);
      var options = $.extend({
        connectWith: "[data-sortable]",
        cursor: "move"
      }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'sortable'));
      new Sortable(self[0], _index__WEBPACK_IMPORTED_MODULE_0__["assign"]({
        animation: 150
      }, options));
    });
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/toggle-checkbox.js":
/*!********************************************************!*\
  !*** ./resources/js/xin/autorender/toggle-checkbox.js ***!
  \********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //init all check

$(function () {
  $("[data-choice-check]").each(function () {
    _index__WEBPACK_IMPORTED_MODULE_0__["choiceCheckBox"](this);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/tooltips.js":
/*!*************************************************!*\
  !*** ./resources/js/xin/autorender/tooltips.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 // Tooltips

$(function () {
  $(document).on('mouseenter', '[data-tip]', function () {
    var self = $(this);
    var options = $.extend({
      time: 1000,
      tips: 3
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'tip'));
    options.content = self.data('tip');
    if (!options.content) return;
    options.el = this;
    _index__WEBPACK_IMPORTED_MODULE_0__["tooltip"](options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/treetable.js":
/*!**************************************************!*\
  !*** ./resources/js/xin/autorender/treetable.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //treetable

$(function () {
  $('[data-tree-table]').each(function () {
    var self = $(this);
    var options = $.extend({
      expandable: true,
      stringCollapse: '闭合',
      stringExpand: '展开',
      initialState: 'expanded'
    }, _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](self, 'treeTable'));
    self.treetable(options);
  });
});

/***/ }),

/***/ "./resources/js/xin/autorender/uploader.js":
/*!*************************************************!*\
  !*** ./resources/js/xin/autorender/uploader.js ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../index */ "./resources/js/xin/index.js");
 //uploader

$(function () {
  $('[data-img-uploader]').each(function () {
    var options = _index__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"](this, 'imgUploader');
    options.el = this;
    _index__WEBPACK_IMPORTED_MODULE_0__["uploader"](options);
  });
});

/***/ }),

/***/ "./resources/js/xin/core/base.js":
/*!***************************************!*\
  !*** ./resources/js/xin/core/base.js ***!
  \***************************************/
/*! exports provided: assign, optimize, clone, parseUrl, parseQuery, parseName, getFileExtensionName, safeCallback */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assign", function() { return assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "optimize", function() { return optimize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clone", function() { return clone; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseUrl", function() { return parseUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseQuery", function() { return parseQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseName", function() { return parseName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFileExtensionName", function() { return getFileExtensionName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "safeCallback", function() { return safeCallback; });
/**
 * 合并一个对象
 * @param {*} target
 * @return {*}
 */
function assign(target) {
  'use strict';

  if (target == null) {
    throw new TypeError('Cannot convert undefined or null to object');
  }

  target = Object(target);

  for (var index = 1; index < arguments.length; index++) {
    var source = arguments[index];

    if (source != null) {
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
  }

  return target;
}
/**
 * 优化对象
 * @param {*} obj
 * @return {*}
 */

function optimize(obj) {
  for (var k in obj) {
    if (obj.hasOwnProperty(k) && obj[k] === undefined) {
      delete obj[k];
    }
  }

  return obj;
}
/**
 * 克隆对象
 * @param obj
 * @return {*}
 */

function clone(obj) {
  return JSON.parse(JSON.stringify(obj));
}
/**
 * 解析URL
 * @param  {string} url 被解析的URL
 * @return {object}     解析后的数据
 */

function parseUrl(url) {
  var parse = url.match(/^(?:([a-z]+):\/\/)?([\w-]+(?:\.[\w-]+)+)?(?::(\d+))?([\w-\/.]+)?(?:\?((?:\w+=[^#&=\/]*)?(?:&\w+=[^#&=\/]*)*))?(?:#([\w-]+))?$/i);
  if (!parse) throw new Error("url格式不正确！");
  return {
    scheme: parse[1],
    host: parse[2],
    port: parse[3],
    path: parse[4],
    query: parse[5],
    fragment: parse[6]
  };
}
/**
 * 解析Url Query字符串
 * @param {string} str
 * @returns {{}}
 */

function parseQuery(str) {
  if (!str) return {};
  var value = str.split("&"),
      vars = {},
      param;

  for (var val in value) {
    param = value[val].split("=");
    vars[param[0]] = param[1];
  }

  return vars;
}
/**
 * 解析名称（下划线与驼峰互转）
 * @param {string} name
 * @param {boolean} [type] 转换类型
 * @returns {*}
 */

function parseName(name, type) {
  if (type) {
    /* 下划线转驼峰 */
    name.replace(/_([a-z])/g, function ($0, $1) {
      return $1.toUpperCase();
    });
    /* 首字母大写 */

    name.replace(/[a-z]/, function ($0) {
      return $0.toUpperCase();
    });
  } else {
    /* 大写字母转小写 */
    name = name.replace(/[A-Z]/g, function ($0) {
      return "_" + $0.toLowerCase();
    });
    /* 去掉首字符的下划线 */

    if (0 === name.indexOf("_")) {
      name = name.substr(1);
    }
  }

  return name;
}
/**
 * 获取文件后缀名
 * @param {string} file
 * @returns {string}
 */

function getFileExtensionName(file) {
  var index = file.lastIndexOf(".");
  return file.substring(index + 1, file.length);
}
/**
 * 安全调用函数
 * @param {*} thisArg
 * @param {function} func
 * @param {array} [params]
 */

function safeCallback(thisArg, func) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  if (!func || typeof func !== "function") {
    return;
  }

  try {
    return func.apply(thisArg, params);
  } catch (e) {
    console.error(e);
  }
}

/***/ }),

/***/ "./resources/js/xin/core/config.js":
/*!*****************************************!*\
  !*** ./resources/js/xin/core/config.js ***!
  \*****************************************/
/*! exports provided: setConfig, getConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setConfig", function() { return setConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getConfig", function() { return getConfig; });
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "./resources/js/xin/core/base.js");

var globalConfig = {};
/**
 * 设置config
 * @param {*} config
 */

function setConfig(config) {
  return Object(_base__WEBPACK_IMPORTED_MODULE_0__["assign"])(globalConfig, config);
}
/**
 * 获取配置信息
 * @param {string} key
 * @param defaultValue
 * @return {*}
 */

function getConfig(key) {
  var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return globalConfig[key] !== undefined && globalConfig[key] !== null ? globalConfig[key] : defaultValue;
}

/***/ }),

/***/ "./resources/js/xin/core/date.js":
/*!***************************************!*\
  !*** ./resources/js/xin/core/date.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return date; });
/**
 * 格式化日期
 * @param {string} format
 * @param {Number|Date} time
 * @return {string}
 */
function date(format, time) {
  time = time || new Date();
  time = time instanceof Date ? time : new Date(time);
  var o = {
    "M+": time.getMonth() + 1,
    //月份
    "d+": time.getDate(),
    //日
    "h+": time.getHours(),
    //小时
    "m+": time.getMinutes(),
    //分
    "s+": time.getSeconds(),
    //秒
    "q+": Math.floor((time.getMonth() + 3) / 3),
    //季度
    "S": time.getMilliseconds() //毫秒

  };

  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
  }

  for (var k in o) {
    if (new RegExp("(" + k + ")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    }
  }

  return format;
}
/**
 * 快速格式化日期
 * @param {Number|Date} [time]
 * @param {boolean} [isShowSeconds]
 */

date.datetime = function (time, isShowSeconds) {
  isShowSeconds = isShowSeconds || false;
  return date("yyyy-MM-dd hh:mm" + (isShowSeconds ? ":ss" : ""), time);
};
/**
 * 只格式化日期部分
 * @param {Number|Date} [time]
 */


date.date = function (time) {
  return date("yyyy-MM-dd", time);
};
/**
 * 只格式化时间部分
 * @param {Number|Date} [time]
 * @param {boolean} [isShowSeconds]
 */


date.time = function (time, isShowSeconds) {
  isShowSeconds = isShowSeconds || false;
  return date("hh:mm" + (isShowSeconds ? ":ss" : ""), time);
};
/**
 * 获取相对时间
 * @param timeStamp
 * @return {string}
 */


date.relative = function (timeStamp) {
  var currentTime = new Date().getTime(); // 判断传入时间戳是否早于当前时间戳

  var IS_EARLY = timeStamp <= currentTime; // 获取两个时间戳差值

  var diff = currentTime - timeStamp; // 如果IS_EARLY为false则差值取反

  if (!IS_EARLY) {
    diff = -diff;
  }

  var dirStr = IS_EARLY ? '前' : '后';
  var resStr;

  if (diff < 1000) {
    resStr = '刚刚';
  } else if (diff < 60000) {
    // 少于等于59秒
    resStr = Math.floor(diff / 1000) + '秒' + dirStr;
  } else if (diff >= 60000 && diff < 3600000) {
    // 多于59秒，少于等于59分钟59秒
    resStr = Math.floor(diff / 60000) + '分钟' + dirStr;
  } else if (diff >= 3600000 && diff < 86400000) {
    // 多于59分钟59秒，少于等于23小时59分钟59秒
    resStr = Math.floor(diff / 3600000) + '小时' + dirStr;
  } else if (diff >= 86400000 && diff < 2623860000) {
    // 多于23小时59分钟59秒，少于等于29天59分钟59秒
    resStr = Math.floor(diff / 86400000) + '天' + dirStr;
  } else if (diff >= 2623860000 && diff <= 31567860000 && IS_EARLY) {
    // 多于29天59分钟59秒，少于364天23小时59分钟59秒，且传入的时间戳早于当前
    resStr = date('MM-dd hh:mm', timeStamp);
  } else {
    resStr = date.date(timeStamp);
  }

  return resStr;
};

/***/ }),

/***/ "./resources/js/xin/core/errors.js":
/*!*****************************************!*\
  !*** ./resources/js/xin/core/errors.js ***!
  \*****************************************/
/*! exports provided: CustomError, InvalidParamsError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomError", function() { return CustomError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidParamsError", function() { return InvalidParamsError; });
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var CustomError = /*#__PURE__*/function (_Error) {
  _inherits(CustomError, _Error);

  var _super = _createSuper(CustomError);

  function CustomError(message) {
    var _this;

    _classCallCheck(this, CustomError);

    _this = _super.call(this, message);
    _this.name = "CustomError";
    return _this;
  }

  return CustomError;
}( /*#__PURE__*/_wrapNativeSuper(Error));
var InvalidParamsError = /*#__PURE__*/function (_CustomError) {
  _inherits(InvalidParamsError, _CustomError);

  var _super2 = _createSuper(InvalidParamsError);

  function InvalidParamsError(message) {
    _classCallCheck(this, InvalidParamsError);

    return _super2.call(this, message);
  }

  return InvalidParamsError;
}(CustomError);

/***/ }),

/***/ "./resources/js/xin/core/index.js":
/*!****************************************!*\
  !*** ./resources/js/xin/core/index.js ***!
  \****************************************/
/*! exports provided: assign, optimize, clone, parseUrl, parseQuery, parseName, getFileExtensionName, safeCallback, setConfig, getConfig, date, dynamicImport, mimeMap, getMimeType, getMimeExtension, getDataOptions, getControlName, setValue, setChecked, focusInsert, formValid, isSupportFormValidator, getScreenType, fullscreen, openUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "./resources/js/xin/core/base.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "assign", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["assign"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "optimize", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["optimize"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "clone", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["clone"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseUrl", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["parseUrl"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseQuery", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["parseQuery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseName", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["parseName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getFileExtensionName", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["getFileExtensionName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "safeCallback", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["safeCallback"]; });

/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config */ "./resources/js/xin/core/config.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setConfig", function() { return _config__WEBPACK_IMPORTED_MODULE_1__["setConfig"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getConfig", function() { return _config__WEBPACK_IMPORTED_MODULE_1__["getConfig"]; });

/* harmony import */ var _date__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./date */ "./resources/js/xin/core/date.js");
/* empty/unused harmony star reexport *//* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "date", function() { return _date__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _require__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./require */ "./resources/js/xin/core/require.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dynamicImport", function() { return _require__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _mime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mime */ "./resources/js/xin/core/mime.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "mimeMap", function() { return _mime__WEBPACK_IMPORTED_MODULE_4__["mimeMap"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMimeType", function() { return _mime__WEBPACK_IMPORTED_MODULE_4__["getMimeType"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMimeExtension", function() { return _mime__WEBPACK_IMPORTED_MODULE_4__["getMimeExtension"]; });

/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ui */ "./resources/js/xin/core/ui.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getDataOptions", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["getDataOptions"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getControlName", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["getControlName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setValue", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["setValue"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setChecked", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["setChecked"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "focusInsert", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["focusInsert"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "formValid", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["formValid"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isSupportFormValidator", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["isSupportFormValidator"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getScreenType", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["getScreenType"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fullscreen", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["fullscreen"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "openUrl", function() { return _ui__WEBPACK_IMPORTED_MODULE_5__["openUrl"]; });









/***/ }),

/***/ "./resources/js/xin/core/mime.js":
/*!***************************************!*\
  !*** ./resources/js/xin/core/mime.js ***!
  \***************************************/
/*! exports provided: mimeMap, getMimeType, getMimeExtension */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mimeMap", function() { return mimeMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMimeType", function() { return getMimeType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMimeExtension", function() { return getMimeExtension; });
var mimeMap = {
  "application/andrew-inset": ["ez"],
  "application/applixware": ["aw"],
  "application/atom+xml": ["atom"],
  "application/atomcat+xml": ["atomcat"],
  "application/atomsvc+xml": ["atomsvc"],
  "application/bdoc": ["bdoc"],
  "application/ccxml+xml": ["ccxml"],
  "application/cdmi-capability": ["cdmia"],
  "application/cdmi-container": ["cdmic"],
  "application/cdmi-domain": ["cdmid"],
  "application/cdmi-object": ["cdmio"],
  "application/cdmi-queue": ["cdmiq"],
  "application/cu-seeme": ["cu"],
  "application/dash+xml": ["mpd"],
  "application/davmount+xml": ["davmount"],
  "application/docbook+xml": ["dbk"],
  "application/dssc+der": ["dssc"],
  "application/dssc+xml": ["xdssc"],
  "application/ecmascript": ["ecma", "es"],
  "application/emma+xml": ["emma"],
  "application/epub+zip": ["epub"],
  "application/exi": ["exi"],
  "application/font-tdpfr": ["pfr"],
  "application/geo+json": ["geojson"],
  "application/gml+xml": ["gml"],
  "application/gpx+xml": ["gpx"],
  "application/gxf": ["gxf"],
  "application/gzip": ["gz"],
  "application/hjson": ["hjson"],
  "application/hyperstudio": ["stk"],
  "application/inkml+xml": ["ink", "inkml"],
  "application/ipfix": ["ipfix"],
  "application/java-archive": ["jar", "war", "ear"],
  "application/java-serialized-object": ["ser"],
  "application/java-vm": ["class"],
  "application/javascript": ["js", "mjs"],
  "application/json": ["json", "map"],
  "application/json5": ["json5"],
  "application/jsonml+json": ["jsonml"],
  "application/ld+json": ["jsonld"],
  "application/lost+xml": ["lostxml"],
  "application/mac-binhex40": ["hqx"],
  "application/mac-compactpro": ["cpt"],
  "application/mads+xml": ["mads"],
  "application/manifest+json": ["webmanifest"],
  "application/marc": ["mrc"],
  "application/marcxml+xml": ["mrcx"],
  "application/mathematica": ["ma", "nb", "mb"],
  "application/mathml+xml": ["mathml"],
  "application/mbox": ["mbox"],
  "application/mediaservercontrol+xml": ["mscml"],
  "application/metalink+xml": ["metalink"],
  "application/metalink4+xml": ["meta4"],
  "application/mets+xml": ["mets"],
  "application/mods+xml": ["mods"],
  "application/mp21": ["m21", "mp21"],
  "application/mp4": ["mp4s", "m4p"],
  "application/msword": ["doc", "dot"],
  "application/mxf": ["mxf"],
  "application/n-quads": ["nq"],
  "application/n-triples": ["nt"],
  "application/octet-stream": ["bin", "dms", "lrf", "mar", "so", "dist", "distz", "pkg", "bpk", "dump", "elc", "deploy", "exe", "dll", "deb", "dmg", "iso", "img", "msi", "msp", "msm", "buffer"],
  "application/oda": ["oda"],
  "application/oebps-package+xml": ["opf"],
  "application/ogg": ["ogx"],
  "application/omdoc+xml": ["omdoc"],
  "application/onenote": ["onetoc", "onetoc2", "onetmp", "onepkg"],
  "application/oxps": ["oxps"],
  "application/patch-ops-error+xml": ["xer"],
  "application/pdf": ["pdf"],
  "application/pgp-encrypted": ["pgp"],
  "application/pgp-signature": ["asc", "sig"],
  "application/pics-rules": ["prf"],
  "application/pkcs10": ["p10"],
  "application/pkcs7-mime": ["p7m", "p7c"],
  "application/pkcs7-signature": ["p7s"],
  "application/pkcs8": ["p8"],
  "application/pkix-attr-cert": ["ac"],
  "application/pkix-cert": ["cer"],
  "application/pkix-crl": ["crl"],
  "application/pkix-pkipath": ["pkipath"],
  "application/pkixcmp": ["pki"],
  "application/pls+xml": ["pls"],
  "application/postscript": ["ai", "eps", "ps"],
  "application/pskc+xml": ["pskcxml"],
  "application/raml+yaml": ["raml"],
  "application/rdf+xml": ["rdf", "owl"],
  "application/reginfo+xml": ["rif"],
  "application/relax-ng-compact-syntax": ["rnc"],
  "application/resource-lists+xml": ["rl"],
  "application/resource-lists-diff+xml": ["rld"],
  "application/rls-services+xml": ["rs"],
  "application/rpki-ghostbusters": ["gbr"],
  "application/rpki-manifest": ["mft"],
  "application/rpki-roa": ["roa"],
  "application/rsd+xml": ["rsd"],
  "application/rss+xml": ["rss"],
  "application/rtf": ["rtf"],
  "application/sbml+xml": ["sbml"],
  "application/scvp-cv-request": ["scq"],
  "application/scvp-cv-response": ["scs"],
  "application/scvp-vp-request": ["spq"],
  "application/scvp-vp-response": ["spp"],
  "application/sdp": ["sdp"],
  "application/set-payment-initiation": ["setpay"],
  "application/set-registration-initiation": ["setreg"],
  "application/shf+xml": ["shf"],
  "application/sieve": ["siv", "sieve"],
  "application/smil+xml": ["smi", "smil"],
  "application/sparql-query": ["rq"],
  "application/sparql-results+xml": ["srx"],
  "application/srgs": ["gram"],
  "application/srgs+xml": ["grxml"],
  "application/sru+xml": ["sru"],
  "application/ssdl+xml": ["ssdl"],
  "application/ssml+xml": ["ssml"],
  "application/tei+xml": ["tei", "teicorpus"],
  "application/thraud+xml": ["tfi"],
  "application/timestamped-data": ["tsd"],
  "application/voicexml+xml": ["vxml"],
  "application/wasm": ["wasm"],
  "application/widget": ["wgt"],
  "application/winhlp": ["hlp"],
  "application/wsdl+xml": ["wsdl"],
  "application/wspolicy+xml": ["wspolicy"],
  "application/xaml+xml": ["xaml"],
  "application/xcap-diff+xml": ["xdf"],
  "application/xenc+xml": ["xenc"],
  "application/xhtml+xml": ["xhtml", "xht"],
  "application/xml": ["xml", "xsl", "xsd", "rng"],
  "application/xml-dtd": ["dtd"],
  "application/xop+xml": ["xop"],
  "application/xproc+xml": ["xpl"],
  "application/xslt+xml": ["xslt"],
  "application/xspf+xml": ["xspf"],
  "application/xv+xml": ["mxml", "xhvml", "xvml", "xvm"],
  "application/yang": ["yang"],
  "application/yin+xml": ["yin"],
  "application/zip": ["zip"],
  "audio/3gpp": ["*3gpp"],
  "audio/adpcm": ["adp"],
  "audio/basic": ["au", "snd"],
  "audio/midi": ["mid", "midi", "kar", "rmi"],
  "audio/mp3": ["*mp3"],
  "audio/mp4": ["m4a", "mp4a"],
  "audio/mpeg": ["mpga", "mp2", "mp2a", "mp3", "m2a", "m3a"],
  "audio/ogg": ["oga", "ogg", "spx"],
  "audio/s3m": ["s3m"],
  "audio/silk": ["sil"],
  "audio/wav": ["wav"],
  "audio/wave": ["*wav"],
  "audio/webm": ["weba"],
  "audio/xm": ["xm"],
  "font/collection": ["ttc"],
  "font/otf": ["otf"],
  "font/ttf": ["ttf"],
  "font/woff": ["woff"],
  "font/woff2": ["woff2"],
  "image/aces": ["exr"],
  "image/apng": ["apng"],
  "image/bmp": ["bmp"],
  "image/cgm": ["cgm"],
  "image/dicom-rle": ["drle"],
  "image/emf": ["emf"],
  "image/fits": ["fits"],
  "image/g3fax": ["g3"],
  "image/gif": ["gif"],
  "image/heic": ["heic"],
  "image/heic-sequence": ["heics"],
  "image/heif": ["heif"],
  "image/heif-sequence": ["heifs"],
  "image/ief": ["ief"],
  "image/jls": ["jls"],
  "image/jp2": ["jp2", "jpg2"],
  "image/jpeg": ["jpeg", "jpg", "jpe"],
  "image/jpm": ["jpm"],
  "image/jpx": ["jpx", "jpf"],
  "image/jxr": ["jxr"],
  "image/ktx": ["ktx"],
  "image/png": ["png"],
  "image/sgi": ["sgi"],
  "image/svg+xml": ["svg", "svgz"],
  "image/t38": ["t38"],
  "image/tiff": ["tif", "tiff"],
  "image/tiff-fx": ["tfx"],
  "image/webp": ["webp"],
  "image/wmf": ["wmf"],
  "message/disposition-notification": ["disposition-notification"],
  "message/global": ["u8msg"],
  "message/global-delivery-status": ["u8dsn"],
  "message/global-disposition-notification": ["u8mdn"],
  "message/global-headers": ["u8hdr"],
  "message/rfc822": ["eml", "mime"],
  "model/3mf": ["3mf"],
  "model/gltf+json": ["gltf"],
  "model/gltf-binary": ["glb"],
  "model/iges": ["igs", "iges"],
  "model/mesh": ["msh", "mesh", "silo"],
  "model/stl": ["stl"],
  "model/vrml": ["wrl", "vrml"],
  "model/x3d+binary": ["*x3db", "x3dbz"],
  "model/x3d+fastinfoset": ["x3db"],
  "model/x3d+vrml": ["*x3dv", "x3dvz"],
  "model/x3d+xml": ["x3d", "x3dz"],
  "model/x3d-vrml": ["x3dv"],
  "text/cache-manifest": ["appcache", "manifest"],
  "text/calendar": ["ics", "ifb"],
  "text/coffeescript": ["coffee", "litcoffee"],
  "text/css": ["css"],
  "text/csv": ["csv"],
  "text/html": ["html", "htm", "shtml"],
  "text/jade": ["jade"],
  "text/jsx": ["jsx"],
  "text/less": ["less"],
  "text/markdown": ["markdown", "md"],
  "text/mathml": ["mml"],
  "text/mdx": ["mdx"],
  "text/n3": ["n3"],
  "text/plain": ["txt", "text", "conf", "def", "list", "log", "in", "ini"],
  "text/richtext": ["rtx"],
  "text/rtf": ["*rtf"],
  "text/sgml": ["sgml", "sgm"],
  "text/shex": ["shex"],
  "text/slim": ["slim", "slm"],
  "text/stylus": ["stylus", "styl"],
  "text/tab-separated-values": ["tsv"],
  "text/troff": ["t", "tr", "roff", "man", "me", "ms"],
  "text/turtle": ["ttl"],
  "text/uri-list": ["uri", "uris", "urls"],
  "text/vcard": ["vcard"],
  "text/vtt": ["vtt"],
  "text/xml": ["*xml"],
  "text/yaml": ["yaml", "yml"],
  "video/3gpp": ["3gp", "3gpp"],
  "video/3gpp2": ["3g2"],
  "video/h261": ["h261"],
  "video/h263": ["h263"],
  "video/h264": ["h264"],
  "video/jpeg": ["jpgv"],
  "video/jpm": ["*jpm", "jpgm"],
  "video/mj2": ["mj2", "mjp2"],
  "video/mp2t": ["ts"],
  "video/mp4": ["mp4", "mp4v", "mpg4"],
  "video/mpeg": ["mpeg", "mpg", "mpe", "m1v", "m2v"],
  "video/ogg": ["ogv"],
  "video/quicktime": ["qt", "mov"],
  "video/webm": ["webm"]
};

var extensions = function () {
  var result = {};

  for (var x in mimeMap) {
    if (!mimeMap.hasOwnProperty(x)) {
      continue;
    }

    result[x] = mimeMap[x].map(function (t) {
      return t.toLowerCase();
    });
  }

  return result;
}();
/**
 * Lookup a mime type based on extension
 */


function getMimeType(path) {
  path = String(path);
  var last = path.replace(/^.*[/\\]/, '').toLowerCase();
  var ext = last.replace(/^.*\./, '').toLowerCase();
  var hasPath = last.length < path.length;
  var hasDot = ext.length < last.length - 1;
  return (hasDot || !hasPath) && this.mimeMap[ext] || null;
}
/**
 * Return file extension associated with a mime type
 */

function getMimeExtension(type) {
  type = /^\s*([^;\s]*)/.test(type) && RegExp.$1;
  return type && extensions[type.toLowerCase()] || null;
}

/***/ }),

/***/ "./resources/js/xin/core/require.js":
/*!******************************************!*\
  !*** ./resources/js/xin/core/require.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return dynamicImport; });
var webpackRequireHttp = __webpack_require__(/*! webpack-require-http */ "./node_modules/webpack-require-http/index.js");

var customImport = webpackRequireHttp.custom({
  rules: function rules(filepath, request) {
    // console.log(filepath, request);
    return request;
  }
});
/**
 * 动态加载js
 * @param {string} path
 * @return Promise
 */

function dynamicImport(path) {
  var key = dynamicImport.resolve(path);

  if (!dynamicImport.promises[key]) {
    dynamicImport.promises[key] = customImport('', path, function (err, content) {
      return eval(content);
    });
  }

  return dynamicImport.promises[key];
} // 全局缓存列表

dynamicImport.promises = {}; // 生成全局缓存key

dynamicImport.resolve = function (path) {
  return btoa(path);
};

/***/ }),

/***/ "./resources/js/xin/core/ui.js":
/*!*************************************!*\
  !*** ./resources/js/xin/core/ui.js ***!
  \*************************************/
/*! exports provided: getDataOptions, getControlName, setValue, setChecked, focusInsert, formValid, isSupportFormValidator, getScreenType, fullscreen, openUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDataOptions", function() { return getDataOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getControlName", function() { return getControlName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setValue", function() { return setValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setChecked", function() { return setChecked; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "focusInsert", function() { return focusInsert; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formValid", function() { return formValid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSupportFormValidator", function() { return isSupportFormValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getScreenType", function() { return getScreenType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fullscreen", function() { return fullscreen; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "openUrl", function() { return openUrl; });
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * 获取 dataset 数据并转换为对象
 * @param {*} target
 * @param {String} name
 * @param {*} [defaultValue]
 * @return {*}
 */
function getDataOptions(target, name) {
  var defaultValue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  target = $(target);
  var value = target.data(name);

  if (!value) {
    return defaultValue;
  }

  if (_typeof(value) === 'object') {
    return value;
  }

  try {
    return eval("(" + value + ")");
  } catch (e) {}

  return defaultValue;
}
/**
 * 获取组件名
 * @param {*} target
 * @param {String,Array} names
 * @return {String}
 */

function getControlName(target, names) {
  target = $(target);
  names = _typeof(names) === "object" ? names : names.split(",");

  for (var i = 0; i < names.length; i++) {
    if (undefined !== target.data(names[i])) {
      return names[i];
    }
  }

  return null;
}
/**
 * 设置表单的值
 * @param {String} name
 * @param {*} value
 */

function setValue(name, value) {
  var first = name.substr(0, 1),
      input,
      i = 0,
      val;
  if (value === "") return;

  if ("#" === first || "." === first) {
    input = $(name);
  } else {
    input = $("[name='" + name + "']");
  }

  if (input.eq(0).is(":radio")) {
    //单选按钮
    input.filter("[value='" + value + "']").each(function () {
      this.checked = true;
    });
  } else if (input.eq(0).is(":checkbox")) {
    //复选框
    if (!$.isArray(value)) {
      val = [];
      val[0] = value;
    } else {
      val = value;
    }

    for (i = 0; i < val.length; i++) {
      input.filter("[value='" + val[i] + "']").each(function () {
        this.checked = true;
      });
    }
  } else {
    //其他表单选项直接设置值
    input.val(value);
  }
}
/**
 * 设置是否选中
 * @param {String} name
 * @param {*} checked
 */

function setChecked(name, checked) {
  $(name).prop('checked', isNaN(checked) ? checked : parseInt(checked));
}
/**
 * 插入值并获得焦点
 * @param {*} obj
 * @param {string} str
 */

function focusInsert(obj, str) {
  var result,
      val = obj.value;
  obj.focus();

  if (document.selection) {
    //ie
    result = document.selection.createRange();
    document.selection.empty();
    result.text = str;
  } else {
    result = [val.substring(0, obj.selectionStart), str, val.substr(obj.selectionEnd)];
    obj.focus();
    obj.value = result.join('');
  }
}
/**
 * 表单验证
 * @param {jQuery,string,HTMLElement} target
 * @param {Object} [options]
 */

function formValid(target, options) {
  $(target).valid(options);
}
/**
 * 是否有form验证器
 * @return {boolean}
 */

function isSupportFormValidator() {
  return $.fn.valid;
}
/**
 * 屏幕类型
 * @return {number}
 */

function getScreenType() {
  var width = $(window).width();

  if (width >= 1200) {
    return 3; //大屏幕
  } else if (width >= 992) {
    return 2; //中屏幕
  } else if (width >= 768) {
    return 1; //小屏幕
  } else {
    return 0; //超小屏幕
  }
}
/**
 * 全屏
 * @param {boolean} status
 */

function fullscreen(status) {
  var ELEM = status ? document.body : document;

  if (status) {
    if (ELEM.webkitRequestFullScreen) {
      ELEM.webkitRequestFullScreen();
    } else if (elem.mozRequestFullScreen) {
      ELEM.mozRequestFullScreen();
    } else if (ELEM.requestFullScreen) {
      ELEM.requestFullscreen();
    }
  } else {
    if (ELEM.webkitCancelFullScreen) {
      ELEM.webkitCancelFullScreen();
    } else if (ELEM.mozCancelFullScreen) {
      ELEM.mozCancelFullScreen();
    } else if (ELEM.cancelFullScreen) {
      ELEM.cancelFullScreen();
    } else if (ELEM.exitFullscreen) {
      ELEM.exitFullscreen();
    }
  }
}
/**
 * 新标签页打开地址
 * @param {string} url
 */

function openUrl(url) {
  var a = $('<a target="_blank"><span></span></a>');
  a.attr('href', url);
  a.css('display', 'none');
  a.click(function () {
    a.remove();
  });
  a.appendTo(document.body);
  a.find('span').trigger('click');
}

/***/ }),

/***/ "./resources/js/xin/index.js":
/*!***********************************!*\
  !*** ./resources/js/xin/index.js ***!
  \***********************************/
/*! exports provided: assign, optimize, clone, parseUrl, parseQuery, parseName, getFileExtensionName, safeCallback, setConfig, getConfig, date, dynamicImport, mimeMap, getMimeType, getMimeExtension, getDataOptions, getControlName, setValue, setChecked, focusInsert, formValid, isSupportFormValidator, getScreenType, fullscreen, openUrl, request, uploadFile, responseWrapper, tpl, showModal, showPopup, open, close, showToast, hintSuccess, hintError, showLoading, hideLoading, tooltip, preview, showUploadBox, editor, map, chart, datepicker, colorpicker, importFile, imgError, uploader, showViewChoice, showLoginQrcode, choiceCheckBox, getChoiceCheckBoxValues, getChoiceUnCheckBoxValues, choiceCheckBoxHas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ "./resources/js/xin/core/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "assign", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["assign"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "optimize", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["optimize"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "clone", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["clone"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseUrl", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["parseUrl"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseQuery", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["parseQuery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseName", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["parseName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getFileExtensionName", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getFileExtensionName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "safeCallback", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["safeCallback"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setConfig", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["setConfig"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getConfig", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getConfig"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "date", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["date"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dynamicImport", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["dynamicImport"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "mimeMap", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["mimeMap"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMimeType", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getMimeType"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMimeExtension", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getMimeExtension"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getDataOptions", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getDataOptions"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getControlName", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getControlName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setValue", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["setValue"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setChecked", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["setChecked"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "focusInsert", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["focusInsert"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "formValid", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["formValid"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isSupportFormValidator", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["isSupportFormValidator"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getScreenType", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["getScreenType"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fullscreen", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["fullscreen"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "openUrl", function() { return _core__WEBPACK_IMPORTED_MODULE_0__["openUrl"]; });

/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./request */ "./resources/js/xin/request/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "request", function() { return _request__WEBPACK_IMPORTED_MODULE_1__["request"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "uploadFile", function() { return _request__WEBPACK_IMPORTED_MODULE_1__["uploadFile"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "responseWrapper", function() { return _request__WEBPACK_IMPORTED_MODULE_1__["responseWrapper"]; });

/* harmony import */ var _template__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./template */ "./resources/js/xin/template.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tpl", function() { return _template__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ui */ "./resources/js/xin/ui/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showModal", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["showModal"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showPopup", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["showPopup"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "open", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["open"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "close", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["close"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showToast", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["showToast"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hintSuccess", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["hintSuccess"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hintError", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["hintError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showLoading", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["showLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hideLoading", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["hideLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tooltip", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["tooltip"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "preview", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["preview"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showUploadBox", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["showUploadBox"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "editor", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["editor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "map", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["map"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chart", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["chart"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "datepicker", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["datepicker"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "colorpicker", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["colorpicker"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "importFile", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["importFile"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "imgError", function() { return _ui__WEBPACK_IMPORTED_MODULE_3__["imgError"]; });

/* harmony import */ var _ui_adv__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ui-adv */ "./resources/js/xin/ui-adv/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "uploader", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["uploader"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showViewChoice", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["showViewChoice"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showLoginQrcode", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["showLoginQrcode"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBox", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["choiceCheckBox"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getChoiceCheckBoxValues", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["getChoiceCheckBoxValues"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getChoiceUnCheckBoxValues", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["getChoiceUnCheckBoxValues"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBoxHas", function() { return _ui_adv__WEBPACK_IMPORTED_MODULE_4__["choiceCheckBoxHas"]; });







/***/ }),

/***/ "./resources/js/xin/interceptor/request.default.js":
/*!*********************************************************!*\
  !*** ./resources/js/xin/interceptor/request.default.js ***!
  \*********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request */ "./resources/js/xin/request/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ui */ "./resources/js/xin/ui/index.js");


 //合并默认处理

_core__WEBPACK_IMPORTED_MODULE_1__["assign"](_request__WEBPACK_IMPORTED_MODULE_0__["request"].defaults, {
  tipsMsg: true,
  //是否提示消息,为false时关闭所有提示信息
  tipsSuccessMsg: true,
  //是否提示消息
  tipsErrorMsg: true,
  //是否提示错误信息
  responseRaw: false,
  //是否返回原始数据
  //请求结束事件
  complete: function complete(options) {
    var config = options.config;

    if (config.showLoading !== false) {
      Object(_ui__WEBPACK_IMPORTED_MODULE_2__["hideLoading"])();
    }
  }
}); // 添加请求前拦截器


_request__WEBPACK_IMPORTED_MODULE_0__["request"].addRequestInterceptor(function (config) {
  if (config.showLoading !== false) {
    var showLoadingText = typeof config.showLoading === "string" ? config.showLoading : '请稍后...';
    Object(_ui__WEBPACK_IMPORTED_MODULE_2__["showLoading"])(showLoadingText);
  }

  return $.Deferred().resolve(config);
}); //添加服务器返回结果拦截器

_request__WEBPACK_IMPORTED_MODULE_0__["request"].addResponseInterceptor(function (res) {
  var config = res.config,
      data = res.data;
  var status = Object(_request__WEBPACK_IMPORTED_MODULE_0__["responseWrapper"])(data);

  if (status.isSuccess) {
    if (config.tipsMsg && config.tipsSuccessMsg) {
      Object(_ui__WEBPACK_IMPORTED_MODULE_2__["hintSuccess"])(status.msg === 'ok' ? "操作成功！" : status.msg || "操作成功！");
    }

    return config.responseRaw ? res : data;
  } else {
    if (status.code === -1) {
      //未登录
      if (window.login) {
        return window.login().then(function () {
          return Object(_request__WEBPACK_IMPORTED_MODULE_0__["request"])(config);
        });
      } else if (status.url) {
        window.location.href = status.url;
      } else if (config.loginUrl) {
        window.location.href = config.loginUrl;
      } else {
        window.location.reload();
      }

      return $.Deferred().reject({
        errMsg: '登录已失效！'
      });
    } else if (status.code === -2) {
      //无权限
      if (config.tipsMsg && config.tipsErrorMsg) {
        Object(_ui__WEBPACK_IMPORTED_MODULE_2__["showModal"])({
          icon: 'unauthorized',
          content: status.msg
        });
      }

      return $.Deferred().reject(res);
    } else if (status.code === -3) {
      //重复登录
      Object(_ui__WEBPACK_IMPORTED_MODULE_2__["hintError"])(status.msg);
      setTimeout(function () {
        if (data.url) {
          window.location.href = data.url;
        }
      }, 1500);
      return $.Deferred().reject(res);
    } else {
      if (config.tipsMsg && config.tipsErrorMsg) {
        Object(_ui__WEBPACK_IMPORTED_MODULE_2__["hintError"])(status.msg || "操作失败！");
      }

      return $.Deferred().reject(res);
    }
  }
}, function (err) {
  var config = err.config;

  if (config.tipsMsg && config.tipsErrorMsg) {
    Object(_ui__WEBPACK_IMPORTED_MODULE_2__["hintError"])('服务繁忙，请稍后尝试！');
  }

  return $.Deferred().reject(err);
});

/***/ }),

/***/ "./resources/js/xin/request/index.js":
/*!*******************************************!*\
  !*** ./resources/js/xin/request/index.js ***!
  \*******************************************/
/*! exports provided: request, uploadFile, responseWrapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./request */ "./resources/js/xin/request/request.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "request", function() { return _request__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _uploadFile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./uploadFile */ "./resources/js/xin/request/uploadFile.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "uploadFile", function() { return _uploadFile__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _response_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./response-wrapper */ "./resources/js/xin/request/response-wrapper.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "responseWrapper", function() { return _response_wrapper__WEBPACK_IMPORTED_MODULE_2__["default"]; });





/***/ }),

/***/ "./resources/js/xin/request/request.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/request/request.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");
/* ************************************************************************
 *	异步请求
 *************************************************************************/

/**
 * ajax -依赖Jquery
 * @param {*} options
 */

function request(options) {
  options = $.extend({}, request.defaults, options);
  delete options.success;
  delete options.error;
  var chain = [].concat(request.interceptors.request, [{
    fulfilled: function fulfilled() {
      return $.ajax(options).then(function (res, _, XMLHttpRequest) {
        XMLHttpRequest.data = res;
        XMLHttpRequest.config = options;
        return $.Deferred().resolve(XMLHttpRequest);
      }, function (XMLHttpRequest) {
        XMLHttpRequest.config = options;
        return $.Deferred().reject(XMLHttpRequest);
      });
    },
    rejected: null
  }], request.interceptors.response);
  var promise = new $.Deferred();
  promise.resolve(options);
  chain.forEach(function (interceptor) {
    return promise = promise.then(interceptor.fulfilled, interceptor.rejected);
  });
  return promise;
}
/**
 * 默认配置
 * @type {*}
 */


request.defaults = {
  timeout: 15 * 1000,
  xhrFields: {
    withCredentials: true
  },
  dataType: "json"
};
/**
 * 拦截器
 * @type {{request: [], response: []}}
 */

request.interceptors = {
  request: [],
  response: []
};
/**
 * 添加请求拦截器
 * @param {function} [fulfilled]
 * @param {function} [rejected]
 * @return {number}
 */

request.addRequestInterceptor = function (fulfilled, rejected) {
  return request.interceptors.request.push({
    fulfilled: fulfilled,
    rejected: rejected
  }) - 1;
};
/**
 * 移除拦截器
 * @param {number} number
 */


request.removeRequestInterceptor = function (number) {
  request.interceptors.request.splice(number, 0);
};
/**
 * 添加服务器结果拦截器
 * @param {function} [fulfilled]
 * @param {function} [rejected]
 * @return {number}
 */


request.addResponseInterceptor = function (fulfilled, rejected) {
  return request.interceptors.response.push({
    fulfilled: fulfilled,
    rejected: rejected
  }) - 1;
};
/**
 * 移除拦截器
 * @param {number} number
 */


request.removeResponseInterceptor = function (number) {
  request.interceptors.response.splice(number, 0);
}; //注册快捷方法


['get', 'post', 'put', 'delete'].forEach(function (method) {
  request[method] = function (url, data) {
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    return request(Object(_core__WEBPACK_IMPORTED_MODULE_0__["assign"])(options, {
      url: url,
      method: method,
      data: data
    }));
  };
});
/* harmony default export */ __webpack_exports__["default"] = (request);

/***/ }),

/***/ "./resources/js/xin/request/response-wrapper.js":
/*!******************************************************!*\
  !*** ./resources/js/xin/request/response-wrapper.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * 智能验证服务器返回数据合法性
 * @param {{msg?:string,info?:string,message?:string,errMsg?:string,errorMsg?:string,code?number,status?number,errorCode?number,errCode?number,error?number,data?:*,result?:*,}} res
 * @return {{code: number, msg: string, isSuccess: boolean, data: *}}
 */
/* harmony default export */ __webpack_exports__["default"] = (function (res) {
  var code,
      msg,
      data,
      isSuccess = false;

  if (res.msg) {
    msg = res.msg;
  } else if (res.info) {
    msg = res.info;
  } else if (res.message) {
    msg = res.message;
  } else if (res.errMsg) {
    msg = res.errMsg;
  } else if (res.errorMsg) {
    msg = res.errorMsg;
  }

  if (undefined !== res.code) {
    isSuccess = (code = res.code) === 1;
  } else if (undefined !== res.status) {
    isSuccess = (code = res.status) === 1;
    if (!isSuccess) msg = res.info;
  } else if (undefined !== res.errorCode) {
    isSuccess = (code = res.errorCode) === 0;
  } else if (undefined !== res.errCode) {
    isSuccess = (code = res.errCode) === 0;
  } else if (undefined !== res.error && !isNaN(res.error)) {
    isSuccess = (code = res.error) === 0;
  } else {
    throw new Error('找不到合适验证状态码，请确保返回的数据含有：status,code,errCode,errorCode,error 其中的一项！');
  }

  if (undefined !== res.data) {
    data = res.data;
  } else if (undefined !== res.result) {
    data = res.result;
  } else {
    data = res;
  }

  return {
    code: code,
    msg: msg,
    isSuccess: isSuccess,
    data: data
  };
});
;

/***/ }),

/***/ "./resources/js/xin/request/uploadFile.js":
/*!************************************************!*\
  !*** ./resources/js/xin/request/uploadFile.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./request */ "./resources/js/xin/request/request.js");

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  options = Object.assign({}, options, {
    type: 'POST',
    contentType: false,
    processData: false,
    xhrFields: {
      // 这样在请求的时候会自动将浏览器中的cookie发送给后台
      withCredentials: true
    }
  });
  var file = options.file,
      fileFieldName = options.name;
  options.data = resolveFormData(options.data);
  options.data.append(fileFieldName, file);
  var csrfToken = $('meta[name="csrf-token"]').attr('content');

  if (csrfToken) {
    options.data.append('_token', csrfToken);
  }

  var chain = [].concat(_request__WEBPACK_IMPORTED_MODULE_0__["default"].interceptors.request, [{
    fulfilled: function fulfilled() {
      return $.ajax(options).then(function (res, _, XMLHttpRequest) {
        XMLHttpRequest.data = res;
        XMLHttpRequest.config = options;
        return $.Deferred().resolve(XMLHttpRequest);
      }, function (XMLHttpRequest) {
        XMLHttpRequest.config = options;
        return $.Deferred().reject(XMLHttpRequest);
      });
    },
    rejected: null
  }], _request__WEBPACK_IMPORTED_MODULE_0__["default"].interceptors.response);
  var promise = new $.Deferred();
  promise.resolve(options);
  chain.forEach(function (interceptor) {
    return promise = promise.then(interceptor.fulfilled, interceptor.rejected);
  });
  return promise;
});
;

function resolveFormData(data) {
  data = data || {};

  if (data instanceof FormData) {
    return data;
  }

  var formData = new FormData();

  for (var x in data) {
    if (!data.hasOwnProperty(x)) {
      continue;
    }

    var value = resolveValue(data[x]);
    formData.append(x, value);
  }

  return formData;
}

function resolveValue(value) {
  if (value instanceof File || value instanceof Blob) {
    return value;
  } else if (value === 'object') {
    return JSON.stringify(value);
  }

  return value;
}

/***/ }),

/***/ "./resources/js/xin/template.js":
/*!**************************************!*\
  !*** ./resources/js/xin/template.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return tpl; });
/**
 * 模板引擎
 * @param {{
 * template:string|HTMLElement,
 * data:Object,
 * success:Function|HTMLElement
 * }|*} options
 * @return {String}
 */
function tpl(options) {
  return layui.laytpl(options.template).render(options.data, options.success);
}
;

/***/ }),

/***/ "./resources/js/xin/ui-adv/choice.checkbox.js":
/*!****************************************************!*\
  !*** ./resources/js/xin/ui-adv/choice.checkbox.js ***!
  \****************************************************/
/*! exports provided: choiceCheckBox, getChoiceCheckBoxValues, getChoiceUnCheckBoxValues, choiceCheckBoxHas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBox", function() { return choiceCheckBox; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getChoiceCheckBoxValues", function() { return getChoiceCheckBoxValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getChoiceUnCheckBoxValues", function() { return getChoiceUnCheckBoxValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBoxHas", function() { return choiceCheckBoxHas; });
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui */ "./resources/js/xin/ui/index.js");
/**
 * 获取子目标jQuery对象
 * @param {*} self
 * @return {*}
 */


function makeTargetJQuery(self) {
  var target = self.data("target");

  if (!target) {
    throw Error('请配置data-target属性');
  }

  return $(target);
}
/**
 * layui checkbox
 * @param target
 * @param childTarget
 */


function layuiAdapter(target, childTarget) {
  var formFilter = 'form_' + new Date().getTime();
  target.closest('.layui-form').attr('lay-filter', formFilter);
  layui.form.on('checkbox', function (data) {
    if (undefined !== $(data.elem).data('choiceCheck')) {
      childTarget.prop("checked", data.elem.checked);
    } else {
      var isCheck = true;
      childTarget.each(function (index, checkItem) {
        if ($(checkItem).is(":checked") === false) {
          isCheck = false;
          return false;
        }
      });
      target.prop("checked", isCheck);
    }

    layui.form.render('checkbox', formFilter);
  });
}
/**
 * default checkbox
 * @param target
 * @param childTarget
 */


function defaultAdapter(target, childTarget) {
  target.on($.fn.iCheck ? "ifChanged" : "change", function () {
    childTarget.prop("checked", target.is(":checked"));
    $.fn.iCheck && childTarget.iCheck('update');
  }); //if change someone checkbox,that all-checkbox-button update state.

  childTarget.on($.fn.iCheck ? "ifChanged" : "change", function () {
    var isCheck = true;
    childTarget.each(function (index, checkItem) {
      if ($(checkItem).is(":checked") === false) {
        isCheck = false;
        return false;
      }
    });
    target.prop("checked", isCheck);
    $.fn.iCheck && target.iCheck('update');
  });
}
/**
 * 全选/取消全选
 * @param {*} target
 */


function choiceCheckBox(target) {
  target = $(target);
  var childTarget = makeTargetJQuery(target);

  if (window.layui) {
    //layui
    layuiAdapter(target, childTarget);
  } else {
    //原始或iCheck
    defaultAdapter(target, childTarget);
  }
}
/**
 * 获取checkbox选中的值
 * @param {jQuery,string,HTMLElement} target
 */

function getChoiceCheckBoxValues(target) {
  target = $(target);
  var childTarget = makeTargetJQuery(target);
  var values = [];
  childTarget.each(function (index, checkboxItem) {
    checkboxItem = $(checkboxItem);
    checkboxItem.is(":checked") && values.push(checkboxItem.val());
  });
  return values;
}
/**
 * 获取checkbox没选中的值
 * @param {jQuery,string,HTMLElement} target
 */

function getChoiceUnCheckBoxValues(target) {
  target = $(target);
  var childTarget = makeTargetJQuery(target);
  var values = [];
  childTarget.each(function (index, checkboxItem) {
    checkboxItem = $(checkboxItem);
    !checkboxItem.is(":checked") && values.push(checkboxItem.val());
  });
  return values;
}
/**
 * 如果有选中的checkbox则返回数据
 * @param {jQuery,string,HTMLElement,Function} target
 * @param {Function} [callback]
 * @param {Object} [thisArg]
 */

function choiceCheckBoxHas(target, callback, thisArg) {
  var values = this.getChoiceCheckBoxValues(target);

  if (values.length === 0) {
    Object(_ui__WEBPACK_IMPORTED_MODULE_0__["showModal"])({
      icon: 'error',
      content: '亲，你未选择数据！'
    });
    return false;
  }

  callback.call(thisArg || this, values);
}

/***/ }),

/***/ "./resources/js/xin/ui-adv/index.js":
/*!******************************************!*\
  !*** ./resources/js/xin/ui-adv/index.js ***!
  \******************************************/
/*! exports provided: uploader, showViewChoice, showLoginQrcode, choiceCheckBox, getChoiceCheckBoxValues, getChoiceUnCheckBoxValues, choiceCheckBoxHas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _uploader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./uploader */ "./resources/js/xin/ui-adv/uploader.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "uploader", function() { return _uploader__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _view_choice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view-choice */ "./resources/js/xin/ui-adv/view-choice.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showViewChoice", function() { return _view_choice__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _login_qrcode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login-qrcode */ "./resources/js/xin/ui-adv/login-qrcode.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showLoginQrcode", function() { return _login_qrcode__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _choice_checkbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./choice.checkbox */ "./resources/js/xin/ui-adv/choice.checkbox.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBox", function() { return _choice_checkbox__WEBPACK_IMPORTED_MODULE_3__["choiceCheckBox"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getChoiceCheckBoxValues", function() { return _choice_checkbox__WEBPACK_IMPORTED_MODULE_3__["getChoiceCheckBoxValues"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getChoiceUnCheckBoxValues", function() { return _choice_checkbox__WEBPACK_IMPORTED_MODULE_3__["getChoiceUnCheckBoxValues"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "choiceCheckBoxHas", function() { return _choice_checkbox__WEBPACK_IMPORTED_MODULE_3__["choiceCheckBoxHas"]; });






/***/ }),

/***/ "./resources/js/xin/ui-adv/login-qrcode.js":
/*!*************************************************!*\
  !*** ./resources/js/xin/ui-adv/login-qrcode.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request */ "./resources/js/xin/request/index.js");
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ui */ "./resources/js/xin/ui/index.js");

 // 生成二维码

function makeQrCode(url) {
  return _request__WEBPACK_IMPORTED_MODULE_0__["request"].get(url, {}, {
    showLoading: false,
    tipsSuccessMsg: false
  }).then(function (res) {
    var path = res.data;
    return new Promise(function (resolve, reject) {
      var img = new Image();
      img.src = path;
      img.onload = resolve;
      img.onerror = reject;
    });
  });
} // 检测登录工厂函数


function checkLoginFactory(url, makeTime) {
  var timeout = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;
  // 检测定时器ID
  var poolTimerId = 0; // 是否终止检测

  var isStop = false; // 开始检测

  function start(success, fail) {
    _request__WEBPACK_IMPORTED_MODULE_0__["request"].post(url, {}, {
      showLoading: false,
      tipsSuccessMsg: false
    }).then(function (res) {
      if (res.data.status === 1) {
        poolTimerId = 0;
        success();
      } else {
        var currentTime = Math.floor(new Date().getTime() / 1000);

        if (makeTime + timeout > currentTime) {
          setTimeout(function () {
            start(success, fail);
          }, 1000);
        } else {
          poolTimerId = 0;
          fail();
        }
      }
    }, function () {
      poolTimerId = 0;
      fail();
    });
  } // 终止执行


  function stop() {
    isStop = true;
    clearTimeout(poolTimerId);
  }

  return {
    start: start,
    stop: stop
  };
}

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  // if (!options.makeUrl) {
  // 	return Promise.reject("生成二维码地址不能为空！");
  // }
  //
  // if (!options.checkUrl) {
  // 	return Promise.reject("检测是否已登录地址不能为空！");
  // }
  var TPL = "\n<div style=\"font-weight: bold;font-size: 20px;text-align: center;line-height: 2;margin-top: 15px\">".concat(options.title || "登录二维码", "</div>\n<img src=\"\" alt=\"\" style=\"border-radius: 5px;border:1px solid #eee;width: 200px;height: 200px;display: block;margin: 15px auto;padding: 15px;\"/>\n<p style=\"font-size: 12px;color: gray;text-align: center\">").concat(options.description || "手机扫一扫快捷登录", "</p>\n");
  return makeQrCode(options.makeUrl).then(function (img) {
    new Promise(function (resolve) {
      Object(_ui__WEBPACK_IMPORTED_MODULE_1__["showModal"])({
        area: ['320px', '400px'],
        content: TPL,
        btn: false,
        created: function created(winCtx) {// const container = winCtx.popup.find('.layui-layer-content');
          // makeQrCode(function(img) {
          // 	const path = img.src;
          // 	poolCheckLogin(resolve, function() {
          // 		xin.close(winCtx.index);
          // 		xin.showModal({
          // 			content: '登录二维码已失效，请重新打开~',
          // 			showCancel: false
          // 		});
          // 	});
          // });
        }
      });
    });
  });
});

/***/ }),

/***/ "./resources/js/xin/ui-adv/uploader.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/ui-adv/uploader.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui */ "./resources/js/xin/ui/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }



var PLUS_BUTTON_TPL = "\n<div class=\"uploader-item uploader-plus\">\n\t<i class=\"msicon msicon-img\"></i>\n</div>\n"; // 获取路径

function getPaths(target) {
  var paths = target.data('path');
  paths = paths ? _typeof(paths) === 'object' ? paths : paths.trim().split(',') : [];
  return paths;
}

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  options = Object.assign({
    max: 9,
    multi: false
  }, options);
  var self = $(options.el);
  var paths = getPaths(self);
  self.addClass('uploader-list');
  self.attr('data-preview', ''); //图片选择按钮

  var plusItemBtn = $(PLUS_BUTTON_TPL).on('click', function () {
    var length = self.children().length - 1;
    Object(_ui__WEBPACK_IMPORTED_MODULE_0__["showUploadBox"])(Object.assign({
      max: options.multi ? options.max - length : 1
    }, options)).then(function (res) {
      res.forEach(function (item) {
        plusItemHandler(item);
      });
      updatePlusItemBtnStatus();
    });
    return false;
  }).appendTo(self); //更新是否显示图片选择按钮

  var updatePlusItemBtnStatus = function updatePlusItemBtnStatus() {
    if (options.multi) {
      if (self.children().length >= options.max + 1) {
        plusItemBtn.hide();
      } else {
        plusItemBtn.show();
      }
    } else {
      if (self.children().length > 1) {
        plusItemBtn.hide();
      } else {
        plusItemBtn.show();
      }
    }
  }; //添加项处理器


  var plusItemHandler = function plusItemHandler(path) {
    var TPL = "\n<div class=\"uploader-item\">\n\t<img src=\"".concat(path, "\" mode=\"aspectFill\"  alt=\"\"/>\n\t<input type=\"hidden\" name=\"").concat(options.name, "\" value=\"").concat(path, "\"/>\n\t<span class=\"uploader-btn-remove msicon msicon-close\"></span>\n</div>\n");
    var item = $(TPL);
    item.find('.uploader-btn-remove').on('click', function () {
      item.remove();
      updatePlusItemBtnStatus();
      return false;
    });
    plusItemBtn.before(item);
  }; //显示已默认的图片


  paths.forEach(plusItemHandler);
  updatePlusItemBtnStatus(); //支持拖动排序

  if (window.Sortable) {
    new Sortable(self[0], Object(_core__WEBPACK_IMPORTED_MODULE_1__["assign"])({
      animation: 150,
      filter: ".uploader-plus"
    }, options.sortable || {}));
  }
});

/***/ }),

/***/ "./resources/js/xin/ui-adv/view-choice.js":
/*!************************************************!*\
  !*** ./resources/js/xin/ui-adv/view-choice.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui */ "./resources/js/xin/ui/index.js");

/**
 * 页面选择
 * @param {{
 * url:string,
 * }|*} options
 * @return Promise
 */

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  return new Promise(function (resolve, reject) {
    Object(_ui__WEBPACK_IMPORTED_MODULE_0__["open"])(options).then(function (res) {
      try {
        res.iframe.contentWindow.finish = function (result) {
          resolve(result);
          Object(_ui__WEBPACK_IMPORTED_MODULE_0__["close"])(res.index);
        };
      } catch (e) {
        reject({
          errMsg: "不支持跨域选择"
        });
      }
    }, reject);
  });
});

/***/ }),

/***/ "./resources/js/xin/ui/chart.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/chart.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  return new Promise(function (resolve) {
    layui.define('echarts', function () {
      var echarts = layui.echarts;
      var myCharts = echarts.init($(options.el)[0], layui.echartsTheme);
      myCharts.setOption(options);
      resolve(myCharts);
    });
  });
});
;

/***/ }),

/***/ "./resources/js/xin/ui/child-window.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/ui/child-window.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * 打开一个页面
 * @param {{
 * url:string,
 * closed:function
 * }|*} options
 * @return Promise<{layero:jQuery,index:number,iframe:HTMLIFrameElement}>
 */
/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  var preWidth = $(window).width() * 0.8 + "px",
      preHeight = $(window).height() * 0.8 + "px";
  options = Object.assign({
    area: options.area ? options.area : [preWidth, preHeight],
    type: 2,
    title: '正在加载...',
    content: options.url,
    end: options.closed
  });
  return new Promise(function (resolve) {
    options.success = function (layero, index) {
      var iFrame = layero.find("iframe");
      var result = {
        layero: layero,
        index: index,
        iframe: iFrame[0]
      };

      var updateTitle = function updateTitle() {
        try {
          layer.title(iFrame[0].contentWindow.document.title, index);
        } catch (e) {
          layer.title("", index);
        }
      };

      if (iFrame[0].contentWindow) {
        updateTitle();
        resolve(result);
      } else {
        iFrame.on("load", function () {
          updateTitle();
          resolve(result);
        });
      }
    };

    layer.open(options);
  });
});

/***/ }),

/***/ "./resources/js/xin/ui/close.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/close.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * 关闭窗体/层
 * @param {*} id
 */
/* harmony default export */ __webpack_exports__["default"] = (function (id) {
  layer.close(id);
});

/***/ }),

/***/ "./resources/js/xin/ui/colorpicker.js":
/*!********************************************!*\
  !*** ./resources/js/xin/ui/colorpicker.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");

/**
 * ColorPicker
 * @param {{
 *     el:HTMLElement,
 *     color?:string,
 *     format?:string,
 *     alpha?:boolean,
 *     colors?:Array<string>,
 *     success?:function,
 * }|*} options
 * @return Promise
 */

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  options = Object(_core__WEBPACK_IMPORTED_MODULE_0__["optimize"])({
    elem: options.el,
    color: options.color,
    format: options.format,
    alpha: options.alpha,
    colors: options.colors,
    predefine: !!options.colors,
    done: options.success
  });
  return Promise.resolve(layui.colorpicker.render(options));
});
;

/***/ }),

/***/ "./resources/js/xin/ui/datepicker.js":
/*!*******************************************!*\
  !*** ./resources/js/xin/ui/datepicker.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");

/**
 * DatePicker
 * @param {{
 *     el:HTMLElement,
 *     type?:string,
 *     range?:boolean,
 *     format?:string,
 *     value?:string,
 *     min?:string,
 *     max?:string,
 *     position?:string,
 *     mark?:Object,
 *     success?:function,
 * }|*} options
 * @return Promise
 */

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  options = Object(_core__WEBPACK_IMPORTED_MODULE_0__["optimize"])({
    elem: options.el,
    type: options.type,
    range: options.range,
    format: options.format,
    value: options.value,
    min: options.min,
    max: options.max,
    position: options.position,
    calendar: true,
    mark: options.mark,
    trigger: 'click',
    done: options.success
  });
  return new Promise(function (resolve) {
    options.ready = resolve;
    layui.laydate.render(options);
  });
});
;

/***/ }),

/***/ "./resources/js/xin/ui/editor.js":
/*!***************************************!*\
  !*** ./resources/js/xin/ui/editor.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var load = function () {
  var promise = null;
  return function () {
    if (promise === null) {
      promise = __webpack_require__.e(/*! import() | wangeditor */ "vendors~wangeditor").then(__webpack_require__.t.bind(null, /*! wangeditor */ "./node_modules/wangeditor/release/wangEditor.js", 7));
    }

    return promise;
  };
}();
/**
 * 实例化一个编辑器
 * @param {{el:HTMLElement|string}} options
 */


/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  var container = $(options.el);
  var textarea;

  if ("TEXTAREA" === container[0].tagName) {
    textarea = container;
    container = $('<div></div>');
    textarea.before(container);
    container.html(textarea.val());
    container.addClass('editor');
  } else {
    textarea = $('<textarea></textarea>').after(container);
    textarea.val(container.html());
  }

  textarea.hide();

  if (options.name) {
    textarea.attr('name', options.name);
  }

  return new Promise( /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(resolve) {
      var WangEditor, editor;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return load();

            case 2:
              WangEditor = _context.sent["default"];
              editor = new WangEditor(container[0]); // 自定义菜单配置

              editor.customConfig.menus = defaultMenus();
              editor.customConfig.zIndex = 999; // 监控变化，同步更新到 textarea

              editor.customConfig.onchange = function (html) {
                textarea.val(html);
              };

              editor.create(); // 更新样式

              updateStyle(editor);
              textarea.val(editor.txt.html());
              resolve({
                getContent: function getContent() {
                  return editor.txt.html();
                },
                setContent: function setContent(text) {
                  editor.txt.html(text);
                  textarea.val(text);
                },
                ins: editor
              });

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }());
});
; // 更新样式

function updateStyle(editor) {
  console.log(editor);
  var toolbar = $(editor.$toolbarElem[0]);
  var textContainer = $(editor.$textContainerElem[0]);
  toolbar.css('background-color', 'white').css('border', 'none');
  textContainer.css('border', 'none'); // 更新字号显示
  // const fontSizeTexts = ["超小号"];
  // editor.menus.menus.fontSize.droplist.opt.list.forEach(function(item, index) {
  // 	item.$elem.text(fontSizeTexts[index]);
  // 	console.log(item)
  // });
  // console.log(toolbar.children().eq(2));
} // 默认菜单


function defaultMenus() {
  return ['head', // 标题
  'bold', // 粗体
  // 'fontSize',  // 字号
  // 'fontName',  // 字体
  'italic', // 斜体
  'underline', // 下划线
  'strikeThrough', // 删除线
  'foreColor', // 文字颜色
  // 'backColor',  // 背景颜色
  'link', // 插入链接
  'list', // 列表
  'justify', // 对齐方式
  'quote', // 引用
  // 'emoticon',  // 表情
  'image', // 插入图片
  // 'table',  // 表格
  // 'video',  // 插入视频
  'code', // 插入代码
  'undo', // 撤销
  'redo' // 重复
  ];
}

/***/ }),

/***/ "./resources/js/xin/ui/img-error.js":
/*!******************************************!*\
  !*** ./resources/js/xin/ui/img-error.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Image Error
 * @param {{
 *     el:HTMLElement,
 *     url?:string
 * }} options
 */
/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  var img = $(options.el);
  var defaultURL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAALsUlEQVR4Xu1dXYwcRxGu2vvZjUlw7uy96Qk/dnAgCQRhIyAER/gMUZAQAlsCRcoDOPxIPIUERQgQEuYhIIQQRLwAEtg8IFAkIEYBlCDAKCHYcQAbSEIMAefPXbN75O7sON7T3W6hRntgn3d2umd6Znt3eqR92uqq6qpvaqq7+gfBP6W2AJa6977z4AFQchB4AHgAlNwCJe++jwAeACW3QMm77yOAB0DJLVDy7vsI4AFQcguUvPs+AngAlNwCJe++jwAeACW3QMm77yOAB0DJLVDy7vsI4AFQcguUvPs+AngAlNwCJe9+6ggwPz9/6dLS0ocAYBYALh0hOx4TQtxmoz/NZvPKdrv9TWZObec4PRBxARH/gIiPzMzM/CKtvqkUazQaWzudzk8AYHNawY62O1atVmenpqYWsuoXRdGWTqdzCBE3ZuWV1J6Zn0HEdwsh/ppEu/Z/YwB0nf+bEXvrlV0WmXlrGIYnTI24ln5hYWGq1WodBoBXZ+Wl256Zm9Vq9U3T09NP67ZRdMYAIKJ/jeCbv1ipVGZnZmaOmhgvjlZK+QAiXm+DlyGPh4UQ15q0MQJAFEW7mFmF/pF6mHlnGIYHbXRKSnk3In7ABq80PJj5pjAM79ZtawQAIvo6AHxCl/kw0DHzLWEY7rehKxF9CQA+bYNXBh4/EELcrNveCABSyoOIuEOXuet0zPyFMAz32tAziqIPMvP3bPDKyOOoEGKbLo88AKCSKSvfUt1OpKFDRGUoK8M9Zr6o0Wjc0el0xtLootMGES8DgPcCQJBA/5wQ4uU6PI2TQI0IYG0YpduBMtER0UsA4FcA0C/RGxwAmPlyG8OoMjnVtK9Syp2I+Os+7QYGADWDttW0Q57e3AJExM4BgJl/G4ahmhb2T84WkFK2ELEaI2YwEcADIGevn8NeSnkWEWseAMXZ3EhSs9nc0W63dwGA+iSqeon6qYkmVWM4yMwHsuRKUsoXEfEiDwAjt+RPLKWcRcR9OlPliLh/cnLy9jSFJw+A/H1pLCGKon3MvMew4QIz7zadhpZSnkHEdT4CGFo7L3Ii+lM33KcSYTodTUQvAICaE+j1+CQwlRdSNkr55q+VtlCpVHbqViM9AFI6y3Yzy9XRE0KIy3V0JKLTAHCxjwA61sqRJoe1EbcLIVTFte9DRKcA4BIPgCRL5fi/5bd/VVOtKEBEiwDwUg+AHB2cxFpKuR8R1aJYq49OPcUDwKrJ0zHTqIymYqwzIiAiNaG03keAVCa20yihIJNaiM4iFSKa77Mo1w8DU1vfoKEHwBpjla0YlBCGDaB0PqlOBJBSPo+IU/4TkNrM2RvmlQMg4u4gCO7pp6GU8t+IOO0BkN2PqTnktUK6Wq1OJRWIpJRziLjBAyC1+7I37O6QUjUAm88BIYQqI/d9PACSLFTQ/7Y/A7obVKSUzT57Dss1ClC7lJeXlzfrFlJsYkNKuRkR1VY5G4/W268ESSkbiFj3nwAAICK1VW3WpJpmw1urPKSUe7qLQLKwNVpO7wHQNfWaRMyopJrFW2vbEpHaYPK1lDyPVSqVPSYRjIgiAJgpdQSIefMGBoJucUjtMYybou3lrwPVanVPUtbfA3DUZ4fQ6OcACRn4wECg8pFWq3UbIqqIEAsENWkGAHtNl4KtAoGIyguAbuKlhl/9jqUZGAhWnaRA2l0ZfO4LfKJWq91j+sb3iAASAETpPgHdc4nU6SQ6O5AGDoKUOUFiMyI6CQBh6QDQzfgTJ0rOMcxIgoCIngMAtVu41zOaOYCUci8ifj7x9biQQC293pZlI0YKmbk2KR0ALIy1j1ar1Z1Zv725etWAORE9CwAvK0UEsHgqWW4gkFLeAgCbEHFpbGzsR/V6/biBP41JpZRPIeIrRx4A3aRPTbXaOojSKgjm5uauWl5e/j4ivnGNMz4ihPiusWc1G5QCAIYZv6bp/ktmBQTz8/OblpaW+p0p+FEhxHdMFNOllVI+jYivGOkIYGnHTU8bqY2ZQRCosJ3pIaJbAeCuGCYddVxcEAQ/ziSkR+ORjwAZMn5tW9sCQRRFX2bmT8UIXhkbG7uhXq+rmT9rj5TyBCJuGskIYCHj1za0RRDE7gxm5hfHx8d31Ov1R7QVSyAcWQBYzPi1bW0DBOo08CiKfgoA74kRrOYitodh+Ji2Yn0IE7akDedEUA4Zv7atdVbiJjFj5okoitQpIG+LoW0AwFuFEJkXkBDRPwEgbiPpcAIg6x77JAcl/a+zIyeJR6PRuLjT6TwIAG/oRauOdR8fH7+2Xq+rYk7qZ+QAkGfGb2JlGyBYXFycPnv27BEAeFUMCI5Xq9W3TE9Pqw2eqR4iejKOPwAMVwTIuJomlQH7NdJdmNmPR/emkEcBoOfRscx8RAixAxHPpukAEf0DALYM/Sggpy3WaWz6vza2djgR0e/65ANK3sEgCG5ExGVThUcCAIPI+HUMbREAat1C0sGZ9wZB8D5E7OjotkpDRH8HgCuGNgJ0M361qsfFO4fuynqK+Nzc3NUrKyu6Q759QogPmwBASvkEIr5maAEw6Iy/j7GNlmf34nPq1KkNZ86cUbd5xc3U9WpmBLqhBoArGX8PL2S+NIqZa+rb36M6qPOCf04IcacO4dACwLWM/xxjW7k0Skr5M3V9m44Te9Eg4seDIPhWUnsp5d8Q8cqh+gS4mPGvGlBnS7aGU76KiJ9Mokv4nxHx/UkVRCnl44h41dAAwNWMXxnQxuQPEX0MAL6d0fmrzVdUFAmC4Jdx/IYKADku7Mhsb3XJUxiGpuf7nieXiN4JAPcDQCWzQl0GzNyqVCrvCILg9714SikfQ8SrhyICEJHOeNiW7bT5WHL+NQBwqM+5vdr69CA8rS6fDILgz2v/k1I+ioivdR4ADmf8mYd7zWbzsna7/UeNW7xSg4CZ1VEwb15bQSQidUfw65wGQJELO0wszMxP1Wq1rVmWiDebzUtWVlYe7pOImajUl7ZXBdF5AHQvTlCh37Un83CPmSvqs4aIby+qc8x8fN26ddetX7/+eSWTiP4CAOrz0+sZbDXQ5Yy/UqlsM9mHH5OA5XJErAaY1DkC18/MzLxARCoveL1zAKjVaruWlpZ0N29q9NkeiaXh3mcBQGu2zp7m53F6SAix3VkAIKK6zy6pApaTbeLZ2ljyRUQ3AcAPC1f+QoH3dotoPT8BzPxsGIZxewYu4JbH3cEO2Oj/Klga7qmrWh8AgAmnOudSDuCiYWzU9qMo2tLpdI70OZrVta4PJgl0zQoAkHmsn7K0O2hTeAAAgI3S7gQRHUpZ2h0kCEoPgMxj/e5YW+3p2z1IT6aUXW4AWCrtfgUR70jpgEE3Ky8ALI31bZZ2BwGGcgLA0nDPeml3AAgoHwAsOT/P0m6ROCgdADIP94oo7RaIgPIAwEZp9+TJk+sQUS3jjltjV6DvrIgqDQAyD/dUaTeKovsA4AYrpneDSTkAYGMTZ163fw4SB8z8RBiG2tHMtBg0qFr4eTa1NNz7DAB8cZDOykn2/UKId+nyNgWAjdsxdHXrSTdipd1MtohpfKsQ4hu6jI0AoJgmHFCkKzcVnY3hXhRF1zHzQ6kUcL/Rk0EQXIOILV1VjQGQ03VpOvoeE0LoHBMfy6vRaFzRbrcP97l0UUcPJ2mYeX5iYmL7xo0bHzdR0BgA3Sgwi4jqdkuT61FM9FpLm3msv7CwMNVqtdTRLXEna2TRb6BtmfnBycnJmzds2PCMqSKpAKCEdHf87GFmdX6/uj7NZDu0iZ6ZS7tKGBHdqY5qMxHsMi0inmZmtS/hcBiGP0+ra2oApBXo27llAQ8At/xRuDYeAIWb3C2BHgBu+aNwbTwACje5WwI9ANzyR+HaeAAUbnK3BHoAuOWPwrXxACjc5G4J9ABwyx+Fa+MBULjJ3RLoAeCWPwrXxgOgcJO7JdADwC1/FK6NB0DhJndLoAeAW/4oXBsPgMJN7pZADwC3/FG4Nh4AhZvcLYEeAG75o3BtPAAKN7lbAv8DnZps6ubWsy8AAAAASUVORK5CYII=';
  var url = options.url || defaultURL;

  if (img[0].complete) {
    if (!img[0].naturalWidth && !img[0].naturalHeight) {
      img.attr('src', url);
    }
  } else {
    img[0].onerror = function () {
      img.attr('src', url);
    };
  }
});

/***/ }),

/***/ "./resources/js/xin/ui/importFile.js":
/*!*******************************************!*\
  !*** ./resources/js/xin/ui/importFile.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (function () {});
;

/***/ }),

/***/ "./resources/js/xin/ui/index.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/index.js ***!
  \**************************************/
/*! exports provided: showModal, showPopup, open, close, showToast, hintSuccess, hintError, showLoading, hideLoading, tooltip, preview, showUploadBox, editor, map, chart, datepicker, colorpicker, importFile, imgError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal */ "./resources/js/xin/ui/modal.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showModal", function() { return _modal__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _popup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./popup */ "./resources/js/xin/ui/popup.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showPopup", function() { return _popup__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _child_window__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./child-window */ "./resources/js/xin/ui/child-window.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "open", function() { return _child_window__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _close__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./close */ "./resources/js/xin/ui/close.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "close", function() { return _close__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _toast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./toast */ "./resources/js/xin/ui/toast.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showToast", function() { return _toast__WEBPACK_IMPORTED_MODULE_4__["showToast"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hintSuccess", function() { return _toast__WEBPACK_IMPORTED_MODULE_4__["hintSuccess"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hintError", function() { return _toast__WEBPACK_IMPORTED_MODULE_4__["hintError"]; });

/* harmony import */ var _loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./loading */ "./resources/js/xin/ui/loading.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showLoading", function() { return _loading__WEBPACK_IMPORTED_MODULE_5__["showLoading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hideLoading", function() { return _loading__WEBPACK_IMPORTED_MODULE_5__["hideLoading"]; });

/* harmony import */ var _tooltip__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tooltip */ "./resources/js/xin/ui/tooltip.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tooltip", function() { return _tooltip__WEBPACK_IMPORTED_MODULE_6__["default"]; });

/* harmony import */ var _preview__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./preview */ "./resources/js/xin/ui/preview.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "preview", function() { return _preview__WEBPACK_IMPORTED_MODULE_7__["default"]; });

/* harmony import */ var _uploadbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./uploadbox */ "./resources/js/xin/ui/uploadbox.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showUploadBox", function() { return _uploadbox__WEBPACK_IMPORTED_MODULE_8__["default"]; });

/* harmony import */ var _editor__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./editor */ "./resources/js/xin/ui/editor.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "editor", function() { return _editor__WEBPACK_IMPORTED_MODULE_9__["default"]; });

/* harmony import */ var _map__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./map */ "./resources/js/xin/ui/map.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "map", function() { return _map__WEBPACK_IMPORTED_MODULE_10__["default"]; });

/* harmony import */ var _chart__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./chart */ "./resources/js/xin/ui/chart.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chart", function() { return _chart__WEBPACK_IMPORTED_MODULE_11__["default"]; });

/* harmony import */ var _datepicker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./datepicker */ "./resources/js/xin/ui/datepicker.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "datepicker", function() { return _datepicker__WEBPACK_IMPORTED_MODULE_12__["default"]; });

/* harmony import */ var _colorpicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./colorpicker */ "./resources/js/xin/ui/colorpicker.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "colorpicker", function() { return _colorpicker__WEBPACK_IMPORTED_MODULE_13__["default"]; });

/* harmony import */ var _importFile__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./importFile */ "./resources/js/xin/ui/importFile.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "importFile", function() { return _importFile__WEBPACK_IMPORTED_MODULE_14__["default"]; });

/* harmony import */ var _img_error__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./img-error */ "./resources/js/xin/ui/img-error.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "imgError", function() { return _img_error__WEBPACK_IMPORTED_MODULE_15__["default"]; });


















/***/ }),

/***/ "./resources/js/xin/ui/loading.js":
/*!****************************************!*\
  !*** ./resources/js/xin/ui/loading.js ***!
  \****************************************/
/*! exports provided: showLoading, hideLoading */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showLoading", function() { return showLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hideLoading", function() { return hideLoading; });
//loading 索引，同时只能显示一个
var loadingIndex = null;
/**
 * 显示 loading
 * @param {{
 *     title:string
 * }|string} [options]
 */

function showLoading(options) {
  hideLoading();

  if (typeof options === 'string') {
    options = {
      title: options
    };
  } // const html = `<div style="padding-top:25px;width:200px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;margin-left:-70px;color:#333;text-align:center;font-weight:bold;">${options.title}</div>`;


  loadingIndex = layer.msg(options.title, {
    anim: 1,
    offset: '70px',
    shift: 5,
    shade: 0.01,
    icon: 16,
    time: 999999 // content: html

  });
}
/**
 * 隐藏 loading
 */

function hideLoading() {
  if (!loadingIndex) return;
  layer.close(loadingIndex);
}

/***/ }),

/***/ "./resources/js/xin/ui/map.js":
/*!************************************!*\
  !*** ./resources/js/xin/ui/map.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");
/* harmony import */ var _core_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/errors */ "./resources/js/xin/core/errors.js");
/* harmony import */ var _toast__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./toast */ "./resources/js/xin/ui/toast.js");
/* harmony import */ var _core_require__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/require */ "./resources/js/xin/core/require.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





/**
 * Map
 * @param {{
 *     el:HTMLElement|string,
 *     mapType?:boolean,
 *     overView?:boolean,
 *     toolBar?:Object,
 *     autoComplete?:HTMLElement|string,
 *     mapStyle?:string,
 *     zoom?:number,
 *     viewMode?:string,
 *     pitch?:number,
 *     expandZoomRange?:boolean,
 * }} options
 * @return {Promise<unknown>}
 */

/* harmony default export */ __webpack_exports__["default"] = (function () {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return new Promise( /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(resolve, reject) {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return Object(_core_require__WEBPACK_IMPORTED_MODULE_4__["default"])(resolveUrl(options));

            case 3:
              resolve(initMap(options));
              _context.next = 10;
              break;

            case 6:
              _context.prev = 6;
              _context.t0 = _context["catch"](0);
              Object(_toast__WEBPACK_IMPORTED_MODULE_3__["hintError"])("加载地图组件失败，轻稍后刷新重试~");
              reject(_context.t0);

            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 6]]);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
});
; // 完成一个 key

function resolveKey(options) {
  if (options.key) {
    return options.key;
  }

  var config = Object(_core__WEBPACK_IMPORTED_MODULE_1__["getConfig"])('map');

  if (!config.key) {
    throw new _core_errors__WEBPACK_IMPORTED_MODULE_2__["InvalidParamsError"]('map.key not config!');
  }

  return config.key;
} // 生成加载的地图地址


function resolveUrl(options) {
  return "https://webapi.amap.com/maps?v=1.4.15&key=".concat(resolveKey(options));
} // 初始化options


function initOptions(options) {
  if (typeof options.center === 'string') {
    options.center = options.center.split(',', 2).map(parseFloat);
  }

  return Object.assign({
    mapType: false,
    overView: false,
    toolBar: {
      liteStyle: true
    },
    autoComplete: '',
    mapStyle: 'amap://styles/' + (options.style || "normal"),
    zoom: 14 // viewMode: '3D',
    // pitch: 0
    // expandZoomRange: true,

  }, options);
} // 初始化中心点


function updatePositionFactory(container, map, options) {
  var HTML = "<input type=\"hidden\" name=\"".concat(options.name || '', "\" />");
  var positionInput = $(HTML).appendTo(container);

  var handler = function handler(lng, lat) {
    positionInput.val(lng + ',' + lat);
  };

  var center = options.center;

  if (center) {
    map.setCenter([center[0], center[1]]);
    handler(center[0], center[1]);
  }

  return handler;
} // 初始化其他组件


function initPlugins(_x3, _x4, _x5) {
  return _initPlugins.apply(this, arguments);
} // 初始化地图


function _initPlugins() {
  _initPlugins = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(map, options, updatePosition) {
    var autoCompleteInput, positionPicker, autocomplete;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            // 输入自动提示
            autoCompleteInput = $(options.autoComplete); // 地图显示的位置点

            positionPicker = null; // 点标记

            _context2.next = 4;
            return Object(_core_require__WEBPACK_IMPORTED_MODULE_4__["default"])('https://webapi.amap.com/ui/1.0/main.js');

          case 4:
            AMapUI.loadUI(['misc/PositionPicker'], function (PositionPicker) {
              //设定为拖拽地图模式，可选'dragMap'、'dragMarker'，默认为'dragMap'
              positionPicker = new PositionPicker({
                mode: options.mode || 'dragMap',
                map: map
              });
              positionPicker.start(options.center);
              var isFirst = !!options.center;
              positionPicker.on('success', function (res) {
                if (isFirst) {
                  isFirst = false;
                  return;
                }

                if (autoCompleteInput) {
                  autoCompleteInput.val(res.address);
                }

                updatePosition(res.position.lng, res.position.lat);
              });
            });
            map.addControl(new AMap.ToolBar(options.toolBar)); //工具条

            map.addControl(new AMap.Scale()); //比例尺

            options.overView && map.addControl(new AMap.OverView({
              isOpen: true
            })); //鹰眼

            options.mapType && map.addControl(new AMap.MapType()); //类别切换
            //输入提示

            if (autoCompleteInput[0] instanceof HTMLElement) {
              autocomplete = new AMap.Autocomplete({
                input: autoCompleteInput[0]
              });
              AMap.event.addListener(autocomplete, "select", function (res) {
                if (!res.poi.location) return;
                autoCompleteInput.val(res.poi.name);
                var position = [res.poi.location.lng, res.poi.location.lat];
                map.setCenter(position);

                if (positionPicker) {
                  positionPicker.marker.setPosition(position);
                }

                updatePosition(position[0], position[1]);
              });
            } //覆盖物


            options.markers && options.markers.forEach(function (item) {
              new AMap.Marker(item).setMap(map);
            }); //圆

            options.circles && options.circles.forEach(function (item) {
              new AMap.Circle(item).setMap(map);
            });
            map.setFitView();

          case 13:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _initPlugins.apply(this, arguments);
}

function initMap(_x6) {
  return _initMap.apply(this, arguments);
}

function _initMap() {
  _initMap = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(options) {
    var container, map, updatePosition, plugins;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            options = initOptions(options);
            container = $(options.el);
            map = new AMap.Map(container[0], options); // 隐藏logo和版权

            container.children(".amap-logo").remove();
            container.children(".amap-copyright").remove(); // 初始化中心点

            updatePosition = updatePositionFactory(container, map, options); // 初始化其他插件

            plugins = ['AMap.ToolBar', 'AMap.Scale'];
            options.overView && plugins.push('AMap.OverView');
            options.mapType && plugins.push('AMap.MapType');
            options.autoComplete && plugins.push('AMap.Autocomplete');
            AMap.plugin(plugins, function () {
              initPlugins(map, options, updatePosition);
            });
            return _context3.abrupt("return", map);

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _initMap.apply(this, arguments);
}

/***/ }),

/***/ "./resources/js/xin/ui/modal.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/modal.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * modal 图标
 * @param {string|number} icon
 * @return {*}
 */
var MODAL_ICONS = {
  warn: 0,
  success: 1,
  error: 2,
  question: 3,
  unauthorized: 4
};
/**
 * modal
 * @param {{
 * title?:string,
 * content?:string,
 * confirmText?:string,
 * cancelText?:string,
 * showCancel?:boolean,
 * icon?:string,
 * area?:Array,
 * thisArg?:*,
 * created?:Function,
 * closed?:Function,
 * }|*} options
 * @return Promise<*>
 */

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  if (typeof options === 'string') {
    options = {
      title: false,
      content: options,
      showCancel: false
    };
  } else {
    options = Object.assign({
      title: false,
      showCancel: false
    }, options);
  }

  if (options.icon) {
    options.icon = MODAL_ICONS[options.icon];
  }

  if (options.showCancel && options.icon === undefined) {
    options.icon = MODAL_ICONS.question;
  } // 转换与剔除不必要的字段


  options.success = options.created;
  delete options.created; // btn text

  if (options.btn !== false) {
    options.btn = [];
    options.btn.push(options.confirmText || '确认');

    if (options.showCancel) {
      options.btn.push(options.cancelText || '取消');
    }
  }

  return new Promise(function (resolve) {
    var triggerEvent = function triggerEvent(result) {
      resolve(result);

      if (result.id) {
        layer.close(result.id);
      }
    };

    options.end = function () {
      resolve({
        confirm: false,
        cancel: true,
        close: true
      });
    };

    options.yes = function (id) {
      triggerEvent({
        id: id,
        confirm: true,
        cancel: false
      });
    };

    options.btn2 = function (id) {
      triggerEvent({
        id: id,
        confirm: false,
        cancel: true
      });
    };

    layer.open(options);
  });
});
;

/***/ }),

/***/ "./resources/js/xin/ui/popup.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/popup.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  options = Object(_core__WEBPACK_IMPORTED_MODULE_0__["assign"])({
    title: false,
    type: 1,
    area: [options.width || '420px', options.height || '280px']
  }, options || {}); // 转换与剔除不必要的字段

  options.end = options.closed;
  delete options.closed;
  return new Promise(function (resolve) {
    options.success = function (layero, index) {
      resolve({
        id: index,
        popup: layero
      });
    };

    layer.open(options);
  });
});
;

/***/ }),

/***/ "./resources/js/xin/ui/preview.js":
/*!****************************************!*\
  !*** ./resources/js/xin/ui/preview.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");

/**
 * 预览
 * @param {{
 * title?:string,
 * urls:Array<string>,
 * current?:string|number,
 * }|*} options
 * @return Promise
 */

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  if (typeof options.current === "string") {
    for (var i = 0; i < options.urls.length; i++) {
      if (options.urls[i] === options.current) {
        options.current = i;
      }
    }
  }

  options = Object(_core__WEBPACK_IMPORTED_MODULE_0__["optimize"])({
    title: options.title,
    data: options.urls.map(function (url) {
      return {
        src: url
      };
    }),
    start: options.current
  });
  return new Promise(function (resolve) {
    layui.layer.photos({
      photos: options,
      anim: options.anim || 5
    });
    resolve();
  });
});
;

/***/ }),

/***/ "./resources/js/xin/ui/toast.js":
/*!**************************************!*\
  !*** ./resources/js/xin/ui/toast.js ***!
  \**************************************/
/*! exports provided: showToast, hintSuccess, hintError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showToast", function() { return showToast; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hintSuccess", function() { return hintSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hintError", function() { return hintError; });
/**
 * Toast
 * @param {{
 * content:string,
 * time?:number,
 * close?:Function
 * }|string} options
 * @return Promise<*>
 */
function showToast(options) {
  if (typeof options === 'string') {
    options = {
      content: options
    };
  }

  options = Object.assign({
    offset: '70px',
    anim: 1
  }, options);
  var content = options.content;
  delete options.content;
  return new Promise(function (resolve) {
    layer.msg(content, options, resolve);
  });
}
/**
 * 提示成功
 * @param {string} title
 * @return Promise<*>
 */

function hintSuccess(title) {
  return showToast({
    content: title,
    icon: 6
  });
}
/**
 * 提示失败
 * @param {string} title
 * @return Promise<*>
 */

function hintError(title) {
  return showToast({
    content: title,
    icon: 0,
    anim: 6
  });
}

/***/ }),

/***/ "./resources/js/xin/ui/tooltip.js":
/*!****************************************!*\
  !*** ./resources/js/xin/ui/tooltip.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Tooltip
 * @param {{
 * title:string,
 * el:*,
 * }|*} options
 */
/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  var content = options.content,
      el = options.el;
  delete options.el;
  delete options.content;
  layer.tips(content, el, options);
});

/***/ }),

/***/ "./resources/js/xin/ui/uploadbox.js":
/*!******************************************!*\
  !*** ./resources/js/xin/ui/uploadbox.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return uploadbox; });
/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request */ "./resources/js/xin/request/index.js");
/* harmony import */ var _toast__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./toast */ "./resources/js/xin/ui/toast.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core */ "./resources/js/xin/core/index.js");



/**
 * 生成一个新的 modal框
 * @param {*} options
 * @param {function} success
 * @param {function} btn2Callback
 * @returns number
 */

function newModal(options, success, btn2Callback) {
  // 图库模板
  var PICTURE_TPL = "\n<div class=\"layui-row layui-col-space10\">\n</div>\n<div class=\"layui-laypage-wrapper\" style=\"margin-top: 10px\"></div>\n";
  return layer.open({
    id: options.id || "picture-dialog",
    area: ['530px', '450px'],
    title: '图片库',
    // type: 1,
    resize: false,
    tipsMore: true,
    content: PICTURE_TPL,
    btn: ['本地上传', '确定'],
    success: success,
    yes: function yes() {
      return false;
    },
    btn3: btn2Callback
  });
}
/**
 * 生成url地址
 * @param {*} imageInfo
 * @return {*}
 */


function resolveUrl(imageInfo) {
  return imageInfo.url || imageInfo.path;
}
/**
 * 创建元素
 * @param {*} imageInfo
 * @return {*}
 */


function createItem(imageInfo) {
  var TPL = "\n<div class=\"layui-col-sm2\">\n\t<div class=\"uploader-wrapper\">\n\t\t<img src=\"".concat(resolveUrl(imageInfo), "\" mode=\"aspectFill\" class=\"uploader-image\"  alt=\"\"/>\n\t</div>\n</div>\n");
  return $(TPL);
}
/**
 * 加载数据
 * @param {*} wrapper
 * @param {*} options
 * @return {Function}
 */


function loadDataFactory(wrapper, options) {
  var layPageWrapper = wrapper.next();

  var handler = function handler(page) {
    var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 20;
    var url = options.listUrl;

    if (!url) {
      console.warn("not config picture url!");
      return;
    }

    return _request__WEBPACK_IMPORTED_MODULE_0__["request"].get(url, {
      page: page,
      limit: limit
    }, {
      tipsSuccessMsg: false
    }).then(function (res) {
      wrapper.empty();
      res.data.reverse().forEach(function (item) {
        wrapper.prepend(createItem(item));
      }); //执行一个laypage实例

      layui.laypage.render({
        elem: layPageWrapper[0],
        count: res.total_count,
        //数据总数，从服务端得到
        limit: limit,
        // 分页条数
        curr: page,
        layout: ['prev', 'page', 'next', 'limit', 'count', 'refresh', 'skip'],
        // 布局
        theme: 2,
        //自定义主题
        jump: function jump(obj, first) {
          //首次不执行
          if (first) {
            return;
          }

          handler(obj.curr, obj.limit);
        }
      });
    });
  };

  return handler;
}
/**
 * 更新选中数量
 * @param {*} confirmBtn
 * @param {*} options
 * @param {array} choiceList
 * @return {Function}
 */


function updateChooseCountFactory(confirmBtn, options, choiceList) {
  var originText = confirmBtn.text();
  return function () {
    var max = options.multi ? options.max : 1;
    confirmBtn.text("".concat(originText, "(").concat(choiceList.length, "/").concat(max, ")"));
    confirmBtn.prop('disabled', choiceList.length);
  };
} // 初始化选中图片事件


function initPicture(wrapper, options, choiceList) {
  return wrapper.on('click', '.uploader-wrapper', function () {
    var self = $(this),
        id = self.parent().data('id'); //单选特殊处理

    if (!options.multi) {
      choiceList.splice(0, choiceList.length);
      wrapper.find('.uploader-selected').removeClass('uploader-selected');
      self.addClass('uploader-selected');
      choiceList.push({
        path: self.find('img').attr('src')
      });
    } else {
      //删除选中的元素
      if (self.hasClass('uploader-selected')) {
        var index = choiceList.findIndex(function (it) {
          return id === it.id;
        });

        if (index !== -1) {
          choiceList.splice(index, 1);
        }

        self.removeClass('uploader-selected');
      } else {
        if (choiceList.length >= options.max) {
          Object(_toast__WEBPACK_IMPORTED_MODULE_1__["hintError"])("\u6700\u591A\u53EA\u80FD\u9009\u62E9".concat(options.max, "\u5F20"));
          return;
        }

        choiceList.push({
          id: id,
          path: self.find('img').attr('src')
        });
        self.addClass('uploader-selected');
      }
    }
  });
}
/**
 * 初始化上传操作
 * @param {*} wrapper
 * @param {*} uploaderBtn
 * @param {*} options
 */


function initUpload(wrapper, uploaderBtn, options) {
  var url = options.uploadUrl;

  if (!url) {
    console.warn("upload url not config!");
    return;
  }

  layui.upload.render(Object(_core__WEBPACK_IMPORTED_MODULE_2__["optimize"])({
    elem: uploaderBtn[0],
    url: url,
    field: options.field || 'file',
    //设定文件域的字段名
    accept: 'image',
    //允许上传的文件类型
    size: options.size,
    //最大允许上传的文件大小
    acceptMime: options.acceptMime,
    //规定打开文件选择框时，筛选出的文件类型
    exts: options.exts,
    //允许上传的文件后缀
    multiple: true,
    //是否允许多文件上传
    // auto: false, //选择文件后不自动上传
    done: function done(res) {
      //上传后的回调
      var response = Object(_request__WEBPACK_IMPORTED_MODULE_0__["responseWrapper"])(res);

      if (response.isSuccess) {
        wrapper.prepend(createItem(response.data));
      } else if (response.code === -1) {
        //未登录
        window.location.reload();
      } else {
        Object(_toast__WEBPACK_IMPORTED_MODULE_1__["hintError"])(response.msg || "操作失败！");
      }
    },
    error: function error() {
      Object(_toast__WEBPACK_IMPORTED_MODULE_1__["hintError"])('上传服务器失败，请稍后再试~');
    }
  }));
}
/**
 * 生成新的配置数据
 * @param {*} options
 * @return {*}
 */


function newMergeConfig(options) {
  return Object.assign({}, uploadbox.defaults, options);
}
/**
 *
 * @param {{
 *     listUrl?:string,
 *     uploadUrl?:string,
 * }|*} options
 * @return {Promise<Array<string>>}
 */


function uploadbox() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  options = newMergeConfig(options);
  return new Promise(function (resolve) {
    // 选中的项
    var choiceList = []; // 弹出图片库

    newModal(options, function (dialog) {
      var wrapper = dialog.find('.layui-layer-content'),
          pictureWrapper = wrapper.children().eq(0),
          uploaderBtn = dialog.find('.layui-layer-btn0'),
          confirmBtn = dialog.find('.layui-layer-btn1');
      dialog.attr('type', 'page'); // 生成函数

      var loadData = loadDataFactory(pictureWrapper, options);
      var updateChooseCount = updateChooseCountFactory(confirmBtn, options, choiceList); // 初始化相册

      initPicture(pictureWrapper, options, choiceList).on('click', '.uploader-wrapper', function () {
        updateChooseCount();
        return false;
      }); // 初始化上传操作

      initUpload(pictureWrapper, uploaderBtn, options);
      loadData(0);
      updateChooseCount();
    }, function (dialogId) {
      if (choiceList.length === 0) {
        Object(_toast__WEBPACK_IMPORTED_MODULE_1__["hintError"])('请选择图片');
        return false;
      }

      layer.close(dialogId);
      resolve(choiceList.map(function (it) {
        return it.path;
      }));
    });
  });
}
;
uploadbox.defaults = {
  max: 1,
  size: 1024,
  field: 'image',
  acceptMime: 'image/*',
  exts: 'jpg|png|gif|bmp|jpeg'
};

/***/ }),

/***/ "./resources/js/xin/vendor/index.js":
/*!******************************************!*\
  !*** ./resources/js/xin/vendor/index.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layui__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layui */ "./resources/js/xin/vendor/layui.js");
/* harmony import */ var _layui__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_layui__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./jquery */ "./resources/js/xin/vendor/jquery.js");
/* harmony import */ var _jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_jquery__WEBPACK_IMPORTED_MODULE_1__);



/***/ }),

/***/ "./resources/js/xin/vendor/jquery.js":
/*!*******************************************!*\
  !*** ./resources/js/xin/vendor/jquery.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//validate默认验证规则的提示语中文化
$.validator && $.extend($.validator.messages, {
  required: "必填",
  remote: "请修正该字段",
  email: "电子邮件格式不正确",
  url: "网址格式不正确",
  date: "日期格式不正确",
  dateISO: "请输入合法的日期 (ISO).",
  number: "请输入数字",
  digits: "只能输入整数",
  creditcard: "请输入合法的信用卡号",
  equalTo: "请再次输入相同的值",
  accept: "请输入拥有合法后缀名的字符",
  maxlength: $.validator.format("请输入一个 长度最多是 {0} 的字符"),
  minlength: $.validator.format("请输入一个 长度最少是 {0} 的字符"),
  rangelength: $.validator.format("请输入 一个长度介于 {0} 和 {1} 之间的字符"),
  range: $.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
  max: $.validator.format("请输入一个最大为{0} 的值"),
  min: $.validator.format("请输入一个最小为{0} 的值")
});

/***/ }),

/***/ "./resources/js/xin/vendor/layui.js":
/*!******************************************!*\
  !*** ./resources/js/xin/vendor/layui.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

if (window.layui) {
  var $ = window.jQuery = window.$ = layui.$;
  var layer = window.layer = layui.layer;
  layer.config({
    moveType: 1,
    shade: 0.1,
    shift: 0,
    anim: 0
  });
} // // 让Layui可以加载第三方库
// if (!window.define) {
// 	window.define = function(factory) {
// 		const jsPath = document.currentScript.src;
// 		const key = jsPath.substring(jsPath.lastIndexOf('/') + 1, jsPath.lastIndexOf('.'));
// 		layui.define(function(exports) {
// 			exports(key.toLocaleLowerCase(), factory());
// 		});
// 	};
// 	window.define.amd = true;
// }

/***/ }),

/***/ "./resources/js/xin/vue/layui/checkbox.js":
/*!************************************************!*\
  !*** ./resources/js/xin/vue/layui/checkbox.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// checkbox
window.Vue && Vue.component('layui-checkbox', {
  template: "\n\t\t<div class=\"layui-unselect layui-form-checkbox\" lay-skin=\"primary\" :class=\"classs\" @click=\"onCheck\">\n\t\t\t<span><slot></slot></span>\n\t\t\t<i class=\"layui-icon layui-icon-ok\"></i>\n\t\t</div>\n\t",
  model: {
    prop: 'checked',
    event: 'change'
  },
  props: {
    value: {
      type: null,
      "default": true
    },
    checked: null,
    disbaled: Boolean
  },
  computed: {
    isChecked: function isChecked() {
      if (this.checked !== undefined) {
        var values = _typeof(this.checked) === 'object' && this.checked.length !== undefined ? this.checked : [];
        return values.indexOf(this.value) !== -1;
      } else {
        return this.checked;
      }
    },
    classs: function classs() {
      return {
        'layui-checkbox-disbaled': this.disbaled,
        'layui-disabled': this.disbaled,
        'layui-form-checked': this.isChecked
      };
    }
  },
  methods: {
    onCheck: function onCheck() {
      if (this.disbaled) return;

      if (typeof this.value === 'boolean') {
        this.checked = true;
        this.$emit('change', true);
      } else {
        var value = this.checked;

        if (_typeof(value) !== 'object' || value.length === undefined) {
          value = [];
        }

        var index = value.indexOf(this.value);

        if (index === -1) {
          value.push(this.value);
        } else {
          value.splice(index, 1);
        }

        this.$emit('change', value);
      }
    }
  }
});

/***/ }),

/***/ "./resources/js/xin/vue/layui/index.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/vue/layui/index.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input */ "./resources/js/xin/vue/layui/input.js");
/* harmony import */ var _input__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_input__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./radio */ "./resources/js/xin/vue/layui/radio.js");
/* harmony import */ var _radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _checkbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./checkbox */ "./resources/js/xin/vue/layui/checkbox.js");
/* harmony import */ var _checkbox__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_checkbox__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select */ "./resources/js/xin/vue/layui/select.js");
/* harmony import */ var _select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_select__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _switch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./switch */ "./resources/js/xin/vue/layui/switch.js");
/* harmony import */ var _switch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_switch__WEBPACK_IMPORTED_MODULE_4__);






/***/ }),

/***/ "./resources/js/xin/vue/layui/input.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/vue/layui/input.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// input
window.Vue && Vue.component('layui-input', {
  template: "\n<textarea class=\"layui-textarea\" v-if=\"'textarea'===type\" :value=\"value\" @input=\"$emit('input',$event.target.value)\"></textarea>\n<input :type=\"type\" class=\"layui-input\" :value=\"value\" @input=\"$emit('input',$event.target.value)\" v-else/>\n",
  props: {
    value: null,
    type: {
      type: String,
      "default": 'text'
    }
  }
});

/***/ }),

/***/ "./resources/js/xin/vue/layui/radio.js":
/*!*********************************************!*\
  !*** ./resources/js/xin/vue/layui/radio.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// radio
window.Vue && Vue.component('layui-radio', {
  template: "\n<div class=\"layui-unselect layui-form-radio\" :class=\"classs\" @click=\"onCheck\">\n\t<div style=\"display: none !important;\">\n\t\t<input type=\"radio\" :name=\"name\" :value=\"value\" lay-ignore @change=\"onChange\" ref=\"radio\" :checked=\"isChecked\" />\n\t</div>\n\t<i class=\"layui-anim layui-icon\">{{isChecked?\"\uE643\":\"\uE63F\"}}</i>\n\t<div><slot></slot></div>\n</div>\n",
  model: {
    prop: 'checked',
    event: 'change'
  },
  props: {
    value: {
      type: null,
      "default": true
    },
    checked: null,
    name: String
  },
  data: function data() {
    return {
      selectValue: null
    };
  },
  computed: {
    isChecked: function isChecked() {
      return this.value == this.selectValue;
    },
    classs: function classs() {
      return {
        'layui-form-radioed': this.isChecked
      };
    }
  },
  created: function created() {
    this.selectValue = this.checked;

    if (this._getRadioGroup) {
      this.selectValue = this._getRadioGroup.value;
    }
  },
  methods: {
    onCheck: function onCheck() {
      if (typeof this.value === 'boolean') {
        this.selectValue = true;
        this.$emit('change', true);
      } else {
        this.selectValue = this.value;
        this.$refs.radio.checked = true;
        this.dispatchEvent();
        this.$emit('change', this.value);
      }
    },
    dispatchEvent: function dispatchEvent() {
      var _this = this;

      if (!this.name) {
        return;
      }

      var radios = document.getElementsByName(this.name);
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent("change", false, true);

      if (typeof this.value === 'boolean') {
        evt.value = false;
      } else {
        evt.value = this.selectValue;
      }

      radios.forEach(function (radio) {
        if (radio === _this.$refs.radio) {
          return;
        }

        radio.checked = false;
        radio.dispatchEvent(evt);
      });
    },
    onChange: function onChange(e) {
      this.selectValue = e.value;
    }
  },
  watch: {
    checked: function checked(value) {
      this.selectValue = value;
    }
  }
});

/***/ }),

/***/ "./resources/js/xin/vue/layui/select.js":
/*!**********************************************!*\
  !*** ./resources/js/xin/vue/layui/select.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// select
window.Vue && Vue.component('layui-select', {
  template: "\n<div class=\"layui-unselect layui-form-select\" :class=\"classs\">\n\t<div class=\"layui-select-title\">\n\t\t<input type=\"hidden\" :name=\"name\" :value=\"selectedItem && selectedItem.value\"/>\n\t\t<input type=\"text\" placeholder=\"\u8BF7\u9009\u62E9\" :value=\"selectedItem && selectedItem.title\" class=\"layui-input layui-unselect\" @click.stop=\"showList\" readonly/>\n\t\t<i class=\"layui-edge\"></i>\n\t</div>\n\t<dl class=\"layui-anim layui-anim-upbit\" style=\"\">\n\t\t<dd class=\"layui-select-tips\" @click.stop=\"onSelect(null)\">\u8BF7\u9009\u62E9</dd>\n\t\t<dd v-for=\"item in items\" :class=\"{'layui-this':isSelectItem(item)}\" @click.stop=\"onSelect(item)\">{{item.title}}</dd>\n\t</dl>\n</div>\n",
  model: {
    event: "change"
  },
  props: {
    items: {
      type: Array,
      "default": []
    },
    name: String,
    value: null
  },
  computed: {
    classs: function classs() {
      return {
        'layui-form-selected': this.isShowList
      };
    }
  },
  data: function data() {
    return {
      selectedItem: null,
      isShowList: false
    };
  },
  created: function created() {
    var self = this;

    var handler = function handler() {
      self.isShowList = false;
    };

    document.addEventListener('click', handler);
    this.$once('hook:beforeDestroy', function () {
      document.removeEventListener('click', handler);
    });
    this.updateRender();
  },
  methods: {
    showList: function showList() {
      this.isShowList = true;
    },
    isSelectItem: function isSelectItem(item) {
      return this.selectedItem && item.value === this.selectedItem.value;
    },
    onSelect: function onSelect(item) {
      this.selectedItem = item;
      this.isShowList = false;
      this.$emit('change', item ? item.value : null);
    },
    updateRender: function updateRender() {
      for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];

        if (this.value === item.value) {
          this.selectedItem = item;
          break;
        }
      }
    }
  },
  watch: {
    value: 'updateRender'
  }
});

/***/ }),

/***/ "./resources/js/xin/vue/layui/switch.js":
/*!**********************************************!*\
  !*** ./resources/js/xin/vue/layui/switch.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// switch
window.Vue && Vue.component('layui-switch', {
  template: "\n<div class=\"layui-unselect layui-form-switch\" lay-skin=\"_switch\" :class=\"classs\" @click=\"onCheck\">\n\t<em>{{showTitle}}</em><i></i>\n</div>\n",
  model: {
    prop: 'checked',
    event: 'change'
  },
  props: {
    value: {
      type: Array,
      "default": [false, true]
    },
    text: {
      type: String,
      "default": '关闭|开启'
    },
    checked: null
  },
  computed: {
    showTitle: function showTitle() {
      var title = this.text.split('|', 2);
      return this.value[0] === this.checked ? title[0] : title[1];
    },
    classs: function classs() {
      return {
        'layui-form-onswitch': this.value[0] !== this.checked
      };
    }
  },
  methods: {
    onCheck: function onCheck() {
      this.$emit('change', this.value[0] === this.checked ? this.value[1] : this.value[0]);
    }
  }
});

/***/ }),

/***/ "./resources/sass/admin/app.scss":
/*!***************************************!*\
  !*** ./resources/sass/admin/app.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/admin/login.scss":
/*!*****************************************!*\
  !*** ./resources/sass/admin/login.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/index/app.scss":
/*!***************************************!*\
  !*** ./resources/sass/index/app.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*****************************************************************************************************************************************!*\
  !*** multi ./resources/js/xin/app.js ./resources/sass/index/app.scss ./resources/sass/admin/app.scss ./resources/sass/admin/login.scss ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\www\thinkphp-blog\resources\js\xin\app.js */"./resources/js/xin/app.js");
__webpack_require__(/*! D:\www\thinkphp-blog\resources\sass\index\app.scss */"./resources/sass/index/app.scss");
__webpack_require__(/*! D:\www\thinkphp-blog\resources\sass\admin\app.scss */"./resources/sass/admin/app.scss");
module.exports = __webpack_require__(/*! D:\www\thinkphp-blog\resources\sass\admin\login.scss */"./resources/sass/admin/login.scss");


/***/ })

/******/ });