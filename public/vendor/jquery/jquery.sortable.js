/*
 * HTML5 Sortable jQuery Plugin
 * http://farhadi.ir/projects/html5sortable
 *
 * Copyright 2012, Ali Farhadi
 * Released under the MIT license.
 */
(function ($) {
	var dragging, placeholders = $();
	$.fn.sortable = function (options) {
		var method = String(options);
		options = $.extend({
			pull: false,
			push: false,
			clone: false,
			items: "",
			sort: true
		}, options);
		return this.each(function () {
			var items = null;
			if (/^(enable|disable|destroy)$/.test(method)) {
				items = $(this).children($(this).data('items')).attr('draggable', method === 'enable');
				if (method === 'destroy') {
					items.add(this).off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
				}
				return;
			}

			var index;
			items = $(this).children(options.items);
			items.mousedown(function () {
				var tempItem;
				if (options.clone) tempItem = options.onClone ? options.onClone() : this.cloneNode(true);
				else tempItem = this;

				tempItem = $(tempItem);
				tempItem.attr('draggable', 'true');
				$(this).after(tempItem);
				tempItem.on('dragstart.h5s', function (e) {
					var dt = e.originalEvent.dataTransfer;
					if (dt === undefined) return false;
					dt.effectAllowed = 'move';
					dt.setData('Text', 'dummy');
					index = (dragging = $(this)).addClass('sortable-dragging').index();
				});
			});

			$(this).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
				if (!options.push)return true;

				if (e.type === 'drop') {
					e.stopPropagation();
					$(e.currentTarget).append(dragging);
					dragging.trigger('dragend.h5s');
					console.log(dragging, e);
					return false;
				}

				return false;
			});


			// var isHandle, index;
			// var placeholder = $('<' + (/^(ul|ol)$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
			// items.find(options.handle).mousedown(function () {
			// 	isHandle = true;
			// }).mouseup(function () {
			// 	isHandle = false;
			// });
			// $(this).data('items', options.items);
			// placeholders = placeholders.add(placeholder);
			//
			// items.attr('draggable', 'true').on('dragstart.h5s', function (e) {
			// 	if (options.handle && !isHandle) {
			// 		return false;
			// 	}
			// 	isHandle = false;
			// 	var dt = e.originalEvent.dataTransfer;
			// 	if (dt === undefined) return false;
			// 	dt.effectAllowed = 'move';
			// 	dt.setData('Text', 'dummy');
			// 	index = (dragging = $(this)).addClass('sortable-dragging').index();
			// }).on('dragend.h5s', function () {
			// 	if (!dragging) {
			// 		return;
			// 	}
			// 	dragging.removeClass('sortable-dragging').show();
			// 	placeholders.detach();
			// 	if (index !== dragging.index()) {
			// 		dragging.parent().trigger('sortupdate', {item: dragging});
			// 	}
			// 	dragging = null;
			// }).not('a[href], img').on('selectstart.h5s', function () {
			// 	this.dragDrop && this.dragDrop();
			// 	return false;
			// }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
			// 	if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
			// 		return true;
			// 	}
			// 	if (e.type === 'drop') {
			// 		e.stopPropagation();
			// 		placeholders.filter(':visible').after(dragging);
			// 		dragging.trigger('dragend.h5s');
			// 		return false;
			// 	}
			// 	e.preventDefault();
			// 	e.originalEvent.dataTransfer.dropEffect = 'move';
			// 	if (items.is(this)) {
			// 		if (options.forcePlaceholderSize) {
			// 			placeholder.height(dragging.outerHeight());
			// 		}
			// 		dragging.hide();
			// 		$(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
			// 		placeholders.not(placeholder).detach();
			// 	} else if (!placeholders.is(this) && !$(this).children(options.items).length) {
			// 		placeholders.detach();
			// 		$(this).append(placeholder);
			// 	}
			// 	return false;
			// });
		});
	};
})(jQuery);
