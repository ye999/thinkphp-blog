import {close, open} from "../ui";

/**
 * 页面选择
 * @param {{
 * url:string,
 * }|*} options
 * @return Promise
 */
export default function(options) {
	return new Promise(function(resolve, reject) {
		open(options).then(function(res) {
			try {
				res.iframe.contentWindow.finish = (result) => {
					resolve(result);
					close(res.index);
				};
			} catch (e) {
				reject({
					errMsg: "不支持跨域选择"
				});
			}
		}, reject);
	});
}
