/**
 * 模板引擎
 * @param {{
 * template:string|HTMLElement,
 * data:Object,
 * success:Function|HTMLElement
 * }|*} options
 * @return {String}
 */
export default function tpl(options) {
	return layui.laytpl(options.template).render(options.data, options.success);
};
