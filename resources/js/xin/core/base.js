/**
 * 合并一个对象
 * @param {*} target
 * @return {*}
 */
export function assign(target) {
	'use strict';
	if (target == null) {
		throw new TypeError('Cannot convert undefined or null to object');
	}

	target = Object(target);
	for (let index = 1; index < arguments.length; index++) {
		const source = arguments[index];
		if (source != null) {
			for (const key in source) {
				if (Object.prototype.hasOwnProperty.call(source, key)) {
					target[key] = source[key];
				}
			}
		}
	}
	return target;
}

/**
 * 优化对象
 * @param {*} obj
 * @return {*}
 */
export function optimize(obj) {
	for (const k in obj) {
		if (obj.hasOwnProperty(k) && obj[k] === undefined) {
			delete obj[k];
		}
	}

	return obj;
}

/**
 * 克隆对象
 * @param obj
 * @return {*}
 */
export function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

/**
 * 解析URL
 * @param  {string} url 被解析的URL
 * @return {object}     解析后的数据
 */
export function parseUrl(url) {
	const parse = url.match(/^(?:([a-z]+):\/\/)?([\w-]+(?:\.[\w-]+)+)?(?::(\d+))?([\w-\/.]+)?(?:\?((?:\w+=[^#&=\/]*)?(?:&\w+=[^#&=\/]*)*))?(?:#([\w-]+))?$/i);
	if (!parse) throw new Error("url格式不正确！");
	return {
		scheme: parse[1],
		host: parse[2],
		port: parse[3],
		path: parse[4],
		query: parse[5],
		fragment: parse[6]
	};
}

/**
 * 解析Url Query字符串
 * @param {string} str
 * @returns {{}}
 */
export function parseQuery(str) {
	if (!str) return {};
	let value = str.split("&"), vars = {}, param;
	for (const val in value) {
		param = value[val].split("=");
		vars[param[0]] = param[1];
	}
	return vars;
}

/**
 * 解析名称（下划线与驼峰互转）
 * @param {string} name
 * @param {boolean} [type] 转换类型
 * @returns {*}
 */
export function parseName(name, type) {
	if (type) {
		/* 下划线转驼峰 */
		name.replace(/_([a-z])/g, ($0, $1) => $1.toUpperCase());
		/* 首字母大写 */
		name.replace(/[a-z]/, ($0) => $0.toUpperCase());
	} else {
		/* 大写字母转小写 */
		name = name.replace(/[A-Z]/g, ($0) => "_" + $0.toLowerCase());
		/* 去掉首字符的下划线 */
		if (0 === name.indexOf("_")) {
			name = name.substr(1);
		}
	}
	return name;
}

/**
 * 获取文件后缀名
 * @param {string} file
 * @returns {string}
 */
export function getFileExtensionName(file) {
	let index = file.lastIndexOf(".");
	return file.substring(index + 1, file.length);
}

/**
 * 安全调用函数
 * @param {*} thisArg
 * @param {function} func
 * @param {array} [params]
 */
export function safeCallback(thisArg, func, params = []) {
	if (!func || typeof func !== "function") {
		return;
	}

	try {
		return func.apply(thisArg, params);
	} catch (e) {
		console.error(e);
	}
}

