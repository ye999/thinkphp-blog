import * as xin from "../index";

//color picker
$(function() {
	$('[data-colorpicker]').each(function() {
		const options = $.extend({
			elem: this,
			done: function(color) {
				if (this.target) {
					$(this.target).val(color);
				}
			}
		}, xin.getDataOptions(this, 'colorpicker'));
		xin.colorpicker(options);
	});
});
