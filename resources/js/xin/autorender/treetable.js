import * as xin from "../index";

//treetable
$(function() {
	$('[data-tree-table]').each(function() {
		const self = $(this);

		const options = $.extend({
			expandable: true,
			stringCollapse: '闭合',
			stringExpand: '展开',
			initialState: 'expanded'
		}, xin.getDataOptions(self, 'treeTable'));

		self.treetable(options);
	});
});
