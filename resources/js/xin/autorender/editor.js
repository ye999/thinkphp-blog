import * as xin from "../index";

//editor
$(function() {
	$('[data-editor]').each(function() {
		const self = $(this);
		const options = $.extend({
			name: self.attr('name')
		}, xin.getDataOptions(self, 'editor'), {
			el: this,
		});
		xin.editor(options).then(function(editor) {
			self[0].editor = editor;
		});
	});
});
