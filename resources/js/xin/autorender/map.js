import * as xin from "../index";

// 地图
$(function() {
	$('[data-map]').each(function() {
		const self = $(this),
			markers = self.data('markers') || [],
			circles = self.data('circles') || [],
			center = self.data("center") || "",
			name = self.data('name') || "location";

		const options = $.extend(xin.getDataOptions(self, 'map'), {
			el: this,
			markers: markers,
			circles: circles,
			center: center,
			name: name,
		});

		xin.map(options).then((map) => {
			this.map = map;
		});
	});
});
