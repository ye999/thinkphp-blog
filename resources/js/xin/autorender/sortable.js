import * as xin from "../index";

//sortable
$(function() {
	const sortable = $('[data-sortable]');
	if (!sortable.length) {
		return;
	}
	xin.dynamicImport(location.origin + '/vendor/sortable/Sortable.min.js').then(function() {
		sortable.each(function() {
			const self = $(this);
			const options = $.extend({
				connectWith: "[data-sortable]",
				cursor: "move"
			}, xin.getDataOptions(self, 'sortable'));

			new Sortable(self[0], xin.assign({
				animation: 150,
			}, options));
		});
	});

});
