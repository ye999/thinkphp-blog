import * as xin from "../index";

//uploader
$(function() {
	$('[data-img-uploader]').each(function() {
		const options = xin.getDataOptions(this, 'imgUploader');
		options.el = this;
		xin.uploader(options);
	});
});
