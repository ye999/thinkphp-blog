import * as xin from './index';
import './interceptor/request.default';
import './vendor';
import './vue/layui'
import './autorender';

//解析地址栏参数
Object.defineProperty(xin, 'query', {
	get: function() {
		return xin.parseQuery(window ? window.location.search.substr(1) : "");
	}
});

// 基本配置
xin.setConfig({
	map: {
		key: '4dcc5c8b956a746a3f6ab1c47a8f15d5'
	}
});

window.xin = xin;
