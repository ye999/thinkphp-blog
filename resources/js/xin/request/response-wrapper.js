/**
 * 智能验证服务器返回数据合法性
 * @param {{msg?:string,info?:string,message?:string,errMsg?:string,errorMsg?:string,code?number,status?number,errorCode?number,errCode?number,error?number,data?:*,result?:*,}} res
 * @return {{code: number, msg: string, isSuccess: boolean, data: *}}
 */
export default function(res) {
	let code, msg, data, isSuccess = false;

	if (res.msg) {
		msg = res.msg;
	} else if (res.info) {
		msg = res.info;
	} else if (res.message) {
		msg = res.message;
	} else if (res.errMsg) {
		msg = res.errMsg;
	} else if (res.errorMsg) {
		msg = res.errorMsg;
	}

	if (undefined !== res.code) {
		isSuccess = (code = res.code) === 1;
	} else if (undefined !== res.status) {
		isSuccess = (code = res.status) === 1;
		if (!isSuccess) msg = res.info;
	} else if (undefined !== res.errorCode) {
		isSuccess = (code = res.errorCode) === 0;
	} else if (undefined !== res.errCode) {
		isSuccess = (code = res.errCode) === 0;
	} else if (undefined !== res.error && !isNaN(res.error)) {
		isSuccess = (code = res.error) === 0;
	} else {
		throw new Error('找不到合适验证状态码，请确保返回的数据含有：status,code,errCode,errorCode,error 其中的一项！');
	}

	if (undefined !== res.data) {
		data = res.data;
	} else if (undefined !== res.result) {
		data = res.result;
	} else {
		data = res;
	}

	return {code: code, msg: msg, isSuccess: isSuccess, data: data};
};
