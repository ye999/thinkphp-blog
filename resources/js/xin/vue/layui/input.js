// input
window.Vue && Vue.component('layui-input', {
	template: `
<textarea class="layui-textarea" v-if="'textarea'===type" :value="value" @input="$emit('input',$event.target.value)"></textarea>
<input :type="type" class="layui-input" :value="value" @input="$emit('input',$event.target.value)" v-else/>
`,
	props: {
		value: null,
		type: {
			type: String,
			default: 'text'
		}
	},
});
