/**
 * Tooltip
 * @param {{
 * title:string,
 * el:*,
 * }|*} options
 */
export default function(options) {
	const content = options.content,
		el = options.el;

	delete options.el;
	delete options.content;

	layer.tips(content, el, options);
}
