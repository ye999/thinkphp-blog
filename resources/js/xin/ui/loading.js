//loading 索引，同时只能显示一个
let loadingIndex = null;

/**
 * 显示 loading
 * @param {{
 *     title:string
 * }|string} [options]
 */
export function showLoading(options) {
	hideLoading();

	if (typeof options === 'string') {
		options = {
			title: options,
		};
	}

	// const html = `<div style="padding-top:25px;width:200px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;margin-left:-70px;color:#333;text-align:center;font-weight:bold;">${options.title}</div>`;
	loadingIndex = layer.msg(options.title, {
		anim: 1,
		offset: '70px',
		shift: 5,
		shade: 0.01,
		icon: 16,
		time: 999999
		// content: html
	});
}

/**
 * 隐藏 loading
 */
export function hideLoading() {
	if (!loadingIndex) return;
	layer.close(loadingIndex);
}
