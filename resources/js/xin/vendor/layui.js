if (window.layui) {
	const $ = window.jQuery = window.$ = layui.$;
	const layer = window.layer = layui.layer;

	layer.config({
		moveType: 1,
		shade: 0.1,
		shift: 0,
		anim: 0,
	});
}

// // 让Layui可以加载第三方库
// if (!window.define) {
// 	window.define = function(factory) {
// 		const jsPath = document.currentScript.src;
// 		const key = jsPath.substring(jsPath.lastIndexOf('/') + 1, jsPath.lastIndexOf('.'));
// 		layui.define(function(exports) {
// 			exports(key.toLocaleLowerCase(), factory());
// 		});
// 	};
// 	window.define.amd = true;
// }

