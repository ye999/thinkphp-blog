<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app;

use think\exception\ValidateException;

/**
 * 基础控制器
 *
 * @property-read \app\Request $request
 */
class BaseController extends \think\Controller{

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * 验证数据
	 *
	 * @access protected
	 * @param array        $data 数据
	 * @param string|array $validate 验证器名或者验证规则数组
	 * @param array        $message 提示信息
	 * @param bool         $batch 是否批量验证
	 * @param mixed        $callback 回调方法（闭包）
	 * @return array|string|true
	 * @throws ValidateException
	 */
	protected function validate($data, $validate, $message = [], $batch = false, $callback = null){
		if(is_array($validate)){
			$v = $this->app->validate();
			$v->rule($validate);
		}else{
			if(strpos($validate, '.')){
				// 支持场景
				list($validate, $scene) = explode('.', $validate);
			}
			$v = $this->app->validate($validate, 'validate', true);
			if(!empty($scene)){
				$v->scene($scene);
			}
		}

		// 是否批量验证
		if($batch || $this->batchValidate){
			$v->batch(true);
		}

		if(is_array($message)){
			$v->message($message);
		}

		if($callback && is_callable($callback)){
			call_user_func_array($callback, [$v, &$data]);
		}

		if(!$v->check($data)){
			if($this->failException){
				throw new ValidateException($v->getError());
			}
			return $v->getError();
		}

		return true;
	}
}
