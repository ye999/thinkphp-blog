<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/7/27 18:13
 */

namespace app\index\weight;

use app\common\model\Post as PostModel;
use Xin\Thinkphp5\Weight\Weight;

class PostWeight extends Weight{

	/**
	 * 最新发布
	 *
	 * @param int $limit
	 * @param int $page
	 * @return mixed
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function newList($limit = 10, $page = 1){
		$data = $this->lists([], 'id desc', 'category', $page, $limit);
		$this->view->assign('data', $data);
		return $this->view->fetch('post/weight/list');
	}

	/**
	 * 最新热议
	 *
	 * @param int $limit
	 * @param int $page
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function hotList($limit = 5, $page = 1){
		$data = $this->lists([], 'view_count desc,id desc', null, $page, $limit);
		$this->view->assign('data', $data);
		return $this->view->fetch('post/weight/hot_list');
	}

	/**
	 * 最新热议
	 *
	 * @param int $limit
	 * @param int $page
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function goodList($limit = 5, $page = 1){
		$data = $this->lists([
			['good_time', '<>', '0'],
		], 'good_time desc', null, $page, $limit);
		$this->view->assign('data', $data);
		return $this->view->fetch('post/weight/good_list');
	}

	/**
	 * 获取列表
	 *
	 * @param array  $where
	 * @param string $order
	 * @param array  $with
	 * @param int    $page
	 * @param int    $limit
	 * @return array|string|\think\Collection
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function lists(array $where = [], $order = 'id desc', $with = null, $page = 1, $limit = 10){
		$field = 'id,title,category_id,cover,view_count,good_count,comment_count,update_time';
		return PostModel::with($with)
			->where('status', 1)
			->where($where)
			->field($field)
			->order($order)
			->page($page, $limit)
			->select();
	}

}
