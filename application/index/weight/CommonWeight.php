<?php
/**
 * The following code, none of which has BUG.
 *
 * @author: BD<657306123@qq.com>
 * @date: 2019/12/7 16:22
 */

namespace app\index\weight;

use think\Db;
use think\db\Query;
use think\facade\Cache;
use Xin\Thinkphp5\Weight\Weight;

class CommonWeight extends Weight{

	/**
	 * 显示友链
	 *
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function links(){
		$cacheKey = 'site_links';
		$links = Cache::get($cacheKey);
		if(empty($links)){
			$links = Db::name('link')->where('status', 1)
				->where(function(Query $query){
					$query->where('expire_time', '>', $this->request->time())
						->whereOr('expire_time', '=', '0');
				})
				->select();
			Cache::set($cacheKey, $links);
		}

		$this->view->assign('data', $links);
		return $this->view->fetch('public/weight/links');
	}

}
