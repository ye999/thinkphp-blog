<?php
/**
 * I know no such things as genius,it is nothing but labor and diligence.
 *
 * @copyright (c) 2015~2019 BD All rights reserved.
 * @license       http://www.apache.org/licenses/LICENSE-2.0
 * @author        <657306123@qq.com> LXSEA
 */
namespace app\index;

use app\BaseController;

/**
 * 前台基础控制器
 *
 * @property-read \think\App                         $app
 * @property-read \app\common\contract\UserInterface $user
 * @package app\common\controller
 */
class Controller extends BaseController{

	/**
	 * 验证失败直接抛出异常
	 *
	 * @var bool
	 */
	protected $failException = true;

	/**
	 * 中间件
	 *
	 * @var array
	 */
	protected $middleware = [];

	/**
	 * 用户接口
	 *
	 * @var \app\common\contract\UserInterface
	 */
	protected $user;

	/**
	 * 初始化
	 */
	protected function initialize(){
		//		$this->user = $this->app['user'];
	}

	/**
	 * 渲染config信息
	 *
	 * @param array $fields
	 * @return mixed
	 */
	protected function getConfigRender(array $fields){
		$this->assign('fields', $fields);
		return $this->fetch('public/config');
	}

}
