<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/16 22:12
 */
use app\common\middleware\ViewInit;
use app\index\middleware\CheckSiteState;

return [
	ViewInit::class,
	CheckSiteState::class,
];
