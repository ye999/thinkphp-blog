<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 *
 */

namespace app\admin\controller;

use app\admin\Controller;
use Xin\Thinkphp5\Controller\CURD;
use app\common\facade\View;
use app\common\model\Advertising as AdvertisingModel;
use app\common\model\AdvertisingItem as AdvertisingItemModel;

/**
 * 广告位项管理
 *
 *
 */
class AdvertisingItemController extends Controller{

	use CURD;

	/**
	 * 初始化
	 *
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	protected function initialize(){
		parent::initialize();

		if(in_array($this->request->action(), [
			'create', 'update', 'index',
		])){
			$this->assignAdList();
		}
	}

	/**
	 * 获取广告列表
	 *
	 * @return mixed
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$data = AdvertisingItemModel::order('sort asc')->paginate($this->request->limit(), false, [
			'page'  => $this->request->page(),
			'query' => $this->request->get(),
		]);

		$this->assign('data', $data);

		return View::fetch();
	}

	/**
	 * 向页面赋值
	 *
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	private function assignAdList(){
		$adList = AdvertisingModel::field('id,title')->select();
		$this->assign('ad_list', $adList);
	}
}
