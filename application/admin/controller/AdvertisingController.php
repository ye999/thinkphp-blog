<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 *
 */

namespace app\admin\controller;

use app\admin\Controller;
use Xin\Thinkphp5\Controller\CURD;
use app\common\facade\View;
use app\common\model\Advertising as AdvertisingModel;

/**
 * 广告位管理
 *
 *
 */
class AdvertisingController extends Controller{

	use CURD;

	/**
	 * 获取广告位列表
	 *
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$data = AdvertisingModel::order('id desc')->paginate($this->request->limit(), false, [
			'page'  => $this->request->page(),
			'query' => $this->request->get(),
		]);

		$this->assign('data', $data);

		return View::fetch();
	}

}
