<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 *
 */

namespace app\admin\controller;

use app\admin\Controller;
use Xin\Thinkphp5\Controller\CURD;
use app\common\facade\View;
use app\common\model\Badge as BadgeModel;

/**
 * 标签管理
 *
 *
 */
class BadgeController extends Controller{

	use CURD;

	/**
	 * 获取广告位列表
	 *
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$data = BadgeModel::order('sort asc')->select();

		$this->assign('data', $data);

		return View::fetch();
	}
}
