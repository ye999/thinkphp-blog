<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 *
 */

namespace app\admin\validate;

use think\Validate;

class LoginValidate extends Validate{

	/**
	 * @var array
	 */
	protected $rule = [
		'user' => 'require|alphaDash',
		'pwd'  => 'require|alphaDash',
	];

	/**
	 * @var array
	 */
	protected $message = [];

	/**
	 * @var array
	 */
	protected $field = [
		'user' => '用户名',
		'pwd'  => '密码',
	];
}
