<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */
declare (strict_types = 1);

namespace app\common\support;

use think\facade\Hook;
use think\facade\Log;

final class SafeEventEmitter{

	/**
	 * 触发事件
	 *
	 * @access public
	 * @param string|object $event 事件名称
	 * @param mixed         $params 传入参数
	 * @param bool          $once 只获取一个有效返回值
	 * @return mixed
	 */
	public static function trigger($event, $params = null, bool $once = false){
		try{
			if(class_exists('\think\facade\Event')){
				/** @noinspection PhpUndefinedClassInspection */
				return \think\facade\Event::trigger($event, $params, $once);
			}else{
				return Hook::listen($event, $params, $once);
			}
		}catch(\Exception $e){
			Log::error($e->getMessage()."\n".$e->getTraceAsString());
		}

		return null;
	}
}
