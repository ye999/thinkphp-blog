<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */
namespace app\common\support;

class MimeType{

	/**
	 * @var string[]
	 */
	protected $data = [
		"xls" => "application/excel",
		"doc" => "application/msword",
		//"bin"=>"application/octet-stream",
		"pdf" => "application/pdf",
		"zip" => "application/zip",
		"mp3" => "audio/mpeg",
		"ram" => "audio/x-pn-realaudio",
		"wav" => "audio/x-wav",
		"csv" => "text/comma-separated-values",
	];

	/**
	 * @var string[]
	 */
	protected static $imageMimeTypes = [
		"gif"  => "image/gif",
		"jpeg" => "image/jpeg",
		"jpg"  => "image/jpeg",
		"png"  => "image/png",
		"bmp"  => "image/x-ms-bmp",
		"ief"  => "image/ief",
		"tiff" => "image/tiff",
		"tif"  => "image/tiff",
	];

	/**
	 * @param string $mimeType
	 * @return string
	 */
	public static function getImageMimeType($mimeType){
		$mimeTypes = array_flip(self::$imageMimeTypes);
		return isset($mimeTypes[$mimeType]) ? $mimeTypes[$mimeType] : '';
	}
}
