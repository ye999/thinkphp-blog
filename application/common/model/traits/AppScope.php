<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\model\traits;

use think\db\Query;

/**
 * Trait AppScope
 * @method $this appId(int $appId)
 */
trait AppScope{

	/**
	 * AppId作用域
	 *
	 * @param \think\db\Query $query
	 * @param int             $appId
	 */
	public function scopeAppId(Query $query, $appId){
		$query->where('app_id', $appId);
	}
}
