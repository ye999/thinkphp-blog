<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\model\traits;

/**
 * Trait StatusText
 *
 * @property int         status
 * @property-read string status_text
 */
trait StatusText{

	/**
	 * 状态文本
	 *
	 * @var array
	 */
	private static $DEFAULT_STATUS_TEXT = [
		0 => '禁用',
		1 => '启用',
	];

	/**
	 * 获取状态文本
	 *
	 * @return string
	 * @noinspection PhpUndefinedFieldInspection
	 */
	protected function getStatusTextAttr(){
		$statusTextList = self::$DEFAULT_STATUS_TEXT;
		if(property_exists(static::class, 'STATUS_TEXT')){
			$statusTextList = static::$STATUS_TEXT;
		}

		return isset($statusTextList[$this->status]) ? $statusTextList[$this->status] : '未知';
	}

}
