<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/11 20:15
 */

namespace app\common\model;

use think\Model;

/**
 * 广告位项
 */
class AdvertisingItem extends Model{

	/**
	 * @var array
	 */
	protected $type = [
		'begin_time' => 'timestamp',
		'end_time'   => 'timestamp',
	];
}
