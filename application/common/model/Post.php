<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/7/26 18:32
 */

namespace app\common\model;

use QL\QueryList;
use think\Model;
use think\model\concern\SoftDelete;
use Xin\Support\Num;
use Xin\Support\Time;

/**
 * 帖子模型
 *
 * @property string                     title
 * @property int                        status
 * @property string                     cover
 * @property int                        view_count
 * @property array                      badge_id_list
 * @property \app\common\model\Category category
 */
class Post extends Model{

	use SoftDelete;

	/**
	 * @var int
	 */
	protected $defaultSoftDelete = 0;

	/**
	 * @var array
	 */
	protected $type = [
		'id'              => 'int',
		'uid'             => 'int',
		'app_id'          => 'int',
		'category_id'     => 'int',
		'status'          => 'int',
		'is_original'     => 'int',
		'allow_comment'   => 'int',
		'view_count'      => 'int',
		'good_count'      => 'int',
		'comment_count'   => 'int',
		'last_reply_uid'  => 'int',
		'last_reply_time' => 'int',
		'delete_time'     => 'int',
	];

	/**
	 * @var array
	 */
	private static $BADGE_LIST = null;

	/**
	 * 分类动态属性
	 *
	 * @return \think\model\relation\BelongsTo
	 */
	public function category(){
		return $this->belongsTo("Category", "category_id")
			->field('id,title,cover,description');
	}

	/**
	 * 关联评论
	 *
	 * @return \think\model\relation\HasMany
	 */
	public function comments(){
		return $this->hasMany('Comment');
	}

	/**
	 * 获取封面地址
	 *
	 * @return string
	 */
	protected function getCoverUrlAttr(){
		return get_cover_path($this->cover);
	}

	/**
	 * 访问量-获取器（人性化数字）
	 *
	 * @return \app\common\model\Post|string
	 */
	protected function getSimplyViewCountAttr(){
		$val = $this->getData('view_count');
		return Num::formatSimple($val);
	}

	/**
	 * 评论量-获取器（人性化数字）
	 *
	 * @return \app\common\model\Post|string
	 */
	protected function getSimplyCommentCountAttr(){
		$val = $this->getData('comment_count');
		return Num::formatSimple($val);
	}

	/**
	 * 收藏量-获取器（人性化数字）
	 *
	 * @return \app\common\model\Post|string
	 */
	protected function getSimplyCollectCountAttr(){
		$val = $this->getData('collect_count');
		return Num::formatSimple($val);
	}

	/**
	 * 更新时间-获取器（人性化日期）
	 *
	 * @return \app\common\model\Post|string
	 */
	protected function getSimplyUpdateTimeAttr(){
		$val = $this->getData('update_time');
		return Time::formatRelative($val);
	}

	/**
	 * 获取描述信息
	 *
	 * @return \QL\Dom\Elements|string
	 */
	protected function getDescriptionAttr(){
		$content = $this->getData('content');
		$ql = QueryList::html($content);
		return mb_substr($ql->find(null)->text(), 0, 72);
	}

	//	/**
	//	 * 标签ID列表 - 获取器
	//	 *
	//	 * @param string $val
	//	 * @return array
	//	 */
	//	protected function getBadgeIdListAttr($val){
	//		return explode(',', $val);
	//	}
	//
	//	/**
	//	 * 标签ID列表 - 修改器
	//	 *
	//	 * @param array $val
	//	 * @return array
	//	 */
	//	protected function setBadgeIdListAttr($val){
	//		return implode(',', $val);
	//	}
	//
	//	/**
	//	 * 获取标签列表
	//	 *
	//	 * @return array
	//	 */
	//	protected function getBadgeListAttr(){
	//		$badgeIds = $this->badge_id_list;
	//		return self::getBadgeList($badgeIds);
	//	}
	//
	//	/**
	//	 * 根据标签ID获取标签
	//	 *
	//	 * @param array $badgeIds
	//	 * @return array
	//	 */
	//	private static function getBadgeList(array $badgeIds){
	//		if(is_null(self::$BADGE_LIST)){
	//			self::$BADGE_LIST = Db::name('badge')->column('title', 'id');
	//		}
	//
	//		$result = [];
	//		foreach($badgeIds as $badgeId){
	//			if(isset(self::$BADGE_LIST[$badgeId])){
	//				$result[] = [
	//					'id'    => $badgeId,
	//					'title' => self::$BADGE_LIST[$badgeId],
	//				];
	//			}
	//		}
	//
	//		return $result;
	//	}
}
