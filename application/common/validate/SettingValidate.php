<?php
/**
 * I know no such things as genius,it is nothing but labor and diligence.
 *
 * @copyright (c) 2015~2019 BD All rights reserved.
 * @license       http://www.apache.org/licenses/LICENSE-2.0
 * @author        <657306123@qq.com> LXSEA
 */

namespace app\common\validate;

use think\facade\Config;
use think\Validate;

/**
 * 配置验证器
 */
class SettingValidate extends Validate{

	/**
	 * 验证规则
	 *
	 * @var array
	 */
	protected $rule = [
		'name'  => 'require|alphaDash|length:3,32|unique:setting',
		'title' => 'require|length:2,12',
		'group' => 'integer',
		'type'  => 'integer',
	];

	/**
	 * 字段信息
	 *
	 * @var array
	 */
	protected $field = [
		'name'  => '配置标识',
		'title' => '配置标题',
		'group' => '配置分组',
		'type'  => '配置类型',
	];

	/**
	 * 情景模式
	 *
	 * @var array
	 */
	protected $scene = [];

	/**
	 * SettingValidate constructor.
	 *
	 * @param array $rules
	 * @param array $message
	 * @param array $field
	 */
	public function __construct(array $rules = [], array $message = [], array $field = []){
		$typeList = Config::get('web.config_type_list');
		if(!empty($typeList)){
			$this->rule['type'] .= "|in:".implode(",", array_keys($typeList));
		}

		$groupList = Config::get('web.config_group_list');
		if(!empty($groupList)){
			$this->rule['group'] .= "|in:".implode(",", array_keys($groupList));
		}

		parent::__construct($rules, $message, $field);
	}
}
