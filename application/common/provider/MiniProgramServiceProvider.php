<?php
/**
 * The following code, none of which has BUG.
 *
 * @author: BD<657306123@qq.com>
 * @date: 2020/1/7 22:37
 */

namespace app\common\provider;

use EasyWeChat\Factory;
use Xin\Thinkphp5\Provider\ServiceProvider;

class MiniProgramServiceProvider extends ServiceProvider{

	/**
	 * @inheritDoc
	 */
	public function register(){
		// 注册微信小程序
		$this->registerWechatMiniProgram();
	}

	/**
	 * 注册微信小程序
	 */
	private function registerWechatMiniProgram(){
		$appId = $this->config->get('web.weapp_appid');
		$secret = $this->config->get('web.weapp_secret');
		$config = $this->config->get('wechat.mini');
		$config['app_id'] = $appId;
		$config['secret'] = $secret;

		$instance = Factory::miniProgram($config);
		$this->app->bindTo('miniProgram', $instance);
	}
}

