<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\api\controller;

use app\api\ApiController;
use app\common\model\Collect;
use Xin\Thinkphp5\Hint\Facade\Hint;

/**
 * 会员管理
 */
class MemberController extends ApiController{

	/**
	 * 获取用户信息
	 */
	public function getInfo(){
		$uid = $this->user->getUserId();
		$user = $this->user->getUserInfo();

		$user['collect_post_count'] = Collect::where([
			'uid'    => $uid,
			'type'   => Collect::POST_TYPE,
		])->count();

		$user['visit_category_count'] = Collect::where([
			'uid'    => $uid,
			'type'   => Collect::VISIT_CATEGORY_TYPE,
		])->count();

		return Hint::result($user);
	}

	/**
	 * 更新用户信息
	 *
	 * @return \think\Response
	 */
	public function saveInfo(){
		$data = $this->request->only([
			'nickname', 'avatar', 'gender',
			'city', 'province', 'country',
			'language', 'remark', 'email', 'mobile',
		]);
		$user = $this->user->saveUserInfo($data);
		return Hint::result($user);
	}

}
