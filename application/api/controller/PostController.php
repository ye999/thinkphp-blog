<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\api\controller;

use app\api\ApiController;
use app\common\model\Collect;
use app\common\model\Post;
use app\common\service\CategoryService;
use think\db\exception\ModelNotFoundException;
use Xin\Thinkphp5\Hint\Facade\Hint;

/**
 * 文章接口
 */
class PostController extends ApiController{

	/**
	 * 文章列表
	 *
	 * @throws \think\exception\DbException
	 */
	public function lists(){
		$categoryId = $this->request->param('cid/d', 0);
		$keywords = $this->request->keywordsSql();

		$order = 'publish_time desc';
		if(!empty($keywords)){
			$order = 'view_count desc';
		}

		$data = Post::with('category')
			->field(self::getBasePostFields())
			->where('status', 1)
			->when($categoryId > 0, ['category_id' => $categoryId])
			->when(!empty($keywords), [['title', 'like', $keywords],])
			->order($order)
			->paginate($this->request->limit(), false, [
				'page'  => $this->request->page(),
				'query' => $this->request->get(),
			])->each(function(Post $item){
				if($item->view_count > 10000){
					$item->view_count = sprintf("%.2f", $item->view_count / 10000).'万';
				}

				$item->isAutoWriteTimestamp(false);
			});

		$count = 0;
		if($this->request->page() == 1){
			$count = Post::where('status', 1)
				->when($categoryId > 0, ['category_id' => $categoryId])
				->when(!empty($keywords), [['title', 'like', $keywords],])
				->count();
		}

		return Hint::result($data, [
			'count' => $count,
		]);
	}

	/**
	 * 获取基本的文章字段
	 *
	 * @return string
	 */
	public static function getBasePostFields(){
		return 'id,category_id,title,cover,view_count,collect_count,is_original,original_url,good_time,share_count,publish_time';
	}

	/**
	 * 获取文章详情信息
	 *
	 * @return \think\response
	 * @throws ModelNotFoundException
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail(){
		$id = $this->request->idWithValid();
		$uid = $this->user->getUserId(false);

		/** @var Post $info */
		$info = Post::with('category')->where('id', $id)->findOrFail();
		if($info->status == 0){
			throw new ModelNotFoundException("文章不存在！", 'Post');
		}
		$info->isAutoWriteTimestamp(false);

		$info['is_visit'] = $uid
			&& logic_action('common/collect/collect', [
				Collect::VISIT_POST_TYPE, $id,
			]);

		$info['is_collect'] = $uid
			&& logic_action('common/collect/isCollected', [
				Collect::POST_TYPE, $id,
			]);

		/** @var CategoryService $categoryService */
		$categoryService = app(CategoryService::class);
		$info['good_categories'] = $categoryService->lists([], 'sort asc', 1, 4);

		return Hint::result($info);
	}

	/**
	 * 收藏文章
	 *
	 * @return mixed
	 */
	public function collect(){
		return logic_action('common/collect/collectWithResponse', [
			Collect::POST_TYPE,
			$this->request->idWithValid(),
		]);
	}

	/**
	 * 取消收藏文章
	 *
	 * @return mixed
	 */
	public function unCollect(){
		return logic_action('common/collect/unCollectWithResponse', [
			Collect::POST_TYPE,
			$this->request->idWithValid(),
		]);
	}

	/**
	 * 上报分享数量
	 *
	 * @return \think\Response
	 * @throws \think\Exception
	 */
	public function reportShareCount(){
		$id = $this->request->idWithValid();
		Post::where('id', $id)->setInc('share_count');
		return Hint::result();
	}
}
