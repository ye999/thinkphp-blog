<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\api;

use app\BaseController;

/**
 * Api 基础控制器
 *
 * @property-read \Xin\Auth\UserInterface             $user
 * @property-read \EasyWeChat\MiniProgram\Application $miniProgram
 */
class ApiController extends BaseController{

	protected $middleware = [];

	/**
	 * 验证失败是否抛出异常
	 *
	 * @var bool
	 */
	protected $failException = true;

	/**
	 * 用户接口
	 *
	 * @var \Xin\Auth\UserInterface
	 */
	protected $user;

	/**
	 * @var \EasyWeChat\MiniProgram\Application
	 */
	protected $miniProgram;

	/**
	 * 初始化
	 */
	protected function initialize(){
		$this->user = $this->app['user'];
		$this->miniProgram = $this->app['miniProgram'];
	}
}
