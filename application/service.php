<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

return [
	// 系统包
	Xin\Thinkphp5\Hint\HintServiceProvider::class,
	Xin\Thinkphp5\Auth\AuthServiceProvider::class,
	Xin\Thinkphp5\Filesystem\FilesystemServiceProvider::class,
	Xin\Thinkphp5\Setting\SettingServiceProvider::class,

	// 其他包
	Xin\Thinkphp5\ArticleCollect\ArticleCollectServiceProvider::class,

	// 应用包
	app\common\provider\AppServiceProvider::class,
	app\common\provider\MiniProgramServiceProvider::class,
];
