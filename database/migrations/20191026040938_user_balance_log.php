<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class UserBalanceLog extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('user_balance_log')){
			//			$this->dropTable('user_balance_log');
			return;
		}

		$table = $this->table('user_balance_log', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '用户余额记录表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('type', 'integer', [
				'limit'   => MysqlAdapter::INT_TINY,
				'scale'   => 2,
				'signed'  => false,
				'comment' => '类型：0.减少，1.增加',
			])
			->addColumn('value', 'decimal', [
				'limit'   => 10,
				'scale'   => 2,
				'signed'  => false,
				'comment' => '增加或减少的余额',
			])
			->addColumn('current_value', 'decimal', [
				'limit'   => 11,
				'signed'  => false,
				'comment' => '增加或减少的余额',
			])
			->addColumn('remark', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '描述',
			])
			->addColumn('admin_id', 'string', [
				'limit'   => 11,
				'default' => '0',
				'comment' => '操作员ID',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['uid'])
			->create();
	}
}
