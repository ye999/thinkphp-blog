<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class User extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('user')){
//			$this->dropTable('user');
			return;
		}

		$table = $this->table('user', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '终端用户表',
		]);

		$table->addColumn('nickname', 'string', [
			'limit'   => 50,
			'default' => '',
			'comment' => '用户昵称',
		])
			->addColumn('gender', 'boolean', [
				'limit'   => MysqlAdapter::TEXT_TINY,
				'default' => '1',
				'comment' => '性别',
			])
			->addColumn('openid', 'string', [
				'limit'   => 50,
				'default' => '',
				'comment' => '用户唯一标识',
			])
			->addColumn('avatar', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '用户头像地址',
			])
			->addColumn('mobile', 'string', [
				'limit'   => 15,
				'default' => '1',
				'comment' => '手机号',
			])
			->addColumn('email', 'string', [
				'limit'   => 50,
				'default' => '1',
				'comment' => 'Email',
			])
			->addColumn('remark', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '描述',
			])
			->addColumn('language', 'string', [
				'limit'   => 12,
				'default' => '',
				'comment' => '语言',
			])
			->addColumn('country', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '国家',
			])
			->addColumn('province', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '省',
			])
			->addColumn('city', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '城市',
			])
			->addColumn('source', 'integer', [
				'limit'   => MysqlAdapter::INT_SMALL,
				'signed'  => false,
				'default' => 0,
				'comment' => '来源信息',
			])
			->addColumn('pid', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '推荐用户id',
			])
			->addColumn('integral', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '积分',
			])
			->addColumn('balance', 'decimal', [
				'limit'   => 10,
				'scale'   => 2,
				'signed'  => false,
				'default' => 0,
				'comment' => '余额信息',
			])
			->addColumn('login_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '登录次数',
			])
			->addColumn('login_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '最后登录时间',
			])
			->addColumn('login_ip', 'biginteger', [
				'limit'   => 20,
				'signed'  => false,
				'default' => 0,
				'comment' => '最后登录IP',
			])
			->addColumn('is_sync', 'integer', [
				'limit'   => MysqlAdapter::INT_SMALL,
				'signed'  => false,
				'default' => 0,
				'comment' => '是否同步',
			])
			->addColumn('sync_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '同步时间',
			])
			->addColumn('delete_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '删除时间',
			])
			->addColumn('update_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '更新时间',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['openid'])
			->create();
	}
}
