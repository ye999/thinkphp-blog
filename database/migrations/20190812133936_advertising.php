<?php

use think\migration\Migrator;

class Advertising extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('advertising')){
			//			$this->dropTable('advertising');
			return;
		}

		$table = $this->table('advertising', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '广告位表',
		]);

		$table->addColumn('title', 'string', [
			'limit'   => 48,
			'default' => '',
			'comment' => '标题',
		])
			->addColumn('name', 'string', [
				'limit'   => 32,
				'default' => '',
				'comment' => '唯一标识',
			])
			->addColumn('type', 'integer', [
				'limit'   => 4,
				'signed'  => false,
				'default' => 0,
				'comment' => '广告类型,0.PC端广告，1.小程序端广告',
			])
			->addColumn('status', 'boolean', [
				'limit'   => 1,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['name'], ['unique' => true])
			->create();
	}
}
