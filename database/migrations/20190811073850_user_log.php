<?php

use think\migration\Migrator;

class UserLog extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('user_log')){
			//			$this->dropTable('user_log');
			return;
		}

		$table = $this->table('user_log', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '用户记录表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('original', 'text', [
				'comment' => '更改的字段，原来的值',
			])
			->addColumn('new', 'text', [
				'comment' => '更改的字段，现在的新值',
			])
			->addColumn('remark', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '说明',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['uid'], [])
			->create();
	}
}
