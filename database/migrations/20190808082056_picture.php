<?php

use think\migration\Migrator;

class Picture extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('picture')){
			//			$this->dropTable('picture');
			return;
		}

		$table = $this->table('picture', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8',
			'collation' => 'utf8_general_ci',
			'comment'   => '图片库表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('path', 'string', [
				'limit'   => 255,
				'comment' => '路径',
			])
			->addColumn('sha1', 'char', [
				'limit'   => 255,
				'comment' => '文件 sha1编码',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['sha1'])
			->addIndex(['uid'])
			->create();
	}
}
