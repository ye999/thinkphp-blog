<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Collect extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		// create the table
		if($this->hasTable('collect')){
			//			$this->dropTable('collect');
			return;
		}

		$table = $this->table('collect', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '收藏表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('topic_id', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '关联主题',
			])
			->addColumn('type', 'integer', [
				'limit'   => MysqlAdapter::INT_SMALL,
				'signed'  => false,
				'default' => 0,
				'comment' => '关联类型，0.文章,1.图集',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['uid', 'type'], [])
			->create();
	}
}
