<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class UserIntegralLog extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('user_integral_log')){
			//			$this->dropTable('user_integral_log');
			return;
		}

		$table = $this->table('user_integral_log', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '用户积分记录表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('type', 'integer', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'comment' => '类型：0.减少，1.增加',
			])
			->addColumn('value', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'comment' => '增加或减少的积分',
			])
			->addColumn('remark', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '描述',
			])
			->addColumn('admin_id', 'string', [
				'limit'   => 11,
				'default' => '0',
				'comment' => '操作员ID',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['uid'])
			->create();
	}
}
