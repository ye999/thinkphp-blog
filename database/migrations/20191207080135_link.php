<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Link extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('link')){
			//			$this->dropTable('link');
			return;
		}

		$table = $this->table('link', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '友链表',
		]);

		$table->addColumn('title', 'string', [
			'limit'   => 48,
			'default' => '',
			'comment' => '标题',
		])
			->addColumn('url', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => 'url地址',
			])
			->addColumn('sort', 'integer', [
				'limit'   => 4,
				'signed'  => false,
				'default' => 0,
				'comment' => '排序',
			])
			->addColumn('status', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('expire_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '到期时间',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->create();
	}
}
