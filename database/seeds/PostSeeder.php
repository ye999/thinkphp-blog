<?php

use think\migration\Seeder;

class PostSeeder extends Seeder{

	use \Xin\Thinkphp5\Database\Seeder;

	public function getDependencies(){
		return [
		];
	}

	/**
	 * Run Method.
	 * Write your database seeder using this method.
	 * More information on writing seeders is available here:
	 * http://docs.phinx.org/en/latest/seeding.html
	 */
	public function run(){
		$this->factoryToTable('post', function(\Faker\Generator $faker){
			return [
				'title'         => $faker->realText(24),
				'uid'           => 1,
				'category_id'   => rand(1, 19),
				'content'       => $faker->realText(6550, rand(3, 5)),
				'view_count'    => rand(100, 99999999),
				'collect_count' => rand(100, 999999),
				'comment_count' => rand(100, 999999),
				'good_time'     => rand(0, 10) === 0 ? time() : 0,
				'status'        => 1,
				'publish_time'  => time() + rand(0, 31536000),
				'update_time'   => time() + rand(0, 31536000),
				'create_time'   => time(),
			];
		}, 100);
	}
}
